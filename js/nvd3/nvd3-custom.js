function randomData(e, t) {
    var n = [], r = [ "circle", "cross", "triangle-up", "triangle-down", "diamond", "square" ], s = d3.random.normal();
    for (i = 0; i < e; i++) {
        n.push({
            key: "Group " + i,
            values: []
        });
        for (j = 0; j < t; j++) n[i].values.push({
            x: s(),
            y: s(),
            size: Math.random(),
            shape: r[j % 6]
        });
    }
    return n;
}

function stream_layers(e, t, n) {
    function r(e) {
        var n = 1 / (.1 + Math.random()), r = 2 * Math.random() - .5, i = 10 / (.1 + Math.random());
        for (var s = 0; s < t; s++) {
            var o = (s / t - r) * i;
            e[s] += n * Math.exp(-o * o);
        }
    }
    arguments.length < 3 && (n = 0);
    return d3.range(e).map(function() {
        var e = [], i;
        for (i = 0; i < t; i++) e[i] = n + n * Math.random();
        for (i = 0; i < 5; i++) r(e);
        return e.map(stream_index);
    });
}

function stream_waves(e, t) {
    return d3.range(e).map(function(e) {
        return d3.range(t).map(function(n) {
            var r = 20 * n / t - e / 3;
            return 2 * r * Math.exp(-0.5 * r);
        }).map(stream_index);
    });
}

function stream_index(e, t) {
    return {
        x: t,
        y: Math.max(0, e)
    };
}

var dataOne = [ {
    key: "Quantity",
    bar: !0,
    values: [ [ 11360052e5, 80000 ], [ 11386836e5, 87000 ], [ 11411028e5, 91000 ], [ 11437812e5, 99000 ], [ 11463696e5, 88000 ], [ 1149048e6, 99000 ], [ 115164e7, 97000 ], [ 11543184e5, 70000 ], [ 11569968e5, 87000 ], [ 11595888e5, 99000 ], [ 11622708e5, 100000 ], [ 11648628e5, 102000 ]]
}, {
    key: "Price",
    values: [ [ 11360052e5, 76000 ], [ 11386836e5, 85000 ], [ 11411028e5, 95000 ], [ 11437812e5, 101000 ], [ 11463696e5, 91000 ], [ 1149048e6, 105000 ], [ 115164e7, 95000 ], [ 11543184e5, 87000 ], [ 11569968e5, 97000 ], [ 11595888e5, 91000 ], [ 11622708e5, 98000 ], [ 11648628e5, 105000 ] ]
} ].map(function(e) {
    e.values = e.values.map(function(e) {
        return {
            x: e[0],
            y: e[1]
        };
    });
    return e;
}), chart;

nv.addGraph(function() {
    chart = nv.models.linePlusBarChart().margin({
        top: 30,
        right: 60,
        bottom: 50,
        left: 70
    }).x(function(e, t) {
        return t;
    }).color(d3.scale.category10().range());
    chart.xAxis.tickFormat(function(e) {
        var t = dataOne[0].values[e] && dataOne[0].values[e].x || 0;
        return t ? d3.time.format("%x")(new Date(t)) : "";
    }).showMaxMin(!1);
    chart.y1Axis.tickFormat(d3.format(",f"));
    chart.y2Axis.tickFormat(function(e) {
        return "$" + d3.format(",.2f")(e);
    });
    chart.bars.forceY([ 0 ]).padData(!1);
    d3.select("#chart svg").datum(dataOne).transition().duration(500).call(chart);
    nv.utils.windowResize(chart.update);
    chart.dispatch.on("stateChange", function(e) {
        nv.log("New State:", JSON.stringify(e));
    });
    return chart;
});



var chart;
var dataTwo = [{
	key: "Stream 1",
	color: "orange",
	values: [
		{x: 1, y: 1}
	]
}];
nv.addGraph(function() {
  
  chart = nv.models.historicalBarChart();

  chart
      .x(function(d,i) { return d.x });

  chart.xAxis // chart sub-models (ie. xAxis, yAxis, etc) when accessed directly, return themselves, not the parent chart, so need to chain separately
      .tickFormat(d3.format(',.1f'))
      .axisLabel("Time")
      ;

  chart.yAxis
      .axisLabel('Random Number')
      .tickFormat(d3.format(',.4f'));

  chart.showXAxis(true).showYAxis(true).rightAlignYAxis(true).margin({right: 90});

  d3.select('.realtime-bar svg')
      .datum(dataTwo)
      .transition().duration(500)
      .call(chart);

  nv.utils.windowResize(chart.update);

  return chart;
});

var x = 2;
var run = true;
setInterval(function(){
	if (!run) return;
	
	var spike = (Math.random() > 0.95) ? 10: 1;
	dataTwo[0].values.push({
		x: x,
		y: Math.random() * spike
	});

	if (dataTwo[0].values.length > 70) {
		dataTwo[0].values.shift();
	}
	x++;

	chart.update();
}, 500);

d3.select("#start-stop-button").on("click",function() {
	if($(this).hasClass('btn-danger')){
		$(this).removeClass('btn-danger').addClass('btn-primary').html('Start Live Stream');
	}
	else
	{
		$(this).removeClass('btn-primary').addClass('btn-danger').html('Stop Live Stream');
		
	}
	run = !run;
});

var graph;

nv.addGraph(function() {
    graph = nv.models.scatterChart().showDistX(!0).showDistY(!0).useVoronoi(!0).color(d3.scale.category10().range()).transitionDuration(300);
    graph.xAxis.tickFormat(d3.format(".02f"));
    graph.yAxis.tickFormat(d3.format(".02f"));
    graph.tooltipContent(function(e) {
        return "<h2>" + e + "</h2>";
    });
    d3.select("#chart4 svg").datum(randomData(4, 40)).call(graph);
    nv.utils.windowResize(graph.update);
    graph.dispatch.on("stateChange", function(e) {
        "New State:", JSON.stringify(e);
    });
    return graph;
});

var test_data = stream_layers(3, 10 + Math.random() * 100, .1).map(function(e, t) {
    return {
        key: "Stream" + t,
        values: e
    };
});

console.log("td", test_data);


var negative_test_data = (new d3.range(0, 3)).map(function(e, t) {
    return {
        key: "Stream" + t,
        values: (new d3.range(0, 11)).map(function(e, t) {
            return {
                y: 10 + Math.random() * 100 * (Math.floor(Math.random() * 100) % 2 ? 1 : -1),
                x: t
            };
        })
    };
}), chartSub;


nv.addGraph(function() {
    chartSub = nv.models.multiBarChart().barColor(d3.scale.category20().range()).margin({
        bottom: 100
    }).transitionDuration(300).delay(0).rotateLabels(45).groupSpacing(.1);
    chartSub.multibar.hideable(!0);
    chartSub.reduceXTicks(!1).staggerLabels(!0);
    chartSub.xAxis.axisLabel("Current Index").showMaxMin(!0).tickFormat(d3.format(",.6f"));
    chartSub.yAxis.tickFormat(d3.format(",.1f"));
    d3.select("#chart5 svg").datum(negative_test_data).call(chartSub);
    nv.utils.windowResize(chartSub.update);
    chartSub.dispatch.on("stateChange", function(e) {
        nv.log("New State:", JSON.stringify(e));
    });
    return chartSub;
});