/* API method to get paging information */
$.fn.dataTableExt.oApi.fnPagingInfo = function ( oSettings )
{
    return {
        "iStart":         oSettings._iDisplayStart,
        "iEnd":           oSettings.fnDisplayEnd(),
        "iLength":        oSettings._iDisplayLength,
        "iTotal":         oSettings.fnRecordsTotal(),
        "iFilteredTotal": oSettings.fnRecordsDisplay(),
        "iPage":          oSettings._iDisplayLength === -1 ?
            0 : Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
        "iTotalPages":    oSettings._iDisplayLength === -1 ?
            0 : Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
    };
}
 
/* Bootstrap style pagination control */
$.extend( $.fn.dataTableExt.oPagination, {
    "bootstrap": {
        "fnInit": function( oSettings, nPaging, fnDraw ) {
            var oLang = oSettings.oLanguage.oPaginate;
            var fnClickHandler = function ( e ) {
                e.preventDefault();
                if ( oSettings.oApi._fnPageChange(oSettings, e.data.action) ) {
                    fnDraw( oSettings );
                }
            };
 
            $(nPaging).addClass('pagination  pull-right').append(
                '<ul class="pagination pagination-inverse pull-right">'+
                    '<li class="prev disabled"><a href="#">&larr; '+oLang.sPrevious+'</a></li>'+
                    '<li class="next disabled"><a href="#">'+oLang.sNext+' &rarr; </a></li>'+
                '</ul>'
            );
            var els = $('a', nPaging);
            $(els[0]).bind( 'click.DT', { action: "previous" }, fnClickHandler );
            $(els[1]).bind( 'click.DT', { action: "next" }, fnClickHandler );
        },
 
        "fnUpdate": function ( oSettings, fnDraw ) {
            var iListLength = 5;
            var oPaging = oSettings.oInstance.fnPagingInfo();
            var an = oSettings.aanFeatures.p;
            var i, j, sClass, iStart, iEnd, iHalf=Math.floor(iListLength/2);
 
            if ( oPaging.iTotalPages < iListLength) {
                iStart = 1;
                iEnd = oPaging.iTotalPages;
            }
            else if ( oPaging.iPage <= iHalf ) {
                iStart = 1;
                iEnd = iListLength;
            } else if ( oPaging.iPage >= (oPaging.iTotalPages-iHalf) ) {
                iStart = oPaging.iTotalPages - iListLength + 1;
                iEnd = oPaging.iTotalPages;
            } else {
                iStart = oPaging.iPage - iHalf + 1;
                iEnd = iStart + iListLength - 1;
            }
 
            for ( i=0, iLen=an.length ; i<iLen ; i++ ) {
                // Remove the middle elements
                $('li:gt(0)', an[i]).filter(':not(:last)').remove();
 
                // Add the new list items and their event handlers
                for ( j=iStart ; j<=iEnd ; j++ ) {
                    sClass = (j==oPaging.iPage+1) ? 'class="active"' : '';
                    $('<li '+sClass+'><a href="#">'+j+'</a></li>')
                        .insertBefore( $('li:last', an[i])[0] )
                        .bind('click', function (e) {
                            e.preventDefault();
                            oSettings._iDisplayStart = (parseInt($('a', this).text(),10)-1) * oPaging.iLength;
                            fnDraw( oSettings );
                        } );
                }
 
                // Add / remove disabled classes from the static elements
                if ( oPaging.iPage === 0 ) {
                    $('li:first', an[i]).addClass('disabled');
                } else {
                    $('li:first', an[i]).removeClass('disabled');
                }
 
                if ( oPaging.iPage === oPaging.iTotalPages-1 || oPaging.iTotalPages === 0 ) {
                    $('li:last', an[i]).addClass('disabled');
                } else {
                    $('li:last', an[i]).removeClass('disabled');
                }
            }
        }
    }
} );

(function() {
 
/*
 * Natural Sort algorithm for Javascript - Version 0.7 - Released under MIT license
 * Author: Jim Palmer (based on chunking idea from Dave Koelle)
 * Contributors: Mike Grier (mgrier.com), Clint Priest, Kyle Adams, guillermo
 * See: http://js-naturalsort.googlecode.com/svn/trunk/naturalSort.js
 */
function naturalSort (a, b) {
    var re = /(^-?[0-9]+(\.?[0-9]*)[df]?e?[0-9]?$|^0x[0-9a-f]+$|[0-9]+)/gi,
        sre = /(^[ ]*|[ ]*$)/g,
        dre = /(^([\w ]+,?[\w ]+)?[\w ]+,?[\w ]+\d+:\d+(:\d+)?[\w ]?|^\d{1,4}[\/\-]\d{1,4}[\/\-]\d{1,4}|^\w+, \w+ \d+, \d{4})/,
        hre = /^0x[0-9a-f]+$/i,
        ore = /^0/,
        // convert all to strings and trim()
        x = a.toString().replace(sre, '') || '',
        y = b.toString().replace(sre, '') || '',
        // chunk/tokenize
        xN = x.replace(re, '\0$1\0').replace(/\0$/,'').replace(/^\0/,'').split('\0'),
        yN = y.replace(re, '\0$1\0').replace(/\0$/,'').replace(/^\0/,'').split('\0'),
        // numeric, hex or date detection
        xD = parseInt(x.match(hre)) || (xN.length != 1 && x.match(dre) && Date.parse(x)),
        yD = parseInt(y.match(hre)) || xD && y.match(dre) && Date.parse(y) || null;
    // first try and sort Hex codes or Dates
    if (yD)
        if ( xD < yD ) return -1;
        else if ( xD > yD )  return 1;
    // natural sorting through split numeric strings and default strings
    for(var cLoc=0, numS=Math.max(xN.length, yN.length); cLoc < numS; cLoc++) {
        // find floats not starting with '0', string or 0 if not defined (Clint Priest)
        var oFxNcL = !(xN[cLoc] || '').match(ore) && parseFloat(xN[cLoc]) || xN[cLoc] || 0;
        var oFyNcL = !(yN[cLoc] || '').match(ore) && parseFloat(yN[cLoc]) || yN[cLoc] || 0;
        // handle numeric vs string comparison - number < string - (Kyle Adams)
        if (isNaN(oFxNcL) !== isNaN(oFyNcL)) return (isNaN(oFxNcL)) ? 1 : -1;
        // rely on string comparison if different types - i.e. '02' < 2 != '02' < '2'
        else if (typeof oFxNcL !== typeof oFyNcL) {
            oFxNcL += '';
            oFyNcL += '';
        }
        if (oFxNcL < oFyNcL) return -1;
        if (oFxNcL > oFyNcL) return 1;
    }
    return 0;
}
 
$.extend( jQuery.fn.dataTableExt.oSort, {
    "natural-asc": function ( a, b ) {
        return naturalSort(a,b);
    },
 
    "natural-desc": function ( a, b ) {
        return naturalSort(a,b) * -1;
    }
} );
 
}());

$.extend( jQuery.fn.dataTableExt.oSort, {
    "num-html-pre": function ( a ) {
        var x = String(a).replace( /<[\s\S]*?>/g, "" );
        return parseFloat( x );
    },
 
    "num-html-asc": function ( a, b ) {
        return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    },
 
    "num-html-desc": function ( a, b ) {
        return ((a < b) ? 1 : ((a > b) ? -1 : 0));
    }
} );

$.extend(jQuery.fn.dataTableExt.oSort, {
    "hybrid-asc": function(x, y) {
        if (!isNaN(parseInt(x)) && !isNaN(parseInt(y))) {
            x = parseInt(x);
            y = parseInt(y);
        }
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    },
    "hybrid-desc": function(x, y) {
        if (!isNaN(parseInt(x)) && !isNaN(parseInt(y))) {
            x = parseInt(x);
            y = parseInt(y);
        }
        return ((x < y) ? 1 : ((x > y) ? -1 : 0));
    }
});

jQuery.fn.dataTableExt.aTypes.push( 
    function ( sData ) 
    { 
        var sValidChars = "0123456789.-,"; 
        var Char; 
           
        /* Check the numeric part */ 
        for ( i=1 ; i<sData.length ; i++ )  
        {  
            Char = sData.charAt(i);  
            if (sValidChars.indexOf(Char) == -1)  
            { 
                return null; 
            } 
        } 
           
        /* Check prefixed by currency */ 
        if ( sData.charAt(0) == '$' || sData.charAt(0) == '£' ) 
        { 
            return 'currency'; 
        } 
        return null; 
    } 
);
 
$.fn.dataTableExt.oSort['currency-asc'] = function(a,b) {
    /* Remove any commas (assumes that if present all strings will have a fixed number of d.p) */
    var x = a == "-" ? 0 : a.replace( /,/g, "" );
    var y = b == "-" ? 0 : b.replace( /,/g, "" );
     
    /* Remove the currency sign */
    x = x.substring( 1 );
    y = y.substring( 1 );
     
    /* Parse and return */
    x = parseFloat( x );
    y = parseFloat( y );
    return x - y;
};
 
$.fn.dataTableExt.oSort['currency-desc'] = function(a,b) {
    /* Remove any commas (assumes that if present all strings will have a fixed number of d.p) */
    var x = a == "-" ? 0 : a.replace( /,/g, "" );
    var y = b == "-" ? 0 : b.replace( /,/g, "" );
     
    /* Remove the currency sign */
    x = x.substring( 1 );
    y = y.substring( 1 );
     
    /* Parse and return */
    x = parseFloat( x );
    y = parseFloat( y );
    return y - x;
};

jQuery.extend( jQuery.fn.dataTableExt.oSort, {
    "formatted-num-pre": function ( a ) {
        a = (a === "-" || a === "") ? 0 : a.replace( /[^\d\-\.]/g, "" );
        return parseFloat( a );
    },
 
    "formatted-num-asc": function ( a, b ) {
        return a - b;
    },
 
    "formatted-num-desc": function ( a, b ) {
        return b - a;
    }
} );


jQuery.extend( jQuery.fn.dataTableExt.oSort, {
    "date-uk-pre": function ( a ) {
        var ukDatea = a.split('/');
        return (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
    },
 
    "date-uk-asc": function ( a, b ) {
        return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    },
 
    "date-uk-desc": function ( a, b ) {
        return ((a < b) ? 1 : ((a > b) ? -1 : 0));
    }
} );

$(document).ready(function() {
	$('#example5').dataTable({"sPaginationType": "bootstrap",
		aaSorting: [[4, 'asc']],
		iDisplayLength: 100,
		 "aoColumns": [
			null,
			null,
			null,
			null,
			{ "sType": 'formatted-num' },
			null
			
		]});
} );

$(document).ready(function() {
	$('#example').dataTable({"sPaginationType": "bootstrap",
	aaSorting: [[3, 'asc']],
		 "aoColumns": [
			null,
			null,
			null,
			{ "sType": 'currency' }
			
		]});
} );

$(document).ready(function() {
	$('#example2').dataTable({"sPaginationType": "bootstrap",
	aaSorting: [[3, 'asc']],
		 "aoColumns": [
			null,
			null,
			null,
			{ "sType": 'currency' }
			
		]});
} );

$(document).ready(function() {
	$('#example3').dataTable({"sPaginationType": "bootstrap",
	aaSorting: [[3, 'asc']],
		 "aoColumns": [
			null,
			null,
			null,
			{ "sType": 'currency' }
			
		]});
} );

$(document).ready(function() {
	$('#caHistory').dataTable({"sPaginationType": "bootstrap",
	aaSorting: [[1, 'desc']],
	"aoColumns": [
                              null,
							  {"iDataSort": 7},
							  null,
							  null,
                              null,
                              null,
                              null,
							  {"bVisible": false},
                             ]});
} );


$(document).ready(function() {
	$('#baHistory').dataTable({"sPaginationType": "bootstrap",
	
} );
} );

$(document).ready(function() {
	$('#coHistory').dataTable({"sPaginationType": "bootstrap",
	
} );
} );

$(document).ready(function() {
	$('#allCustomers').dataTable({"sPaginationType": "bootstrap",
	aaSorting: [[2, 'desc']],
		 "aoColumns": [
			null,
			null,
			{ "sType": 'currency' },
			null]
	
} );
} );

$(document).ready(function() {
	$('#loadOrders').dataTable({"sPaginationType": "bootstrap",
	aaSorting: [[2, 'desc']],
		 "aoColumns": [
			null,
			{ "sType": 'currency' },
			null,
			null]
	
} );
} );



$(document).ready(function() {
	$('#aeHistory').dataTable({"sPaginationType": "bootstrap",
	aaSorting: [[2, 'desc']],
	"aoColumns": [
                              null,
							  null,
							  { "sType": 'currency' },
                              null,
                              null,
                              null,
                             ]});
} );


$(document).ready(function() {
	$('#pHistory').dataTable({
	"sPaginationType": "bootstrap",
	aaSorting: [[3, "asc"]],
	"aoColumns": [
                              null,
							  null,
							  null,
                              null
                             ]});
	
});

$(document).ready(function() {
	$('#carrierList').dataTable({
	"sPaginationType": "bootstrap",
	aaSorting: [[5, "desc"]],
	iDisplayLength: 100,
	"aoColumns": [
                              null,
							  null,
							  null,
							  { "sType": 'currency' },
                              null,
							  null
                             ]});
	
});

$(document).ready(function() {
	$('#aeList').dataTable({
	"sPaginationType": "bootstrap",
	aaSorting: [[2, "desc"]],
	iDisplayLength: 100,
	"aoColumns": [
                              null,
							  null,
							 { "sType": 'currency' },
                              null,
							  null
                             ]});
	
});

$(document).ready(function() {
	$('#ray').dataTable({"sPaginationType": "bootstrap",
	aaSorting: [[0, 'asc']]
	
} );} );