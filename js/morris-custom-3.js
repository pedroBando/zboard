Morris.Donut({
    element: 'chartCoPiece1',
    data: chartCoPiece1,
	colors: [ "#00ecb7", "#bffff1" ],
    formatter: function(e) {
        return e + "%";
    }
}).select(0);

Morris.Donut({
    element: 'chartCoPiece2',
    data: chartCoPiece2,
	colors: [ "#F79219", "#f2b979" ],
    formatter: function(e) {
        return e + "%";
    }
}).select(0);

Morris.Donut({
  element: 'donut-example',
  data: [
    {label: "Download Sales", value: 12},
    {label: "In-Store Sales", value: 30},
    {label: "Mail-Order Sales", value: 20}
  ]
});