@extends('layouts.main')

@section('content')
<div class="content"> 
	
    
    <div class="row">
  							<div class="col-mod-12">
								<!-- start breadcrumbs -->
  								<ul class="breadcrumb">
  									<li><a href="#">Carrier Executive Dashboard</a></li>
  								</ul><!-- end breadc-->

  								<div class="form-group hiddn-minibar pull-right">
  								
								
								<div class="col-lg-10 col-md-9">
								
						
                                   
								</select>
                               	
								</div><!-- end col-->	
  							</div><!-- end form group search-->

  								<h3 class="page-header"><i class="fa fa fa-dashboard"></i>Carrier Executive Dashboard <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>
									 
  								<blockquote class="page-information hidden">
  									<p>
  										Dashboard is an "Easy To Read" real-time user interface, showing a graphical presentation of the current status and historical trends of an Zmac’s key performance indicators to enable that users can se their different stats.
  									</p>
  								</blockquote>
  							</div><!-- end col-md-12 -->
  						</div><!-- end row -->


  						<!-- Info Boxes -->
  						<div class="row panel-body" style="background-color:#2F343B">
  							<div class="col-md-4">
  								<div class="info-box  bg-info  text-white">
  									<div class="info-icon bg-info-dark">
  										<i class="fa fa-dollar fa-4x"></i>
  									</div>
  									<div class="info-details">
  										<h4>Current Month Gross Profit  <span class="pull-right">${{ number_format($dashboard_data->current_month_gp, 2, '.',',')}}</span></h4>
  										<p>Last Month <span class="badge pull-right bg-white text-info"> ${{ 100*number_format($dashboard_data->onemonthago_gp, 2,'.', ',')}}</span> </p>
  									</div>
  								</div><!-- end info box-->
  							</div><!-- end col-md4-->
  							<div class="col-md-4 ">
  								<div class="info-box  bg-success  text-white">
  									<div class="info-icon bg-success-dark">
  										<i class="fa fa-road fa-4x"></i>
  									</div>
  									<div class="info-details">
  										<h4>Curent Month Gross Profit %    <span class="pull-right">{{ (100*$dashboard_data->current_month_gp_por) }}%</span></h4>
  										<p>Last Month<span class="badge pull-right bg-white text-success"> {{ 100*$dashboard_data->onemonthago_gp_por }}%</span> </p>
  									</div>
  								</div><!-- end info box-->
  							</div><!-- end col-md4-->
  							<div class="col-md-4">
  								<div class="info-box  bg-warning  text-white">
  									<div class="info-icon bg-warning-dark">
  										<i class="fa fa-certificate fa-4x"></i>
  									</div>
  									<div class="info-details">
  										<h4>Current % Revenue Goal  <span class="pull-right">{{ 100*$dashboard_data->current_por_rev_goal }}%</span></h4>
  										<p>Last Month <span class="badge pull-right bg-white text-warning">{{ 100*$dashboard_data->onemonthago_por_rev_goal }}%</span></p>
  									</div>
  								</div><!-- end info box-->
  							</div><!-- end col md 4-->
  						</div><!-- end row-->

  						<!-- Charts -->
  						<div class="row" style="padding-top:20px; background-color:#fff;">
  							<!-- Discrete Bar Chart -->
  							<div class="col-md-6">
                              <div class="panel panel-primary">
                                <div class="panel-heading">
                                  <h3 class="panel-title">
  										<i class="fa fa-bar-chart-o"></i>
  											Monthly Margin & Margin Goal
  											<span class="pull-right">
  												<a  href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a>
  												
  											</span>
  									</h3>
                                </div><!-- end panel heading-->
                                
                                <div class="panel-body">
                                  <div id="chartdivca" style="width:100%; height:450px;"></div>
                                </div><!-- end panel body-->
                                
                              </div><!-- end panel-->
                              
                              
                                <div class="info-box  bg-info  text-white">
  									<div class="info-icon bg-info-dark">
  										<i class="fa fa-truck fa-4x"></i>
  									</div>
  									<div class="info-details">
  										<h4>Current Month Loads  <span class="pull-right">{{ $dashboard_data->current_month_orders}}</span></h4>
  										<p>Last Month <span class="badge pull-right bg-white text-info">{{ $dashboard_data->last_month_orders }}</span> </p>
  									</div>
  								</div><!-- end info box--><!-- end info box-->
                            
                              <div class="panel panel-primary" style="border-color:#0C9">
  							  	<div class="panel-heading" style="background-color:#0C9">
  									<h3 class="panel-title">
  										<i class="fa fa-bar-chart-o"></i>
  											Incentives
  											<span class="pull-right">
  												<a  href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a>
  												
  											</span>
  									</h3>
  								</div><!-- end panel heading-->
  							  
                                <div class="panel-body nopadding">
  										
								  <div class="list-group list-statistics">
  											<a href="#" class="list-group-item">
  												Current Monthly Incentive
  												<span class="badge bg-success">${{ $dashboard_data->current_month_incentive_earned }}</span>
  											</a>
  											<a href="#" class="list-group-item" >
  												Monthly Target Incentive
  												<span class="badge bg-success">${{ $dashboard_data->monthly_target_incentive }}</span>
  											</a>
  											<a href="#" class="list-group-item">
  												Last Month Incentive
  												<span class="badge bg-success">${{ $dashboard_data->last_month_incentive_earned }}</span>
  											</a>
  										</div><!--end list-->

  							    </div><!-- end panel body-->
  							  </div><!-- end panel primary-->
                              
                              <div class="panel panel-primary">
  							  	<div class="panel-heading bg-purple">
  									<h3 class="panel-title">
  										<i class="fa fa-bar-chart-o"></i>
  											Message Board
  											<span class="pull-right">
  												<a  href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a>
  												
  											</span>
  									</h3>
  								</div><!-- end panel heading-->
  							  
                                <div class="panel-body nopadding">
  										
								  <div style="height:260px"></div>

  							    </div><!-- end panel body-->
  							  </div><!-- end panel primary-->
                              
                              
                         	</div><!-- end col-->
                         
                           
                            <div class="col-md-6">
                            <div class="panel panel-primary">
  									<div class="panel-heading">
  										<h3 class="panel-title">
  											<i class="fa fa-bar-chart-o"></i>
  											Monthly Incentives Grid
  											<span class="pull-right">
  												<a  href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a>
  												
  											</span>
  										</h3>
  									</div><!-- end panel heading-->
							  <div class="panel-body" style="overflow:scroll; max-height:1127px;">
                                   <span class="label bg-warning pull-left">Margin Goal</span><span class="label bg-primary pull-right">Average Gross Margin</span><br/>
  										<table class="table table-bordered table-striped display" id="incentives">
                                          <tr class="bg-primary">
                                          	<th ></th>
                                           	<th class="bg-primary" style="color:#fff">15.0%</th>
                                            <th class="bg-primary" style="color:#fff">16.0%</th>
                                            <th class="bg-primary" style="color:#fff">17.0%</th>
                                            <th class="bg-primary" style="color:#fff">18.0%</th>
                                            <th class="bg-primary" style="color:#fff">19.0%</th>
                                            <th class="bg-info" style="color:#fff">20.0%</th>
                                            <th class="bg-primary" style="color:#fff">21.0%</th>
                                            <th class="bg-primary" style="color:#fff">22.0%</th>
                                            <th class="bg-primary" style="color:#fff">23.0%</th>
                                            <th class="bg-primary" style="color:#fff">24.0%</th>
                                            <th class="bg-primary" style="color:#fff">25.0%</th>
                                           </tr>
                                  
                                           
                                           
                                           <tr>
                                           	<td class="bg-warning" style="color:#fff">${{ number_format(($dashboard_data->current_month_goal * .7), 0,".",",") }}</td>
                                           	<td @if( ((100*$dashboard_data->current_month_gp_por) > 15 && (100*$dashboard_data->current_month_gp_por) < 15.99) && ((100*$dashboard_data->current_por_rev_goal) < 74.99 && (100*$dashboard_data->current_por_rev_goal) >= 70))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  .11), 2,".",",")}}</td>
                                            <td @if( (100*$dashboard_data->current_month_gp_por) > 16 && (100*$dashboard_data->current_month_gp_por)  < 16.99 && (100*$dashboard_data->current_por_rev_goal) <= 74.99 && (100*$dashboard_data->current_por_rev_goal) >= 70 )class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  .15), 2,".",",")}}</td>
                                            <td @if( (100*$dashboard_data->current_month_gp_por) > 17 && (100*$dashboard_data->current_month_gp_por)  < 17.99 && (100*$dashboard_data->current_por_rev_goal) <= 74.99 && (100*$dashboard_data->current_por_rev_goal) >= 70)class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  .18), 2,".",",")}}</td>
                                            <td @if( (100*$dashboard_data->current_month_gp_por) > 18 && (100*$dashboard_data->current_month_gp_por)  < 18.99 && (100*$dashboard_data->current_por_rev_goal) <= 74.99 && (100*$dashboard_data->current_por_rev_goal) >= 70)class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .24), 2,".",",")}}</td>
                                            <td @if( (100*$dashboard_data->current_month_gp_por) > 19 && (100*$dashboard_data->current_month_gp_por)  < 19.99 && ((100*$dashboard_data->current_por_rev_goal) <= 74.99 && (100*$dashboard_data->current_por_rev_goal) >= 70.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .27), 2,".",",")}}</td>
                                            <td @if( (100*$dashboard_data->current_month_gp_por) > 20 && (100*$dashboard_data->current_month_gp_por)  < 20.99 && (100*$dashboard_data->current_por_rev_goal) <= 74.99 && (100*$dashboard_data->current_por_rev_goal) >= 70.00 )class="selected" @else class="bg-info" @endif style="color:#fff">${{ number_format(($dashboard_data->monthly_target_incentive * .33), 2,".",",")}}</td>
                                            <td @if( (100*$dashboard_data->current_month_gp_por) > 21 && (100*$dashboard_data->current_month_gp_por)  < 21.99 && (100*$dashboard_data->current_por_rev_goal) <= 74.99 && (100*$dashboard_data->current_por_rev_goal) >= 70)class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .38), 2,".",",")}}</td>
                                            <td @if( (100*$dashboard_data->current_month_gp_por) > 22 && (100*$dashboard_data->current_month_gp_por)  < 22.99 && (100*$dashboard_data->current_por_rev_goal) <= 74.99 && (100*$dashboard_data->current_por_rev_goal) >= 70)class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .46), 2,".",",")}}</td>
                                            <td @if( (100*$dashboard_data->current_month_gp_por) > 23 && (100*$dashboard_data->current_month_gp_por)  < 23.99  && (100*$dashboard_data->current_por_rev_goal) <= 74.99 && (100*$dashboard_data->current_por_rev_goal) >= 70)class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .51), 2,".",",")}}</td>
                                            <td @if( (100*$dashboard_data->current_month_gp_por) > 24 && (100*$dashboard_data->current_month_gp_por)  < 24.99 && (100*$dashboard_data->current_por_rev_goal) <= 74.99 && (100*$dashboard_data->current_por_rev_goal) >= 70)class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .59), 2,".",",")}}</td>
                                            <td @if( (100*$dashboard_data->current_month_gp_por) > 25 && (100*$dashboard_data->current_por_rev_goal) <= 74.99 && (100*$dashboard_data->current_por_rev_goal) >= 70.00)class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .66), 2,".",",")}}</td>
                                           </tr>
                                           
                                           <tr>
                                           	<td class="bg-warning" style="color:#fff">${{ number_format(($dashboard_data->current_month_goal * .75), 0,".",",") }}</td>
                                           	<td @if( (100*$dashboard_data->current_month_gp_por) < 15 && (100*$dashboard_data->current_por_rev_goal) < 79.99 && (100*$dashboard_data->current_por_rev_goal) > 75.00)class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  .18), 2,".",",")}}</td>
                                            <td @if( (100*$dashboard_data->current_month_gp_por) > 16 && (100*$dashboard_data->current_month_gp_por)  < 16.99 && (100*$dashboard_data->current_por_rev_goal) <= 79.99 && (100*$dashboard_data->current_por_rev_goal) >= 75.00)class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .23), 2,".",",")}}</td>
                                            <td @if( (100*$dashboard_data->current_month_gp_por) > 17 && (100*$dashboard_data->current_month_gp_por)  < 17.99 && (100*$dashboard_data->current_por_rev_goal) <= 79.99 && (100*$dashboard_data->current_por_rev_goal) >= 75.00)class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .27), 2,".",",")}}</td>
                                            <td @if( (100*$dashboard_data->current_month_gp_por) > 18 && (100*$dashboard_data->current_month_gp_por)  < 18.99 && (100*$dashboard_data->current_por_rev_goal) <= 79.99 && (100*$dashboard_data->current_por_rev_goal) >= 75.00)class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .33), 2,".",",")}}</td>
                                            <td @if( (100*$dashboard_data->current_month_gp_por) > 19 && (100*$dashboard_data->current_month_gp_por)  < 19.99 && ((100*$dashboard_data->current_por_rev_goal) <= 79.99 && (100*$dashboard_data->current_por_rev_goal) >= 75.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .37), 2,".",",")}}</td>
                                            <td @if( (100*$dashboard_data->current_month_gp_por) > 20 && (100*$dashboard_data->current_month_gp_por)  < 20.99 && (100*$dashboard_data->current_por_rev_goal) <= 79.99 && (100*$dashboard_data->current_por_rev_goal) >= 75.00)class="selected" @else class="bg-info" @endif style="color:#fff">${{ number_format(($dashboard_data->monthly_target_incentive * .43), 2,".",",")}}</td>
                                            <td @if( (100*$dashboard_data->current_month_gp_por) > 21 && (100*$dashboard_data->current_month_gp_por)  < 21.99 && (100*$dashboard_data->current_por_rev_goal) <= 79.99 && (100*$dashboard_data->current_por_rev_goal) >= 75.00 )class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .49), 2,".",",")}}</td>
                                            <td @if( (100*$dashboard_data->current_month_gp_por) > 22 && (100*$dashboard_data->current_month_gp_por)  < 22.99 && (100*$dashboard_data->current_por_rev_goal) <= 79.99 && (100*$dashboard_data->current_por_rev_goal) >= 75.00 )class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .57), 2,".",",")}}</td>
                                            <td @if( (100*$dashboard_data->current_month_gp_por) > 23 && (100*$dashboard_data->current_month_gp_por)  < 23.99  && (100*$dashboard_data->current_por_rev_goal) <= 79.99 && (100*$dashboard_data->current_por_rev_goal) >= 75.00 )class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .63), 2,".",",")}}</td>
                                            <td @if( (100*$dashboard_data->current_month_gp_por) > 24 && (100*$dashboard_data->current_month_gp_por)  < 24.99 && (100*$dashboard_data->current_por_rev_goal) <= 79.99 && (100*$dashboard_data->current_por_rev_goal) >= 75.00 )class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .71), 2,".",",")}}</td>
                                            <td @if( (100*$dashboard_data->current_month_gp_por) > 25 && (100*$dashboard_data->current_por_rev_goal) <= 79.99 && (100*$dashboard_data->current_por_rev_goal) >= 75.00)class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .78), 2,".",",")}}</td>
                                           </tr>
                                           
                                           
                                           <tr>
                                           	<td class="bg-warning" style="color:#fff">${{ number_format(($dashboard_data->current_month_goal * .8), 0,".",",") }}</td>
                                           	<td @if( (100*$dashboard_data->current_month_gp_por) < 15 && (100*$dashboard_data->current_por_rev_goal) < 84.99 && (100*$dashboard_data->current_por_rev_goal) >= 80.00)class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .25), 2,".",",")}}</td>
                                            <td @if( (100*$dashboard_data->current_month_gp_por) > 16 && (100*$dashboard_data->current_month_gp_por)  < 16.99 && (100*$dashboard_data->current_por_rev_goal) <= 84.99 && (100*$dashboard_data->current_por_rev_goal) >= 80.00)class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .31), 2,".",",")}}</td>
                                            <td @if( (100*$dashboard_data->current_month_gp_por) > 17 && (100*$dashboard_data->current_month_gp_por)  < 17.99 && (100*$dashboard_data->current_por_rev_goal) <= 84.99 && (100*$dashboard_data->current_por_rev_goal) >= 80.00)class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .36), 2,".",",")}}</td>
                                            <td @if( (100*$dashboard_data->current_month_gp_por) > 18 && (100*$dashboard_data->current_month_gp_por)  < 18.99 && (100*$dashboard_data->current_por_rev_goal) <= 84.99 && (100*$dashboard_data->current_por_rev_goal) >= 80.00)class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .42), 2,".",",")}}</td>
                                            <td @if( (100*$dashboard_data->current_month_gp_por) > 19 && (100*$dashboard_data->current_month_gp_por)  < 19.99 && ((100*$dashboard_data->current_por_rev_goal) <= 84.99 && (100*$dashboard_data->current_por_rev_goal) >= 80))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .47), 2,".",",")}}</td>
                                            <td @if( (100*$dashboard_data->current_month_gp_por) > 20 && (100*$dashboard_data->current_month_gp_por)  < 20.99 && (100*$dashboard_data->current_por_rev_goal) <= 84.99 && (100*$dashboard_data->current_por_rev_goal) >= 80.00)class="selected" @else class="bg-info" @endif style="color:#fff">${{ number_format(($dashboard_data->monthly_target_incentive * .53), 2,".",",")}}</td>
                                            <td @if( (100*$dashboard_data->current_month_gp_por) > 21 && (100*$dashboard_data->current_month_gp_por)  < 21.99 && (100*$dashboard_data->current_por_rev_goal) <= 84.99 && (100*$dashboard_data->current_por_rev_goal) >= 80.00)class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .60), 2,".",",")}}</td>
                                            <td @if( (100*$dashboard_data->current_month_gp_por) > 22 && (100*$dashboard_data->current_month_gp_por)  < 22.99 && (100*$dashboard_data->current_por_rev_goal) <= 84.99 && (100*$dashboard_data->current_por_rev_goal) >= 80.00)class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .68), 2,".",",")}}</td>
                                            <td @if( (100*$dashboard_data->current_month_gp_por) > 23 && (100*$dashboard_data->current_month_gp_por)  < 23.99  && (100*$dashboard_data->current_por_rev_goal) <= 84.99 && (100*$dashboard_data->current_por_rev_goal) >= 80.00)class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .75), 2,".",",")}}</td>
                                            <td @if( (100*$dashboard_data->current_month_gp_por) > 24 && (100*$dashboard_data->current_month_gp_por)  < 24.99 && (100*$dashboard_data->current_por_rev_goal) <= 84.99 && (100*$dashboard_data->current_por_rev_goal) >= 80.00)class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .83), 2,".",",")}}</td>
                                            <td @if( (100*$dashboard_data->current_month_gp_por) > 25 && (100*$dashboard_data->current_por_rev_goal) <= 84.99 && (100*$dashboard_data->current_por_rev_goal) >= 80.00)class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .90), 2,".",",")}}</td>
                                           </tr>
                                           
                                           <tr>
                                           	<td class="bg-warning" style="color:#fff">${{ number_format(($dashboard_data->current_month_goal * .85), 0,".",",") }}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 15 && (100*$dashboard_data->current_month_gp_por)  < 15.99) && ((100*$dashboard_data->current_por_rev_goal) >= 80 && (100*$dashboard_data->current_por_rev_goal) <= 89.99 && (100*$dashboard_data->current_por_rev_goal) >= 85.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .32), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 16 && (100*$dashboard_data->current_month_gp_por)  < 16.99) && ((100*$dashboard_data->current_por_rev_goal) >= 85 && (100*$dashboard_data->current_por_rev_goal) <= 89.99 && (100*$dashboard_data->current_por_rev_goal) >= 85.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .39), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 17 && (100*$dashboard_data->current_month_gp_por)  < 17.99) && ((100*$dashboard_data->current_por_rev_goal) >= 85 && (100*$dashboard_data->current_por_rev_goal) <= 89.99 && (100*$dashboard_data->current_por_rev_goal) >= 85.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .45), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 18 && (100*$dashboard_data->current_month_gp_por)  < 18.99) && ((100*$dashboard_data->current_por_rev_goal) >= 85 && (100*$dashboard_data->current_por_rev_goal) <= 89.99 && (100*$dashboard_data->current_por_rev_goal) >= 85.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .52), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 19 && (100*$dashboard_data->current_month_gp_por)  < 19.99) && ((100*$dashboard_data->current_por_rev_goal) >= 85 && (100*$dashboard_data->current_por_rev_goal) <= 89.99 && (100*$dashboard_data->current_por_rev_goal) >= 85.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .58), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 20 && (100*$dashboard_data->current_month_gp_por)  < 20.99) && ((100*$dashboard_data->current_por_rev_goal) >= 85 && (100*$dashboard_data->current_por_rev_goal) <= 89.99 && (100*$dashboard_data->current_por_rev_goal) >= 85.00))class="selected" @else class="bg-info" @endif style="color:#fff">${{ number_format(($dashboard_data->monthly_target_incentive * .65), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 21 && (100*$dashboard_data->current_month_gp_por)  < 21.99) && ((100*$dashboard_data->current_por_rev_goal) >= 85 && (100*$dashboard_data->current_por_rev_goal) <= 89.99 && (100*$dashboard_data->current_por_rev_goal) >= 85.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .72), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 22 && (100*$dashboard_data->current_month_gp_por)  < 22.99) && ((100*$dashboard_data->current_por_rev_goal) >= 85 && (100*$dashboard_data->current_por_rev_goal) <= 89.99 && (100*$dashboard_data->current_por_rev_goal) >= 85.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .80), 2,".",",")}}</td>
                                            <td @if( ((100*$dashboard_data->current_month_gp_por) > 23 && (100*$dashboard_data->current_month_gp_por)  < 23.99) && ( (100*$dashboard_data->current_por_rev_goal) >= 85 && (100*$dashboard_data->current_por_rev_goal) <= 89.99 && (100*$dashboard_data->current_por_rev_goal) >= 85.00)) class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .87), 2,".",",")}}</td>
                                            <td @if( (100*$dashboard_data->current_month_gp_por) > 24 && (100*$dashboard_data->current_month_gp_por)  < 24.99 && ((100*$dashboard_data->current_por_rev_goal) >= 85 && (100*$dashboard_data->current_por_rev_goal) <= 89.99 && (100*$dashboard_data->current_por_rev_goal) >= 85.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .94), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 25) && ((100*$dashboard_data->current_por_rev_goal) >= 85 && (100*$dashboard_data->current_por_rev_goal) <= 89.99 && (100*$dashboard_data->current_por_rev_goal) >= 85.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.02), 2,".",",")}}</td>
                                           </tr>
                                           
                                           
                                           
                                           <tr>
                                           	<td class="bg-warning" style="color:#fff">${{ number_format(($dashboard_data->current_month_goal * .90), 0,".",",") }}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 15 && (100*$dashboard_data->current_month_gp_por)  < 15.99) && ((100*$dashboard_data->current_por_rev_goal) >= 90 && (100*$dashboard_data->current_por_rev_goal) <= 94.99 && (100*$dashboard_data->current_por_rev_goal) >= 90.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .39), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 16 && (100*$dashboard_data->current_month_gp_por)  < 16.99) && ((100*$dashboard_data->current_por_rev_goal) >= 90 && (100*$dashboard_data->current_por_rev_goal) <= 94.99 && (100*$dashboard_data->current_por_rev_goal) >= 90.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .46), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 17 && (100*$dashboard_data->current_month_gp_por)  < 17.99) && ((100*$dashboard_data->current_por_rev_goal) >= 90 && (100*$dashboard_data->current_por_rev_goal) <= 94.99 && (100*$dashboard_data->current_por_rev_goal) >= 90.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .54), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 18 && (100*$dashboard_data->current_month_gp_por)  < 18.99) && ((100*$dashboard_data->current_por_rev_goal) >= 90 && (100*$dashboard_data->current_por_rev_goal) <= 94.99 && (100*$dashboard_data->current_por_rev_goal) >= 90.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .61), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 19 && (100*$dashboard_data->current_month_gp_por)  < 19.99) && ((100*$dashboard_data->current_por_rev_goal) >= 90 && (100*$dashboard_data->current_por_rev_goal) <= 94.99 && (100*$dashboard_data->current_por_rev_goal) >= 90.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .69), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 20 && (100*$dashboard_data->current_month_gp_por)  < 20.99) && ((100*$dashboard_data->current_por_rev_goal) >= 90 && (100*$dashboard_data->current_por_rev_goal) <= 94.99 && (100*$dashboard_data->current_por_rev_goal) >= 90.00))class="selected" @else  class="bg-info"@endif style="color:#fff">${{ number_format(($dashboard_data->monthly_target_incentive * .76), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 21 && (100*$dashboard_data->current_month_gp_por)  < 21.99) && ((100*$dashboard_data->current_por_rev_goal) >= 90 && (100*$dashboard_data->current_por_rev_goal) <= 94.99 && (100*$dashboard_data->current_por_rev_goal) >= 90.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .84), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 22 && (100*$dashboard_data->current_month_gp_por)  < 22.99) && ((100*$dashboard_data->current_por_rev_goal) >= 90 && (100*$dashboard_data->current_por_rev_goal) <= 94.99 && (100*$dashboard_data->current_por_rev_goal) >= 90.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .91), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 23 && (100*$dashboard_data->current_month_gp_por)  < 23.99) && ((100*$dashboard_data->current_por_rev_goal) >= 90 && (100*$dashboard_data->current_por_rev_goal) <= 94.99 && (100*$dashboard_data->current_por_rev_goal) >= 90.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .99), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 24 && (100*$dashboard_data->current_month_gp_por)  < 24.99) && ((100*$dashboard_data->current_por_rev_goal) >= 90 && (100*$dashboard_data->current_por_rev_goal) <= 94.99 && (100*$dashboard_data->current_por_rev_goal) >= 90.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.06), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 25) && ((100*$dashboard_data->current_por_rev_goal) >= 90 && (100*$dashboard_data->current_por_rev_goal) <= 94.99 && (100*$dashboard_data->current_por_rev_goal) >= 90.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.14), 2,".",",")}}</td>
                                           </tr>
                                           
                                           <tr>
                                           	<td class="bg-warning" style="color:#fff">${{ number_format(($dashboard_data->current_month_goal * .95), 0,".",",") }}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 15 && (100*$dashboard_data->current_month_gp_por)  < 15.99) && ((100*$dashboard_data->current_por_rev_goal) >= 95 && (100*$dashboard_data->current_por_rev_goal) <= 99.99 && (100*$dashboard_data->current_por_rev_goal) >= 95.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .46), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 16 && (100*$dashboard_data->current_month_gp_por)  < 16.99) && ((100*$dashboard_data->current_por_rev_goal) >= 95 && (100*$dashboard_data->current_por_rev_goal) <= 99.99 && (100*$dashboard_data->current_por_rev_goal) >= 95.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .54), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 17 && (100*$dashboard_data->current_month_gp_por)  < 17.99) && ((100*$dashboard_data->current_por_rev_goal) >= 95 && (100*$dashboard_data->current_por_rev_goal) <= 99.99 && (100*$dashboard_data->current_por_rev_goal) >= 95.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .63), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 18 && (100*$dashboard_data->current_month_gp_por)  < 18.99) && ((100*$dashboard_data->current_por_rev_goal) >= 95 && (100*$dashboard_data->current_por_rev_goal) <= 99.99 && (100*$dashboard_data->current_por_rev_goal) >= 95.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .71), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 19 && (100*$dashboard_data->current_month_gp_por)  < 19.99) && ((100*$dashboard_data->current_por_rev_goal) >= 95 && (100*$dashboard_data->current_por_rev_goal) <= 99.99 && (100*$dashboard_data->current_por_rev_goal) >= 95.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .80), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 20 && (100*$dashboard_data->current_month_gp_por)  < 20.99) && ((100*$dashboard_data->current_por_rev_goal) >= 95 && (100*$dashboard_data->current_por_rev_goal) <= 99.99 && (100*$dashboard_data->current_por_rev_goal) >= 95.00))class="selected" @else class="bg-info"@endif style="color:#fff">${{ number_format(($dashboard_data->monthly_target_incentive * .88), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 21 && (100*$dashboard_data->current_month_gp_por)  < 21.99) && ((100*$dashboard_data->current_por_rev_goal) >= 95 && (100*$dashboard_data->current_por_rev_goal) <= 99.99 && (100*$dashboard_data->current_por_rev_goal) >= 95.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .96), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 22 && (100*$dashboard_data->current_month_gp_por)  < 22.99) && ((100*$dashboard_data->current_por_rev_goal) >= 95 && (100*$dashboard_data->current_por_rev_goal) <= 99.99 && (100*$dashboard_data->current_por_rev_goal) >= 95.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.03), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 23 && (100*$dashboard_data->current_month_gp_por)  < 23.99) && ((100*$dashboard_data->current_por_rev_goal) >= 95 && (100*$dashboard_data->current_por_rev_goal) <= 99.99 && (100*$dashboard_data->current_por_rev_goal) >= 95.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.11), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 24 && (100*$dashboard_data->current_month_gp_por)  < 24.99) && ((100*$dashboard_data->current_por_rev_goal) >= 95 && (100*$dashboard_data->current_por_rev_goal) <= 99.99 && (100*$dashboard_data->current_por_rev_goal) >= 95.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.18), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 25) && ((100*$dashboard_data->current_por_rev_goal) >= 95 && (100*$dashboard_data->current_por_rev_goal) <= 99.99 && (100*$dashboard_data->current_por_rev_goal) >= 95.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.25), 2,".",",")}}</td>
                                           </tr>
                                           
                                           <tr>
                                           	<td class="bg-info" style="color:#fff">${{ number_format(($dashboard_data->current_month_goal * 1.0), 0,".",",") }}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 15 && (100*$dashboard_data->current_month_gp_por)  < 15.99) && ((100*$dashboard_data->current_por_rev_goal) >= 100 && (100*$dashboard_data->current_por_rev_goal) <= 104.99 && (100*$dashboard_data->current_por_rev_goal) >= 100.00))class="selected" @else class="bg-info"@endif style="color:#fff">${{ number_format(($dashboard_data->monthly_target_incentive * .53), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 16 && (100*$dashboard_data->current_month_gp_por)  < 16.99) && ((100*$dashboard_data->current_por_rev_goal) >= 100 && (100*$dashboard_data->current_por_rev_goal) <= 104.99 && (100*$dashboard_data->current_por_rev_goal) >= 100.00))class="selected" @else class="bg-info"@endif style="color:#fff">${{ number_format(($dashboard_data->monthly_target_incentive * .62), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 17 && (100*$dashboard_data->current_month_gp_por)  < 17.99) && ((100*$dashboard_data->current_por_rev_goal) >= 100 && (100*$dashboard_data->current_por_rev_goal) <= 104.99 && (100*$dashboard_data->current_por_rev_goal) >= 100.00))class="selected" @else class="bg-info"@endif style="color:#fff">${{ number_format(($dashboard_data->monthly_target_incentive * .72), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 18 && (100*$dashboard_data->current_month_gp_por)  < 18.99) && ((100*$dashboard_data->current_por_rev_goal) >= 100 && (100*$dashboard_data->current_por_rev_goal) <= 104.99 && (100*$dashboard_data->current_por_rev_goal) >= 100.00))class="selected" @else class="bg-info"@endif style="color:#fff">${{ number_format(($dashboard_data->monthly_target_incentive * .81), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 19 && (100*$dashboard_data->current_month_gp_por)  < 19.99) && ((100*$dashboard_data->current_por_rev_goal) >= 100 && (100*$dashboard_data->current_por_rev_goal) <= 104.99 && (100*$dashboard_data->current_por_rev_goal) >= 100.00))class="selected" @else class="bg-info"@endif style="color:#fff">${{ number_format(($dashboard_data->monthly_target_incentive * .91), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 20 && (100*$dashboard_data->current_month_gp_por)  < 20.99) && ((100*$dashboard_data->current_por_rev_goal) >= 100 && (100*$dashboard_data->current_por_rev_goal) <= 104.99 && (100*$dashboard_data->current_por_rev_goal) >= 100.00))class="selected" @else class="bg-info"@endif style="color:#fff">${{ number_format(($dashboard_data->monthly_target_incentive *  1.00), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 21 && (100*$dashboard_data->current_month_gp_por)  < 21.99) && ((100*$dashboard_data->current_por_rev_goal) >= 100 && (100*$dashboard_data->current_por_rev_goal) <= 104.99 && (100*$dashboard_data->current_por_rev_goal) >= 100.00))class="selected" @else class="bg-info"@endif style="color:#fff">${{ number_format(($dashboard_data->monthly_target_incentive *  1.07), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 22 && (100*$dashboard_data->current_month_gp_por)  < 22.99) && ((100*$dashboard_data->current_por_rev_goal) >= 100 && (100*$dashboard_data->current_por_rev_goal) <= 104.99 && (100*$dashboard_data->current_por_rev_goal) >= 100.00))class="selected" @else class="bg-info"@endif style="color:#fff">${{ number_format(($dashboard_data->monthly_target_incentive *  1.15), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 23 && (100*$dashboard_data->current_month_gp_por)  < 23.99) && ((100*$dashboard_data->current_por_rev_goal) >= 100 && (100*$dashboard_data->current_por_rev_goal) <= 104.99 && (100*$dashboard_data->current_por_rev_goal) >= 100.00))class="selected" @else class="bg-info"@endif style="color:#fff">${{ number_format(($dashboard_data->monthly_target_incentive *  1.22), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 24 && (100*$dashboard_data->current_month_gp_por)  < 24.99) && ((100*$dashboard_data->current_por_rev_goal) >= 100 && (100*$dashboard_data->current_por_rev_goal) <= 104.99 && (100*$dashboard_data->current_por_rev_goal) >= 100.00))class="selected" @else class="bg-info"@endif style="color:#fff">${{ number_format(($dashboard_data->monthly_target_incentive *  1.30), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 25) && ((100*$dashboard_data->current_por_rev_goal) >= 100 && (100*$dashboard_data->current_por_rev_goal) <= 104.99 && (100*$dashboard_data->current_por_rev_goal) >= 100.00))class="selected" @else class="bg-info" @endif style="color:#fff">${{ number_format(( $dashboard_data->monthly_target_incentive * 1.37), 2,".",",")}}</td>
                                           </tr>
                                           
                                           <tr>
                                           	<td class="bg-warning" style="color:#fff">${{ number_format(($dashboard_data->current_month_goal * 1.5), 0,".",",") }}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 15 && (100*$dashboard_data->current_month_gp_por)  < 15.99) && ((100*$dashboard_data->current_por_rev_goal) >= 105 && (100*$dashboard_data->current_por_rev_goal) <= 109.99 && (100*$dashboard_data->current_por_rev_goal) >= 105.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .62), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 15 && (100*$dashboard_data->current_month_gp_por)  < 15.99) && ((100*$dashboard_data->current_por_rev_goal) >= 105 && (100*$dashboard_data->current_por_rev_goal) <= 109.99 && (100*$dashboard_data->current_por_rev_goal) >= 105.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .72), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 15 && (100*$dashboard_data->current_month_gp_por)  < 15.99) && ((100*$dashboard_data->current_por_rev_goal) >= 105 && (100*$dashboard_data->current_por_rev_goal) <= 109.99 && (100*$dashboard_data->current_por_rev_goal) >= 105.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .81), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 15 && (100*$dashboard_data->current_month_gp_por)  < 15.99) && ((100*$dashboard_data->current_por_rev_goal) >= 105 && (100*$dashboard_data->current_por_rev_goal) <= 109.99 && (100*$dashboard_data->current_por_rev_goal) >= 105.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .90), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 15 && (100*$dashboard_data->current_month_gp_por)  < 15.99) && ((100*$dashboard_data->current_por_rev_goal) >= 105 && (100*$dashboard_data->current_por_rev_goal) <= 109.99 && (100*$dashboard_data->current_por_rev_goal) >= 105.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.00), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 15 && (100*$dashboard_data->current_month_gp_por)  < 15.99) && ((100*$dashboard_data->current_por_rev_goal) >= 105 && (100*$dashboard_data->current_por_rev_goal) <= 109.99 && (100*$dashboard_data->current_por_rev_goal) >= 105.00))class="selected" @else class="bg-info"@endif style="color:#fff">${{ number_format(($dashboard_data->monthly_target_incentive *  1.09), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 15 && (100*$dashboard_data->current_month_gp_por)  < 15.99) && ((100*$dashboard_data->current_por_rev_goal) >= 105 && (100*$dashboard_data->current_por_rev_goal) <= 109.99 && (100*$dashboard_data->current_por_rev_goal) >= 105.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.18), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 15 && (100*$dashboard_data->current_month_gp_por)  < 15.99) && ((100*$dashboard_data->current_por_rev_goal) >= 105 && (100*$dashboard_data->current_por_rev_goal) <= 109.99 && (100*$dashboard_data->current_por_rev_goal) >= 105.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.27), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 15 && (100*$dashboard_data->current_month_gp_por)  < 15.99) && ((100*$dashboard_data->current_por_rev_goal) >= 105 && (100*$dashboard_data->current_por_rev_goal) <= 109.99 && (100*$dashboard_data->current_por_rev_goal) >= 105.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.35), 2,".",",")}}</td>

                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 15 && (100*$dashboard_data->current_month_gp_por)  < 15.99) && ((100*$dashboard_data->current_por_rev_goal) >= 105 && (100*$dashboard_data->current_por_rev_goal) <= 109.99 && (100*$dashboard_data->current_por_rev_goal) >= 105.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.44), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 15 && (100*$dashboard_data->current_month_gp_por)  < 15.99) && ((100*$dashboard_data->current_por_rev_goal) >= 105 && (100*$dashboard_data->current_por_rev_goal) <= 109.99 && (100*$dashboard_data->current_por_rev_goal) >= 105.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.53), 2,".",",")}}</td>
                                           </tr>
                                           
                                           <tr>
                                           	<td class="bg-warning" style="color:#fff">${{ number_format(($dashboard_data->current_month_goal * 1.1), 0,".",",") }}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 15 && (100*$dashboard_data->current_month_gp_por)  < 15.99) && ((100*$dashboard_data->current_por_rev_goal) >= 110 && (100*$dashboard_data->current_por_rev_goal) <= 114.99 && (100*$dashboard_data->current_por_rev_goal) >= 110.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .71), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 16 && (100*$dashboard_data->current_month_gp_por)  < 16.99) && ((100*$dashboard_data->current_por_rev_goal) >= 110 && (100*$dashboard_data->current_por_rev_goal) <= 114.99 && (100*$dashboard_data->current_por_rev_goal) >= 110.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .81), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 17 && (100*$dashboard_data->current_month_gp_por)  < 17.99) && ((100*$dashboard_data->current_por_rev_goal) >= 110 && (100*$dashboard_data->current_por_rev_goal) <= 114.99 && (100*$dashboard_data->current_por_rev_goal) >= 110.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .90), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 18 && (100*$dashboard_data->current_month_gp_por)  < 18.99) && ((100*$dashboard_data->current_por_rev_goal) >= 110 && (100*$dashboard_data->current_por_rev_goal) <= 114.99 && (100*$dashboard_data->current_por_rev_goal) >= 110.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.00), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 19 && (100*$dashboard_data->current_month_gp_por)  < 19.99) && ((100*$dashboard_data->current_por_rev_goal) >= 110 && (100*$dashboard_data->current_por_rev_goal) <= 114.99 && (100*$dashboard_data->current_por_rev_goal) >= 110.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.09), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 20 && (100*$dashboard_data->current_month_gp_por)  < 20.99) && ((100*$dashboard_data->current_por_rev_goal) >= 110 && (100*$dashboard_data->current_por_rev_goal) <= 114.99 && (100*$dashboard_data->current_por_rev_goal) >= 110.00))class="selected" @else class="bg-info"@endif style="color:#fff">${{ number_format(($dashboard_data->monthly_target_incentive *  1.19), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 21 && (100*$dashboard_data->current_month_gp_por)  < 21.99) && ((100*$dashboard_data->current_por_rev_goal) >= 110 && (100*$dashboard_data->current_por_rev_goal) <= 114.99 && (100*$dashboard_data->current_por_rev_goal) >= 110.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.29), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 22 && (100*$dashboard_data->current_month_gp_por)  < 22.99) && ((100*$dashboard_data->current_por_rev_goal) >= 110 && (100*$dashboard_data->current_por_rev_goal) <= 114.99 && (100*$dashboard_data->current_por_rev_goal) >= 110.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.39), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 23 && (100*$dashboard_data->current_month_gp_por)  < 23.99) && ((100*$dashboard_data->current_por_rev_goal) >= 110 && (100*$dashboard_data->current_por_rev_goal) <= 114.99 && (100*$dashboard_data->current_por_rev_goal) >= 110.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.49), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 24 && (100*$dashboard_data->current_month_gp_por)  < 24.99) && ((100*$dashboard_data->current_por_rev_goal) >= 110 && (100*$dashboard_data->current_por_rev_goal) <= 114.99 && (100*$dashboard_data->current_por_rev_goal) >= 110.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.59), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 25) && ((100*$dashboard_data->current_por_rev_goal) >= 110 && (100*$dashboard_data->current_por_rev_goal) <= 114.99 && (100*$dashboard_data->current_por_rev_goal) >= 110.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.69), 2,".",",")}}</td>
                                           </tr>
                                           
                                           <tr>
                                           	<td class="bg-warning" style="color:#fff">${{ number_format(($dashboard_data->current_month_goal * 1.15), 0,".",",") }}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 15 && (100*$dashboard_data->current_month_gp_por)  < 15.99) && ((100*$dashboard_data->current_por_rev_goal) >= 115 && (100*$dashboard_data->current_por_rev_goal) <= 119.99 && (100*$dashboard_data->current_por_rev_goal) >= 115.00 ))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .81), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 16 && (100*$dashboard_data->current_month_gp_por)  < 16.99) && ((100*$dashboard_data->current_por_rev_goal) >= 115 && (100*$dashboard_data->current_por_rev_goal) <= 119.99 && (100*$dashboard_data->current_por_rev_goal) >= 115.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .90), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 17 && (100*$dashboard_data->current_month_gp_por)  < 17.99) && ((100*$dashboard_data->current_por_rev_goal) >= 115 && (100*$dashboard_data->current_por_rev_goal) <= 119.99 && (100*$dashboard_data->current_por_rev_goal) >= 115.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.00), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 18 && (100*$dashboard_data->current_month_gp_por)  < 18.99) && ((100*$dashboard_data->current_por_rev_goal) >= 115 && (100*$dashboard_data->current_por_rev_goal) <= 119.99 && (100*$dashboard_data->current_por_rev_goal) >= 115.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.09), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 19 && (100*$dashboard_data->current_month_gp_por)  < 19.99) && ((100*$dashboard_data->current_por_rev_goal) >= 115 && (100*$dashboard_data->current_por_rev_goal) <= 119.99 && (100*$dashboard_data->current_por_rev_goal) >= 115.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.18), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 20 && (100*$dashboard_data->current_month_gp_por)  < 20.99) && ((100*$dashboard_data->current_por_rev_goal) >= 115 && (100*$dashboard_data->current_por_rev_goal) <= 119.99 && (100*$dashboard_data->current_por_rev_goal) >= 115.00))class="selected" @else class="bg-info"@endif style="color:#fff">${{ number_format(($dashboard_data->monthly_target_incentive *  1.28), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 21 && (100*$dashboard_data->current_month_gp_por)  < 21.99) && ((100*$dashboard_data->current_por_rev_goal) >= 115 && (100*$dashboard_data->current_por_rev_goal) <= 119.99 && (100*$dashboard_data->current_por_rev_goal) >= 115.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.39), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 22 && (100*$dashboard_data->current_month_gp_por)  < 22.99) && ((100*$dashboard_data->current_por_rev_goal) >= 115 && (100*$dashboard_data->current_por_rev_goal) <= 119.99 && (100*$dashboard_data->current_por_rev_goal) >= 115.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.50), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 23 && (100*$dashboard_data->current_month_gp_por)  < 23.99) && ((100*$dashboard_data->current_por_rev_goal) >= 115 && (100*$dashboard_data->current_por_rev_goal) <= 119.99 && (100*$dashboard_data->current_por_rev_goal) >= 115.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.62), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 24 && (100*$dashboard_data->current_month_gp_por)  < 24.99) && ((100*$dashboard_data->current_por_rev_goal) >= 115 && (100*$dashboard_data->current_por_rev_goal) <= 119.99 && (100*$dashboard_data->current_por_rev_goal) >= 115.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.73), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 25) && ((100*$dashboard_data->current_por_rev_goal) >= 115 && (100*$dashboard_data->current_por_rev_goal) <= 119.99 && (100*$dashboard_data->current_por_rev_goal) >= 115.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.84), 2,".",",")}}</td>
                                           </tr>
                                           
                                           <tr>
                                           	<td class="bg-warning" style="color:#fff">${{ number_format(($dashboard_data->current_month_goal * 1.2), 0,".",",") }}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 15 && (100*$dashboard_data->current_month_gp_por)  < 15.99) && ((100*$dashboard_data->current_por_rev_goal) >= 120 && (100*$dashboard_data->current_por_rev_goal) <= 124.99 && (100*$dashboard_data->current_por_rev_goal) >= 120.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .90), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 16 && (100*$dashboard_data->current_month_gp_por)  < 16.99) && ((100*$dashboard_data->current_por_rev_goal) >= 120 && (100*$dashboard_data->current_por_rev_goal) <= 124.99 && (100*$dashboard_data->current_por_rev_goal) >= 120.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .99), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 17 && (100*$dashboard_data->current_month_gp_por)  < 17.99) && ((100*$dashboard_data->current_por_rev_goal) >= 120 && (100*$dashboard_data->current_por_rev_goal) <= 124.99 && (100*$dashboard_data->current_por_rev_goal) >= 120.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.09), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 18 && (100*$dashboard_data->current_month_gp_por)  < 18.99) && ((100*$dashboard_data->current_por_rev_goal) >= 120 && (100*$dashboard_data->current_por_rev_goal) <= 124.99 && (100*$dashboard_data->current_por_rev_goal) >= 120.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.18), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 19 && (100*$dashboard_data->current_month_gp_por)  < 19.99) && ((100*$dashboard_data->current_por_rev_goal) >= 120 && (100*$dashboard_data->current_por_rev_goal) <= 124.99 && (100*$dashboard_data->current_por_rev_goal) >= 120.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.28), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 20 && (100*$dashboard_data->current_month_gp_por)  < 20.99) && ((100*$dashboard_data->current_por_rev_goal) >= 120 && (100*$dashboard_data->current_por_rev_goal) <= 124.99 && (100*$dashboard_data->current_por_rev_goal) >= 120.00))class="selected"  @else class="bg-info" @endifstyle="color:#fff">${{number_format(($dashboard_data->monthly_target_incentive * 1.37), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 21 && (100*$dashboard_data->current_month_gp_por)  < 21.99) && ((100*$dashboard_data->current_por_rev_goal) >= 120 && (100*$dashboard_data->current_por_rev_goal) <= 124.99 && (100*$dashboard_data->current_por_rev_goal) >= 120.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.50), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 22 && (100*$dashboard_data->current_month_gp_por)  < 22.99) && ((100*$dashboard_data->current_por_rev_goal) >= 120 && (100*$dashboard_data->current_por_rev_goal) <= 124.99 && (100*$dashboard_data->current_por_rev_goal) >= 120.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.62), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 23 && (100*$dashboard_data->current_month_gp_por)  < 23.99) && ((100*$dashboard_data->current_por_rev_goal) >= 120 && (100*$dashboard_data->current_por_rev_goal) <= 124.99 && (100*$dashboard_data->current_por_rev_goal) >= 120.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.75), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 24 && (100*$dashboard_data->current_month_gp_por)  < 24.99) && ((100*$dashboard_data->current_por_rev_goal) >= 120 && (100*$dashboard_data->current_por_rev_goal) <= 124.99 && (100*$dashboard_data->current_por_rev_goal) >= 120.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.87), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 25) && ((100*$dashboard_data->current_por_rev_goal) >= 120 && (100*$dashboard_data->current_por_rev_goal) <= 124.99 && (100*$dashboard_data->current_por_rev_goal) >= 120.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.00), 2,".",",")}}</td>
                                           </tr>
                                           
                                           <tr>
                                           	<td class="bg-warning" style="color:#fff">${{ number_format(($dashboard_data->current_month_goal * 1.25), 0,".",",") }}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 15 && (100*$dashboard_data->current_month_gp_por)  < 15.99) && ((100*$dashboard_data->current_por_rev_goal) >= 125 && (100*$dashboard_data->current_por_rev_goal) <= 129.99 && (100*$dashboard_data->current_por_rev_goal) >= 125.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * .95), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 16 && (100*$dashboard_data->current_month_gp_por)  < 16.99) && ((100*$dashboard_data->current_por_rev_goal) >= 125 && (100*$dashboard_data->current_por_rev_goal) <= 129.99 && (100*$dashboard_data->current_por_rev_goal) >= 125.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.04), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 17 && (100*$dashboard_data->current_month_gp_por)  < 17.99) && ((100*$dashboard_data->current_por_rev_goal) >= 125 && (100*$dashboard_data->current_por_rev_goal) <= 129.99 && (100*$dashboard_data->current_por_rev_goal) >= 125.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.14), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 18 && (100*$dashboard_data->current_month_gp_por)  < 18.99) && ((100*$dashboard_data->current_por_rev_goal) >= 125 && (100*$dashboard_data->current_por_rev_goal) <= 129.99 && (100*$dashboard_data->current_por_rev_goal) >= 125.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.23), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 19 && (100*$dashboard_data->current_month_gp_por)  < 19.99) && ((100*$dashboard_data->current_por_rev_goal) >= 125 && (100*$dashboard_data->current_por_rev_goal) <= 129.99 && (100*$dashboard_data->current_por_rev_goal) >= 125.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.33), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 20 && (100*$dashboard_data->current_month_gp_por)  < 20.99) && ((100*$dashboard_data->current_por_rev_goal) >= 125 && (100*$dashboard_data->current_por_rev_goal) <= 129.99 && (100*$dashboard_data->current_por_rev_goal) >= 125.00))class="selected" @else class="bg-info"@endif style="color:#fff">${{ number_format(($dashboard_data->monthly_target_incentive *  1.42), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 21 && (100*$dashboard_data->current_month_gp_por)  < 21.99) && ((100*$dashboard_data->current_por_rev_goal) >= 125 && (100*$dashboard_data->current_por_rev_goal) <= 129.99 && (100*$dashboard_data->current_por_rev_goal) >= 125.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.55), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 22 && (100*$dashboard_data->current_month_gp_por)  < 22.99) && ((100*$dashboard_data->current_por_rev_goal) >= 125 && (100*$dashboard_data->current_por_rev_goal) <= 129.99 && (100*$dashboard_data->current_por_rev_goal) >= 125.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.68), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 23 && (100*$dashboard_data->current_month_gp_por)  < 23.99) && ((100*$dashboard_data->current_por_rev_goal) >= 125 && (100*$dashboard_data->current_por_rev_goal) <= 129.99 && (100*$dashboard_data->current_por_rev_goal) >= 125.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.82), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 24 && (100*$dashboard_data->current_month_gp_por)  < 24.99) && ((100*$dashboard_data->current_por_rev_goal) >= 125 && (100*$dashboard_data->current_por_rev_goal) <= 129.99 && (100*$dashboard_data->current_por_rev_goal) >= 125.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.94), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 25) && ((100*$dashboard_data->current_por_rev_goal) >= 125 && (100*$dashboard_data->current_por_rev_goal) <= 129.99 && (100*$dashboard_data->current_por_rev_goal) >= 125.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.08), 2,".",",")}}</td>
                                           </tr>
                                           
                                           <tr>
                                           	<td class="bg-warning" style="color:#fff">${{ number_format(($dashboard_data->current_month_goal * 1.3), 0,".",",") }}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 15 && (100*$dashboard_data->current_month_gp_por)  < 15.99) && ((100*$dashboard_data->current_por_rev_goal) >= 130 && (100*$dashboard_data->current_por_rev_goal) <= 134.99 && (100*$dashboard_data->current_por_rev_goal) >= 130.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.00), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 16 && (100*$dashboard_data->current_month_gp_por)  < 16.99) && ((100*$dashboard_data->current_por_rev_goal) >= 130 && (100*$dashboard_data->current_por_rev_goal) <= 134.99 && (100*$dashboard_data->current_por_rev_goal) >= 130.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.09), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 17 && (100*$dashboard_data->current_month_gp_por)  < 17.99) && ((100*$dashboard_data->current_por_rev_goal) >= 130 && (100*$dashboard_data->current_por_rev_goal) <= 134.99 && (100*$dashboard_data->current_por_rev_goal) >= 130.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.19), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 18 && (100*$dashboard_data->current_month_gp_por)  < 18.99) && ((100*$dashboard_data->current_por_rev_goal) >= 130 && (100*$dashboard_data->current_por_rev_goal) <= 134.99 && (100*$dashboard_data->current_por_rev_goal) >= 130.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.28), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 19 && (100*$dashboard_data->current_month_gp_por)  < 19.99) && ((100*$dashboard_data->current_por_rev_goal) >= 130 && (100*$dashboard_data->current_por_rev_goal) <= 134.99 && (100*$dashboard_data->current_por_rev_goal) >= 130.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.38), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 20 && (100*$dashboard_data->current_month_gp_por)  < 20.99) && ((100*$dashboard_data->current_por_rev_goal) >= 130 && (100*$dashboard_data->current_por_rev_goal) <= 134.99 && (100*$dashboard_data->current_por_rev_goal) >= 130.00))class="selected" @else class="bg-info"@endif style="color:#fff">${{ number_format(($dashboard_data->monthly_target_incentive *  1.47), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 21 && (100*$dashboard_data->current_month_gp_por)  < 21.99) && ((100*$dashboard_data->current_por_rev_goal) >= 130 && (100*$dashboard_data->current_por_rev_goal) <= 134.99 && (100*$dashboard_data->current_por_rev_goal) >= 130.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.60), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 22 && (100*$dashboard_data->current_month_gp_por)  < 22.99) && ((100*$dashboard_data->current_por_rev_goal) >= 130 && (100*$dashboard_data->current_por_rev_goal) <= 134.99 && (100*$dashboard_data->current_por_rev_goal) >= 130.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.74), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 23 && (100*$dashboard_data->current_month_gp_por)  < 23.99) && ((100*$dashboard_data->current_por_rev_goal) >= 130 && (100*$dashboard_data->current_por_rev_goal) <= 134.99 && (100*$dashboard_data->current_por_rev_goal) >= 130.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.89), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 24 && (100*$dashboard_data->current_month_gp_por)  < 24.99) && ((100*$dashboard_data->current_por_rev_goal) >= 130 && (100*$dashboard_data->current_por_rev_goal) <= 134.99 && (100*$dashboard_data->current_por_rev_goal) >= 130.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.01), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 25) && ((100*$dashboard_data->current_por_rev_goal) >= 130 && (100*$dashboard_data->current_por_rev_goal) <= 134.99 && (100*$dashboard_data->current_por_rev_goal) >= 130.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.16), 2,".",",")}}</td>
                                           </tr>
                                           
                                           <tr>
                                           	<td class="bg-warning" style="color:#fff">${{ number_format(($dashboard_data->current_month_goal * 1.35), 0,".",",") }}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 15 && (100*$dashboard_data->current_month_gp_por)  < 15.99) && ((100*$dashboard_data->current_por_rev_goal) >= 135 && (100*$dashboard_data->current_por_rev_goal) <= 139.99 && (100*$dashboard_data->current_por_rev_goal) >= 135.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.05), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 16 && (100*$dashboard_data->current_month_gp_por)  < 16.99) && ((100*$dashboard_data->current_por_rev_goal) >= 135 && (100*$dashboard_data->current_por_rev_goal) <= 139.99 && (100*$dashboard_data->current_por_rev_goal) >= 135.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.14), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 17 && (100*$dashboard_data->current_month_gp_por)  < 17.99) && ((100*$dashboard_data->current_por_rev_goal) >= 135 && (100*$dashboard_data->current_por_rev_goal) <= 139.99 && (100*$dashboard_data->current_por_rev_goal) >= 135.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.24), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 18 && (100*$dashboard_data->current_month_gp_por)  < 18.99) && ((100*$dashboard_data->current_por_rev_goal) >= 135 && (100*$dashboard_data->current_por_rev_goal) <= 139.99 && (100*$dashboard_data->current_por_rev_goal) >= 135.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.33), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 19 && (100*$dashboard_data->current_month_gp_por)  < 19.99) && ((100*$dashboard_data->current_por_rev_goal) >= 135 && (100*$dashboard_data->current_por_rev_goal) <= 139.99 && (100*$dashboard_data->current_por_rev_goal) >= 135.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.43), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 20 && (100*$dashboard_data->current_month_gp_por)  < 20.99) && ((100*$dashboard_data->current_por_rev_goal) >= 135 && (100*$dashboard_data->current_por_rev_goal) <= 139.99 && (100*$dashboard_data->current_por_rev_goal) >= 135.00))class="selected" @else class="bg-info" @endifstyle="color:#fff">${{number_format(($dashboard_data->monthly_target_incentive * 1.52), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 21 && (100*$dashboard_data->current_month_gp_por)  < 21.99) && ((100*$dashboard_data->current_por_rev_goal) >= 135 && (100*$dashboard_data->current_por_rev_goal) <= 139.99 && (100*$dashboard_data->current_por_rev_goal) >= 135.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.65), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 22 && (100*$dashboard_data->current_month_gp_por)  < 22.99) && ((100*$dashboard_data->current_por_rev_goal) >= 135 && (100*$dashboard_data->current_por_rev_goal) <= 139.99 && (100*$dashboard_data->current_por_rev_goal) >= 135.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.80), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 23 && (100*$dashboard_data->current_month_gp_por)  < 23.99) && ((100*$dashboard_data->current_por_rev_goal) >= 135 && (100*$dashboard_data->current_por_rev_goal) <= 139.99 && (100*$dashboard_data->current_por_rev_goal) >= 135.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.96), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 24 && (100*$dashboard_data->current_month_gp_por)  < 24.99) && ((100*$dashboard_data->current_por_rev_goal) >= 135 && (100*$dashboard_data->current_por_rev_goal) <= 139.99 && (100*$dashboard_data->current_por_rev_goal) >= 135.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.08), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 25) && ((100*$dashboard_data->current_por_rev_goal) >= 135 && (100*$dashboard_data->current_por_rev_goal) <= 139.99 && (100*$dashboard_data->current_por_rev_goal) >= 135.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.24), 2,".",",")}}</td>
                                           </tr>
                                           
                                           <tr>
                                           	<td class="bg-warning" style="color:#fff">${{ number_format(($dashboard_data->current_month_goal * 1.4), 0,".",",") }}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 15 && (100*$dashboard_data->current_month_gp_por)  < 15.99) && ((100*$dashboard_data->current_por_rev_goal) >= 140 && (100*$dashboard_data->current_por_rev_goal) <= 144.99 && (100*$dashboard_data->current_por_rev_goal) >= 140.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.10), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 16 && (100*$dashboard_data->current_month_gp_por)  < 16.99) && ((100*$dashboard_data->current_por_rev_goal) >= 140 && (100*$dashboard_data->current_por_rev_goal) <= 144.99 && (100*$dashboard_data->current_por_rev_goal) >= 140.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.19), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 17 && (100*$dashboard_data->current_month_gp_por)  < 17.99) && ((100*$dashboard_data->current_por_rev_goal) >= 140 && (100*$dashboard_data->current_por_rev_goal) <= 144.99 && (100*$dashboard_data->current_por_rev_goal) >= 140.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.29), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 18 && (100*$dashboard_data->current_month_gp_por)  < 18.99) && ((100*$dashboard_data->current_por_rev_goal) >= 140 && (100*$dashboard_data->current_por_rev_goal) <= 144.99 && (100*$dashboard_data->current_por_rev_goal) >= 140.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.38), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 19 && (100*$dashboard_data->current_month_gp_por)  < 19.99) && ((100*$dashboard_data->current_por_rev_goal) >= 140 && (100*$dashboard_data->current_por_rev_goal) <= 144.99 && (100*$dashboard_data->current_por_rev_goal) >= 140.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.48), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 20 && (100*$dashboard_data->current_month_gp_por)  < 20.99) && ((100*$dashboard_data->current_por_rev_goal) >= 140 && (100*$dashboard_data->current_por_rev_goal) <= 144.99 && (100*$dashboard_data->current_por_rev_goal) >= 140.00))class="selected" @else class="bg-info" @endif style="color:#fff">${{ number_format(($dashboard_data->monthly_target_incentive *  1.57), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 21 && (100*$dashboard_data->current_month_gp_por)  < 21.99) && ((100*$dashboard_data->current_por_rev_goal) >= 140 && (100*$dashboard_data->current_por_rev_goal) <= 144.99 && (100*$dashboard_data->current_por_rev_goal) >= 140.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.70), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 22 && (100*$dashboard_data->current_month_gp_por)  < 22.99) && ((100*$dashboard_data->current_por_rev_goal) >= 140 && (100*$dashboard_data->current_por_rev_goal) <= 144.99 && (100*$dashboard_data->current_por_rev_goal) >= 140.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.86), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 23 && (100*$dashboard_data->current_month_gp_por)  < 23.99) && ((100*$dashboard_data->current_por_rev_goal) >= 140 && (100*$dashboard_data->current_por_rev_goal) <= 144.99 && (100*$dashboard_data->current_por_rev_goal) >= 140.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.03), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 24 && (100*$dashboard_data->current_month_gp_por)  < 24.99) && ((100*$dashboard_data->current_por_rev_goal) >= 140 && (100*$dashboard_data->current_por_rev_goal) <= 144.99 && (100*$dashboard_data->current_por_rev_goal) >= 140.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.15), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 25) && ((100*$dashboard_data->current_por_rev_goal) >= 140 && (100*$dashboard_data->current_por_rev_goal) <= 144.99 && (100*$dashboard_data->current_por_rev_goal) >= 140.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.32), 2,".",",")}}</td>
                                           </tr>
                                           
                                           <tr>
                                           	<td class="bg-warning" style="color:#fff">${{ number_format(($dashboard_data->current_month_goal * 1.45), 0,".",",") }}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 15 && (100*$dashboard_data->current_month_gp_por)  < 15.99) && ((100*$dashboard_data->current_por_rev_goal) >= 145 && (100*$dashboard_data->current_por_rev_goal) <= 149.99 && (100*$dashboard_data->current_por_rev_goal) >= 145.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.15), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 16 && (100*$dashboard_data->current_month_gp_por)  < 16.99) && ((100*$dashboard_data->current_por_rev_goal) >= 145 && (100*$dashboard_data->current_por_rev_goal) <= 149.99 && (100*$dashboard_data->current_por_rev_goal) >= 145.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.24), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 17 && (100*$dashboard_data->current_month_gp_por)  < 17.99) && ((100*$dashboard_data->current_por_rev_goal) >= 145 && (100*$dashboard_data->current_por_rev_goal) <= 149.99 && (100*$dashboard_data->current_por_rev_goal) >= 145.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.34), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 18 && (100*$dashboard_data->current_month_gp_por)  < 18.99) && ((100*$dashboard_data->current_por_rev_goal) >= 145 && (100*$dashboard_data->current_por_rev_goal) <= 149.99 && (100*$dashboard_data->current_por_rev_goal) >= 145.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.43), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 19 && (100*$dashboard_data->current_month_gp_por)  < 19.99) && ((100*$dashboard_data->current_por_rev_goal) >= 145 && (100*$dashboard_data->current_por_rev_goal) <= 149.99 && (100*$dashboard_data->current_por_rev_goal) >= 145.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.53), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 20 && (100*$dashboard_data->current_month_gp_por)  < 20.99) && ((100*$dashboard_data->current_por_rev_goal) >= 145 && (100*$dashboard_data->current_por_rev_goal) <= 149.99 && (100*$dashboard_data->current_por_rev_goal) >= 145.00))class="selected" @else class="bg-info"@endif style="color:#fff">${{ number_format(($dashboard_data->monthly_target_incentive *  1.62), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 21 && (100*$dashboard_data->current_month_gp_por)  < 21.99) && ((100*$dashboard_data->current_por_rev_goal) >= 145 && (100*$dashboard_data->current_por_rev_goal) <= 149.99 && (100*$dashboard_data->current_por_rev_goal) >= 145.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.75), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 22 && (100*$dashboard_data->current_month_gp_por)  < 22.99) && ((100*$dashboard_data->current_por_rev_goal) >= 145 && (100*$dashboard_data->current_por_rev_goal) <= 149.99 && (100*$dashboard_data->current_por_rev_goal) >= 145.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.92), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 23 && (100*$dashboard_data->current_month_gp_por)  < 23.99) && ((100*$dashboard_data->current_por_rev_goal) >= 145 && (100*$dashboard_data->current_por_rev_goal) <= 149.99 && (100*$dashboard_data->current_por_rev_goal) >= 145.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.10), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 24 && (100*$dashboard_data->current_month_gp_por)  < 24.99) && ((100*$dashboard_data->current_por_rev_goal) >= 145 && (100*$dashboard_data->current_por_rev_goal) <= 149.99 && (100*$dashboard_data->current_por_rev_goal) >= 145.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.22), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 25) && ((100*$dashboard_data->current_por_rev_goal) >= 145 && (100*$dashboard_data->current_por_rev_goal) <= 149.99 && (100*$dashboard_data->current_por_rev_goal) >= 145.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.40), 2,".",",")}}</td>
                                           </tr>
                                           
                                           <tr>
                                           	<td class="bg-warning" style="color:#fff">${{ number_format(($dashboard_data->current_month_goal * 1.5), 0,".",",") }}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 15 && (100*$dashboard_data->current_month_gp_por)  < 15.99) && ((100*$dashboard_data->current_por_rev_goal) >= 150 && (100*$dashboard_data->current_por_rev_goal) <= 154.99 && (100*$dashboard_data->current_por_rev_goal) >= 150.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.20), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 16 && (100*$dashboard_data->current_month_gp_por)  < 16.99) && ((100*$dashboard_data->current_por_rev_goal) >= 150 && (100*$dashboard_data->current_por_rev_goal) <= 154.99 && (100*$dashboard_data->current_por_rev_goal) >= 150.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.29), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 17 && (100*$dashboard_data->current_month_gp_por)  < 17.99) && ((100*$dashboard_data->current_por_rev_goal) >= 150 && (100*$dashboard_data->current_por_rev_goal) <= 154.99 && (100*$dashboard_data->current_por_rev_goal) >= 150.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.39), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 18 && (100*$dashboard_data->current_month_gp_por)  < 18.99) && ((100*$dashboard_data->current_por_rev_goal) >= 150 && (100*$dashboard_data->current_por_rev_goal) <= 154.99 && (100*$dashboard_data->current_por_rev_goal) >= 150.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.48), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 19 && (100*$dashboard_data->current_month_gp_por)  < 19.99) && ((100*$dashboard_data->current_por_rev_goal) >= 150 && (100*$dashboard_data->current_por_rev_goal) <= 154.99 && (100*$dashboard_data->current_por_rev_goal) >= 150.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.58), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 20 && (100*$dashboard_data->current_month_gp_por)  < 20.99) && ((100*$dashboard_data->current_por_rev_goal) >= 150 && (100*$dashboard_data->current_por_rev_goal) <= 154.99 && (100*$dashboard_data->current_por_rev_goal) >= 150.00))class="selected" @else class="bg-info"@endif style="color:#fff">${{ number_format(($dashboard_data->monthly_target_incentive *  1.67), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 21 && (100*$dashboard_data->current_month_gp_por)  < 21.99) && ((100*$dashboard_data->current_por_rev_goal) >= 150 && (100*$dashboard_data->current_por_rev_goal) <= 154.99 && (100*$dashboard_data->current_por_rev_goal) >= 150.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.80), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 22 && (100*$dashboard_data->current_month_gp_por)  < 22.99) && ((100*$dashboard_data->current_por_rev_goal) >= 150 && (100*$dashboard_data->current_por_rev_goal) <= 154.99 && (100*$dashboard_data->current_por_rev_goal) >= 150.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.98), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 23 && (100*$dashboard_data->current_month_gp_por)  < 23.99) && ((100*$dashboard_data->current_por_rev_goal) >= 150 && (100*$dashboard_data->current_por_rev_goal) <= 154.99 && (100*$dashboard_data->current_por_rev_goal) >= 150.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.17), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 24 && (100*$dashboard_data->current_month_gp_por)  < 24.99) && ((100*$dashboard_data->current_por_rev_goal) >= 150 && (100*$dashboard_data->current_por_rev_goal) <= 154.99 && (100*$dashboard_data->current_por_rev_goal) >= 150.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.29), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 25) && ((100*$dashboard_data->current_por_rev_goal) >= 150 && (100*$dashboard_data->current_por_rev_goal) <= 154.99 && (100*$dashboard_data->current_por_rev_goal) >= 150.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.48), 2,".",",")}}</td>
                                           </tr>
                                           
                                           <tr>
                                           	<td class="bg-warning" style="color:#fff">${{ number_format(($dashboard_data->current_month_goal * 1.55), 0,".",",") }}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 15 && (100*$dashboard_data->current_month_gp_por)  < 15.99) && ((100*$dashboard_data->current_por_rev_goal) >= 155 && (100*$dashboard_data->current_por_rev_goal) <= 159.99 && (100*$dashboard_data->current_por_rev_goal) >= 155.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.25), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 16 && (100*$dashboard_data->current_month_gp_por)  < 16.99) && ((100*$dashboard_data->current_por_rev_goal) >= 155 && (100*$dashboard_data->current_por_rev_goal) <= 159.99 && (100*$dashboard_data->current_por_rev_goal) >= 155.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.34), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 17 && (100*$dashboard_data->current_month_gp_por)  < 17.99) && ((100*$dashboard_data->current_por_rev_goal) >= 155 && (100*$dashboard_data->current_por_rev_goal) <= 159.99 && (100*$dashboard_data->current_por_rev_goal) >= 155.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.44), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 18 && (100*$dashboard_data->current_month_gp_por)  < 18.99) && ((100*$dashboard_data->current_por_rev_goal) >= 155 && (100*$dashboard_data->current_por_rev_goal) <= 159.99 && (100*$dashboard_data->current_por_rev_goal) >= 155.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.53), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 19 && (100*$dashboard_data->current_month_gp_por)  < 19.99) && ((100*$dashboard_data->current_por_rev_goal) >= 155 && (100*$dashboard_data->current_por_rev_goal) <= 159.99 && (100*$dashboard_data->current_por_rev_goal) >= 155.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.63), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 20 && (100*$dashboard_data->current_month_gp_por)  < 20.99) && ((100*$dashboard_data->current_por_rev_goal) >= 155 && (100*$dashboard_data->current_por_rev_goal) <= 159.99 && (100*$dashboard_data->current_por_rev_goal) >= 155.00))class="selected" @else  class="bg-info"@endif style="color:#fff">${{ number_format(($dashboard_data->monthly_target_incentive *  1.72), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 21 && (100*$dashboard_data->current_month_gp_por)  < 21.99) && ((100*$dashboard_data->current_por_rev_goal) >= 155 && (100*$dashboard_data->current_por_rev_goal) <= 159.99 && (100*$dashboard_data->current_por_rev_goal) >= 155.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.85), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 22 && (100*$dashboard_data->current_month_gp_por)  < 22.99) && ((100*$dashboard_data->current_por_rev_goal) >= 155 && (100*$dashboard_data->current_por_rev_goal) <= 159.99 && (100*$dashboard_data->current_por_rev_goal) >= 155.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.04), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 23 && (100*$dashboard_data->current_month_gp_por)  < 23.99) && ((100*$dashboard_data->current_por_rev_goal) >= 155 && (100*$dashboard_data->current_por_rev_goal) <= 159.99 && (100*$dashboard_data->current_por_rev_goal) >= 155.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.24), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 24 && (100*$dashboard_data->current_month_gp_por)  < 24.99) && ((100*$dashboard_data->current_por_rev_goal) >= 155 && (100*$dashboard_data->current_por_rev_goal) <= 159.99 && (100*$dashboard_data->current_por_rev_goal) >= 155.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.36), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 25) && ((100*$dashboard_data->current_por_rev_goal) >= 155 && (100*$dashboard_data->current_por_rev_goal) <= 159.99 && (100*$dashboard_data->current_por_rev_goal) >= 155.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.56), 2,".",",")}}</td>
                                           </tr>
                                           
                                           <tr>
                                           	<td class="bg-warning" style="color:#fff">${{ number_format(($dashboard_data->current_month_goal * 1.60), 0,".",",") }}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 15 && (100*$dashboard_data->current_month_gp_por)  < 15.99) && ((100*$dashboard_data->current_por_rev_goal) >= 160 && (100*$dashboard_data->current_por_rev_goal) <= 164.99 && (100*$dashboard_data->current_por_rev_goal) >= 160.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.30), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 16 && (100*$dashboard_data->current_month_gp_por)  < 16.99) && ((100*$dashboard_data->current_por_rev_goal) >= 160 && (100*$dashboard_data->current_por_rev_goal) <= 164.99 && (100*$dashboard_data->current_por_rev_goal) >= 160.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.39), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 17 && (100*$dashboard_data->current_month_gp_por)  < 17.99) && ((100*$dashboard_data->current_por_rev_goal) >= 160 && (100*$dashboard_data->current_por_rev_goal) <= 164.99 && (100*$dashboard_data->current_por_rev_goal) >= 160.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.49), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 18 && (100*$dashboard_data->current_month_gp_por)  < 18.99) && ((100*$dashboard_data->current_por_rev_goal) >= 160 && (100*$dashboard_data->current_por_rev_goal) <= 164.99 && (100*$dashboard_data->current_por_rev_goal) >= 160.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.58), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 19 && (100*$dashboard_data->current_month_gp_por)  < 19.99) && ((100*$dashboard_data->current_por_rev_goal) >= 160 && (100*$dashboard_data->current_por_rev_goal) <= 164.99 && (100*$dashboard_data->current_por_rev_goal) >= 160.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.68), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 20 && (100*$dashboard_data->current_month_gp_por)  < 20.99) && ((100*$dashboard_data->current_por_rev_goal) >= 160 && (100*$dashboard_data->current_por_rev_goal) <= 164.99 && (100*$dashboard_data->current_por_rev_goal) >= 160.00))class="selected"@else  class="bg-info"@endif style="color:#fff">${{ number_format(($dashboard_data->monthly_target_incentive *  1.77), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 21 && (100*$dashboard_data->current_month_gp_por)  < 21.99) && ((100*$dashboard_data->current_por_rev_goal) >= 160 && (100*$dashboard_data->current_por_rev_goal) <= 164.99 && (100*$dashboard_data->current_por_rev_goal) >= 160.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.90), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 22 && (100*$dashboard_data->current_month_gp_por)  < 22.99) && ((100*$dashboard_data->current_por_rev_goal) >= 160 && (100*$dashboard_data->current_por_rev_goal) <= 164.99 && (100*$dashboard_data->current_por_rev_goal) >= 160.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.10), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 23 && (100*$dashboard_data->current_month_gp_por)  < 23.99) && ((100*$dashboard_data->current_por_rev_goal) >= 160 && (100*$dashboard_data->current_por_rev_goal) <= 164.99 && (100*$dashboard_data->current_por_rev_goal) >= 160.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.31), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 24 && (100*$dashboard_data->current_month_gp_por)  < 24.99) && ((100*$dashboard_data->current_por_rev_goal) >= 160 && (100*$dashboard_data->current_por_rev_goal) <= 164.99 && (100*$dashboard_data->current_por_rev_goal) >= 160.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.43), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 25) && ((100*$dashboard_data->current_por_rev_goal) >= 160 && (100*$dashboard_data->current_por_rev_goal) <= 164.99 && (100*$dashboard_data->current_por_rev_goal) >= 160.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.64), 2,".",",")}}</td>
                                           </tr>
                                           
                                           <tr>
                                           	<td class="bg-warning" style="color:#fff">${{ number_format(($dashboard_data->current_month_goal * 1.65), 0,".",",") }}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 15 && (100*$dashboard_data->current_month_gp_por)  < 15.99) && ((100*$dashboard_data->current_por_rev_goal) >= 165 && (100*$dashboard_data->current_por_rev_goal) <= 169.99 && (100*$dashboard_data->current_por_rev_goal) >= 165.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.35), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 16 && (100*$dashboard_data->current_month_gp_por)  < 16.99) && ((100*$dashboard_data->current_por_rev_goal) >= 165 && (100*$dashboard_data->current_por_rev_goal) <= 169.99 && (100*$dashboard_data->current_por_rev_goal) >= 165.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.44), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 17 && (100*$dashboard_data->current_month_gp_por)  < 17.99) && ((100*$dashboard_data->current_por_rev_goal) >= 165 && (100*$dashboard_data->current_por_rev_goal) <= 169.99 && (100*$dashboard_data->current_por_rev_goal) >= 165.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.54), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 18 && (100*$dashboard_data->current_month_gp_por)  < 18.99) && ((100*$dashboard_data->current_por_rev_goal) >= 165 && (100*$dashboard_data->current_por_rev_goal) <= 169.99 && (100*$dashboard_data->current_por_rev_goal) >= 165.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.63), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 19 && (100*$dashboard_data->current_month_gp_por)  < 19.99) && ((100*$dashboard_data->current_por_rev_goal) >= 165 && (100*$dashboard_data->current_por_rev_goal) <= 169.99 && (100*$dashboard_data->current_por_rev_goal) >= 165.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.73), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 20 && (100*$dashboard_data->current_month_gp_por)  < 20.99) && ((100*$dashboard_data->current_por_rev_goal) >= 165 && (100*$dashboard_data->current_por_rev_goal) <= 169.99 && (100*$dashboard_data->current_por_rev_goal) >= 165.00))class="selected" @else class="bg-info"@endif style="color:#fff">${{ number_format(($dashboard_data->monthly_target_incentive *  1.82), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 21 && (100*$dashboard_data->current_month_gp_por)  < 21.99) && ((100*$dashboard_data->current_por_rev_goal) >= 165 && (100*$dashboard_data->current_por_rev_goal) <= 169.99 && (100*$dashboard_data->current_por_rev_goal) >= 165.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.95), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 22 && (100*$dashboard_data->current_month_gp_por)  < 22.99) && ((100*$dashboard_data->current_por_rev_goal) >= 165 && (100*$dashboard_data->current_por_rev_goal) <= 169.99 && (100*$dashboard_data->current_por_rev_goal) >= 165.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.16), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 23 && (100*$dashboard_data->current_month_gp_por)  < 23.99) && ((100*$dashboard_data->current_por_rev_goal) >= 165 && (100*$dashboard_data->current_por_rev_goal) <= 169.99 && (100*$dashboard_data->current_por_rev_goal) >= 165.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.38), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 24 && (100*$dashboard_data->current_month_gp_por)  < 24.99) && ((100*$dashboard_data->current_por_rev_goal) >= 165 && (100*$dashboard_data->current_por_rev_goal) <= 169.99 && (100*$dashboard_data->current_por_rev_goal) >= 165.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.50), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 25) && ((100*$dashboard_data->current_por_rev_goal) >= 165 && (100*$dashboard_data->current_por_rev_goal) <= 169.99 && (100*$dashboard_data->current_por_rev_goal) >= 165.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.72), 2,".",",")}}</td>
                                           </tr>
                                           
                                           <tr>
                                           	<td class="bg-warning" style="color:#fff">${{ number_format(($dashboard_data->current_month_goal * 1.70), 0,".",",") }}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 15 && (100*$dashboard_data->current_month_gp_por)  < 15.99) && ((100*$dashboard_data->current_por_rev_goal) >= 170 && (100*$dashboard_data->current_por_rev_goal) <= 174.99 && (100*$dashboard_data->current_por_rev_goal) >= 170.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.40), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 16 && (100*$dashboard_data->current_month_gp_por)  < 16.99) && ((100*$dashboard_data->current_por_rev_goal) >= 170 && (100*$dashboard_data->current_por_rev_goal) <= 174.99 && (100*$dashboard_data->current_por_rev_goal) >= 170.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.49), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 17 && (100*$dashboard_data->current_month_gp_por)  < 17.99) && ((100*$dashboard_data->current_por_rev_goal) >= 170 && (100*$dashboard_data->current_por_rev_goal) <= 174.99 && (100*$dashboard_data->current_por_rev_goal) >= 170.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.59), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 18 && (100*$dashboard_data->current_month_gp_por)  < 18.99) && ((100*$dashboard_data->current_por_rev_goal) >= 170 && (100*$dashboard_data->current_por_rev_goal) <= 174.99 && (100*$dashboard_data->current_por_rev_goal) >= 170.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.68), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 19 && (100*$dashboard_data->current_month_gp_por)  < 19.99) && ((100*$dashboard_data->current_por_rev_goal) >= 170 && (100*$dashboard_data->current_por_rev_goal) <= 174.99 && (100*$dashboard_data->current_por_rev_goal) >= 170.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.78), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 20 && (100*$dashboard_data->current_month_gp_por)  < 20.99) && ((100*$dashboard_data->current_por_rev_goal) >= 170 && (100*$dashboard_data->current_por_rev_goal) <= 174.99 && (100*$dashboard_data->current_por_rev_goal) >= 170.00))class="selected"  @else class="bg-info"@endif style="color:#fff">${{ number_format(($dashboard_data->monthly_target_incentive *  1.87), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 21 && (100*$dashboard_data->current_month_gp_por)  < 21.99) && ((100*$dashboard_data->current_por_rev_goal) >= 170 && (100*$dashboard_data->current_por_rev_goal) <= 174.99 && (100*$dashboard_data->current_por_rev_goal) >= 170.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.00), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 22 && (100*$dashboard_data->current_month_gp_por)  < 22.99) && ((100*$dashboard_data->current_por_rev_goal) >= 170 && (100*$dashboard_data->current_por_rev_goal) <= 174.99 && (100*$dashboard_data->current_por_rev_goal) >= 170.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.22), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 23 && (100*$dashboard_data->current_month_gp_por)  < 23.99) && ((100*$dashboard_data->current_por_rev_goal) >= 170 && (100*$dashboard_data->current_por_rev_goal) <= 174.99 && (100*$dashboard_data->current_por_rev_goal) >= 170.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.45), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 24 && (100*$dashboard_data->current_month_gp_por)  < 24.99) && ((100*$dashboard_data->current_por_rev_goal) >= 170 && (100*$dashboard_data->current_por_rev_goal) <= 174.99 && (100*$dashboard_data->current_por_rev_goal) >= 170.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.57), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 25) && ((100*$dashboard_data->current_por_rev_goal) >= 170 && (100*$dashboard_data->current_por_rev_goal) <= 174.99 && (100*$dashboard_data->current_por_rev_goal) >= 170.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.80), 2,".",",")}}</td>
                                           </tr>
                                           
                                           <tr>
                                           	<td class="bg-warning" style="color:#fff">${{ number_format(($dashboard_data->current_month_goal * 1.75), 0,".",",") }}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 15 && (100*$dashboard_data->current_month_gp_por)  < 15.99) && ((100*$dashboard_data->current_por_rev_goal) >= 175 && (100*$dashboard_data->current_por_rev_goal) <= 179.99 && (100*$dashboard_data->current_por_rev_goal) >= 175.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.45), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 16 && (100*$dashboard_data->current_month_gp_por)  < 16.99) && ((100*$dashboard_data->current_por_rev_goal) >= 175 && (100*$dashboard_data->current_por_rev_goal) <= 179.99 && (100*$dashboard_data->current_por_rev_goal) >= 175.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.54), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 17 && (100*$dashboard_data->current_month_gp_por)  < 17.99) && ((100*$dashboard_data->current_por_rev_goal) >= 175 && (100*$dashboard_data->current_por_rev_goal) <= 179.99 && (100*$dashboard_data->current_por_rev_goal) >= 175.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.64), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 18 && (100*$dashboard_data->current_month_gp_por)  < 18.99) && ((100*$dashboard_data->current_por_rev_goal) >= 175 && (100*$dashboard_data->current_por_rev_goal) <= 179.99 && (100*$dashboard_data->current_por_rev_goal) >= 175.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.73), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 19 && (100*$dashboard_data->current_month_gp_por)  < 19.99) && ((100*$dashboard_data->current_por_rev_goal) >= 175 && (100*$dashboard_data->current_por_rev_goal) <= 179.99 && (100*$dashboard_data->current_por_rev_goal) >= 175.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.83), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 20 && (100*$dashboard_data->current_month_gp_por)  < 20.99) && ((100*$dashboard_data->current_por_rev_goal) >= 175 && (100*$dashboard_data->current_por_rev_goal) <= 179.99 && (100*$dashboard_data->current_por_rev_goal) >= 175.00))class="selected" @endif class="bg-info" style="color:#fff">${{number_format(($dashboard_data->monthly_target_incentive *  1.92), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 21 && (100*$dashboard_data->current_month_gp_por)  < 21.99) && ((100*$dashboard_data->current_por_rev_goal) >= 175 && (100*$dashboard_data->current_por_rev_goal) <= 179.99 && (100*$dashboard_data->current_por_rev_goal) >= 175.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.05), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 22 && (100*$dashboard_data->current_month_gp_por)  < 22.99) && ((100*$dashboard_data->current_por_rev_goal) >= 175 && (100*$dashboard_data->current_por_rev_goal) <= 179.99 && (100*$dashboard_data->current_por_rev_goal) >= 175.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.28), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 23 && (100*$dashboard_data->current_month_gp_por)  < 23.99) && ((100*$dashboard_data->current_por_rev_goal) >= 175 && (100*$dashboard_data->current_por_rev_goal) <= 179.99 && (100*$dashboard_data->current_por_rev_goal) >= 175.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.52), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 24 && (100*$dashboard_data->current_month_gp_por)  < 24.99) && ((100*$dashboard_data->current_por_rev_goal) >= 175 && (100*$dashboard_data->current_por_rev_goal) <= 179.99 && (100*$dashboard_data->current_por_rev_goal) >= 175.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.64), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 25) && ((100*$dashboard_data->current_por_rev_goal) >= 175 && (100*$dashboard_data->current_por_rev_goal) <= 179.99 && (100*$dashboard_data->current_por_rev_goal) >= 175.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.88), 2,".",",")}}</td>
                                           </tr>
                                           
                                           <tr>
                                           	<td class="bg-warning" style="color:#fff">${{ number_format(($dashboard_data->current_month_goal * 1.80), 0,".",",") }}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 15 && (100*$dashboard_data->current_month_gp_por)  < 15.99) && ((100*$dashboard_data->current_por_rev_goal) >= 180 && (100*$dashboard_data->current_por_rev_goal) <= 184.99 && (100*$dashboard_data->current_por_rev_goal) >= 180.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.50), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 16 && (100*$dashboard_data->current_month_gp_por)  < 16.99) && ((100*$dashboard_data->current_por_rev_goal) >= 180 && (100*$dashboard_data->current_por_rev_goal) <= 184.99 && (100*$dashboard_data->current_por_rev_goal) >= 180.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.59), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 17 && (100*$dashboard_data->current_month_gp_por)  < 17.99) && ((100*$dashboard_data->current_por_rev_goal) >= 180 && (100*$dashboard_data->current_por_rev_goal) <= 184.99 && (100*$dashboard_data->current_por_rev_goal) >= 180.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.69), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 18 && (100*$dashboard_data->current_month_gp_por)  < 18.99) && ((100*$dashboard_data->current_por_rev_goal) >= 180 && (100*$dashboard_data->current_por_rev_goal) <= 184.99 && (100*$dashboard_data->current_por_rev_goal) >= 180.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.78), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 19 && (100*$dashboard_data->current_month_gp_por)  < 19.99) && ((100*$dashboard_data->current_por_rev_goal) >= 180 && (100*$dashboard_data->current_por_rev_goal) <= 184.99 && (100*$dashboard_data->current_por_rev_goal) >= 180.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.88), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 20 && (100*$dashboard_data->current_month_gp_por)  < 20.99) && ((100*$dashboard_data->current_por_rev_goal) >= 180 && (100*$dashboard_data->current_por_rev_goal) <= 184.99 && (100*$dashboard_data->current_por_rev_goal) >= 180.00))class="selected" @else class="bg-info"@endif style="color:#fff">${{ number_format(($dashboard_data->monthly_target_incentive *  1.97), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 21 && (100*$dashboard_data->current_month_gp_por)  < 21.99) && ((100*$dashboard_data->current_por_rev_goal) >= 180 && (100*$dashboard_data->current_por_rev_goal) <= 184.99 && (100*$dashboard_data->current_por_rev_goal) >= 180.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.10), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 22 && (100*$dashboard_data->current_month_gp_por)  < 22.99) && ((100*$dashboard_data->current_por_rev_goal) >= 180 && (100*$dashboard_data->current_por_rev_goal) <= 184.99 && (100*$dashboard_data->current_por_rev_goal) >= 180.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.34), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 23 && (100*$dashboard_data->current_month_gp_por)  < 23.99) && ((100*$dashboard_data->current_por_rev_goal) >= 180 && (100*$dashboard_data->current_por_rev_goal) <= 184.99 && (100*$dashboard_data->current_por_rev_goal) >= 180.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.59), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 24 && (100*$dashboard_data->current_month_gp_por)  < 24.99) && ((100*$dashboard_data->current_por_rev_goal) >= 180 && (100*$dashboard_data->current_por_rev_goal) <= 184.99 && (100*$dashboard_data->current_por_rev_goal) >= 180.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.71), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 25) && ((100*$dashboard_data->current_por_rev_goal) >= 180 && (100*$dashboard_data->current_por_rev_goal) <= 184.99 && (100*$dashboard_data->current_por_rev_goal) >= 180.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.96), 2,".",",")}}</td>
                                           </tr>
                                           
                                           <tr>
                                           	<td class="bg-warning" style="color:#fff">${{ number_format(($dashboard_data->current_month_goal * 1.85), 0,".",",") }}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 15 && (100*$dashboard_data->current_month_gp_por)  < 15.99) && ((100*$dashboard_data->current_por_rev_goal) >= 185 && (100*$dashboard_data->current_por_rev_goal) <= 189.99 && (100*$dashboard_data->current_por_rev_goal) >= 185.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.55), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 16 && (100*$dashboard_data->current_month_gp_por)  < 16.99) && ((100*$dashboard_data->current_por_rev_goal) >= 185 && (100*$dashboard_data->current_por_rev_goal) <= 189.99 && (100*$dashboard_data->current_por_rev_goal) >= 185.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.64), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 17 && (100*$dashboard_data->current_month_gp_por)  < 17.99) && ((100*$dashboard_data->current_por_rev_goal) >= 185 && (100*$dashboard_data->current_por_rev_goal) <= 189.99 && (100*$dashboard_data->current_por_rev_goal) >= 185.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.74), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 18 && (100*$dashboard_data->current_month_gp_por)  < 18.99) && ((100*$dashboard_data->current_por_rev_goal) >= 185 && (100*$dashboard_data->current_por_rev_goal) <= 189.99 && (100*$dashboard_data->current_por_rev_goal) >= 185.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.83), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 19 && (100*$dashboard_data->current_month_gp_por)  < 19.99) && ((100*$dashboard_data->current_por_rev_goal) >= 185 && (100*$dashboard_data->current_por_rev_goal) <= 189.99 && (100*$dashboard_data->current_por_rev_goal) >= 185.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.93), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 20 && (100*$dashboard_data->current_month_gp_por)  < 20.99) && ((100*$dashboard_data->current_por_rev_goal) >= 185 && (100*$dashboard_data->current_por_rev_goal) <= 189.99 && (100*$dashboard_data->current_por_rev_goal) >= 185.00))class="selected" @endif class="bg-info" style="color:#fff">${{number_format(($dashboard_data->monthly_target_incentive * 2.02), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 21 && (100*$dashboard_data->current_month_gp_por)  < 21.99) && ((100*$dashboard_data->current_por_rev_goal) >= 185 && (100*$dashboard_data->current_por_rev_goal) <= 189.99 && (100*$dashboard_data->current_por_rev_goal) >= 185.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.15), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 22 && (100*$dashboard_data->current_month_gp_por)  < 22.99) && ((100*$dashboard_data->current_por_rev_goal) >= 185 && (100*$dashboard_data->current_por_rev_goal) <= 189.99 && (100*$dashboard_data->current_por_rev_goal) >= 185.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.40), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 23 && (100*$dashboard_data->current_month_gp_por)  < 23.99) && ((100*$dashboard_data->current_por_rev_goal) >= 185 && (100*$dashboard_data->current_por_rev_goal) <= 189.99 && (100*$dashboard_data->current_por_rev_goal) >= 185.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.66), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 24 && (100*$dashboard_data->current_month_gp_por)  < 24.99) && ((100*$dashboard_data->current_por_rev_goal) >= 185 && (100*$dashboard_data->current_por_rev_goal) <= 189.99 && (100*$dashboard_data->current_por_rev_goal) >= 185.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.78), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 25) && ((100*$dashboard_data->current_por_rev_goal) >= 185 && (100*$dashboard_data->current_por_rev_goal) <= 189.99 && (100*$dashboard_data->current_por_rev_goal) >= 185.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 3.04), 2,".",",")}}</td>
                                           </tr>
                                           
                                           <tr>
                                           	<td class="bg-warning" style="color:#fff">${{ number_format(($dashboard_data->current_month_goal * 1.9), 0,".",",") }}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 15 && (100*$dashboard_data->current_month_gp_por)  < 15.99) && ((100*$dashboard_data->current_por_rev_goal) >= 190 && (100*$dashboard_data->current_por_rev_goal) <= 194.99 && (100*$dashboard_data->current_por_rev_goal) >= 190.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.60), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 16 && (100*$dashboard_data->current_month_gp_por)  < 16.99) && ((100*$dashboard_data->current_por_rev_goal) >= 190 && (100*$dashboard_data->current_por_rev_goal) <= 194.99 && (100*$dashboard_data->current_por_rev_goal) >= 190.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.69), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 17 && (100*$dashboard_data->current_month_gp_por)  < 17.99) && ((100*$dashboard_data->current_por_rev_goal) >= 190 && (100*$dashboard_data->current_por_rev_goal) <= 194.99 && (100*$dashboard_data->current_por_rev_goal) >= 190.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.79), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 18 && (100*$dashboard_data->current_month_gp_por)  < 18.99) && ((100*$dashboard_data->current_por_rev_goal) >= 190 && (100*$dashboard_data->current_por_rev_goal) <= 194.99 && (100*$dashboard_data->current_por_rev_goal) >= 190.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.88), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 19 && (100*$dashboard_data->current_month_gp_por)  < 19.99) && ((100*$dashboard_data->current_por_rev_goal) >= 190 && (100*$dashboard_data->current_por_rev_goal) <= 194.99 && (100*$dashboard_data->current_por_rev_goal) >= 190.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.98), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 20 && (100*$dashboard_data->current_month_gp_por)  < 20.99) && ((100*$dashboard_data->current_por_rev_goal) >= 190 && (100*$dashboard_data->current_por_rev_goal) <= 194.99 && (100*$dashboard_data->current_por_rev_goal) >= 190.00))class="selected" @endif class="bg-info" style="color:#fff">${{number_format(($dashboard_data->monthly_target_incentive * 2.07), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 21 && (100*$dashboard_data->current_month_gp_por)  < 21.99) && ((100*$dashboard_data->current_por_rev_goal) >= 190 && (100*$dashboard_data->current_por_rev_goal) <= 194.99 && (100*$dashboard_data->current_por_rev_goal) >= 190.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.20), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 22 && (100*$dashboard_data->current_month_gp_por)  < 22.99) && ((100*$dashboard_data->current_por_rev_goal) >= 190 && (100*$dashboard_data->current_por_rev_goal) <= 194.99 && (100*$dashboard_data->current_por_rev_goal) >= 190.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.46), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 23 && (100*$dashboard_data->current_month_gp_por)  < 23.99) && ((100*$dashboard_data->current_por_rev_goal) >= 190 && (100*$dashboard_data->current_por_rev_goal) <= 194.99 && (100*$dashboard_data->current_por_rev_goal) >= 190.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.73), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 24 && (100*$dashboard_data->current_month_gp_por)  < 24.99) && ((100*$dashboard_data->current_por_rev_goal) >= 190 && (100*$dashboard_data->current_por_rev_goal) <= 194.99 && (100*$dashboard_data->current_por_rev_goal) >= 190.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.85), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 25) && ((100*$dashboard_data->current_por_rev_goal) >= 190 && (100*$dashboard_data->current_por_rev_goal) <= 194.99 && (100*$dashboard_data->current_por_rev_goal) >= 190.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 3.12), 2,".",",")}}</td>
                                           </tr>
                                           
                                           <tr>
                                           	<td class="bg-warning" style="color:#fff">${{ number_format(($dashboard_data->current_month_goal * 1.95), 0,".",",") }}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 15 && (100*$dashboard_data->current_month_gp_por)  < 15.99) && ((100*$dashboard_data->current_por_rev_goal) >= 195 && (100*$dashboard_data->current_por_rev_goal) <= 199.99 && (100*$dashboard_data->current_por_rev_goal) >= 195.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.65), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 16 && (100*$dashboard_data->current_month_gp_por)  < 16.99) && ((100*$dashboard_data->current_por_rev_goal) >= 195 && (100*$dashboard_data->current_por_rev_goal) <= 199.99 && (100*$dashboard_data->current_por_rev_goal) >= 195.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.74), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 17 && (100*$dashboard_data->current_month_gp_por)  < 17.99) && ((100*$dashboard_data->current_por_rev_goal) >= 195 && (100*$dashboard_data->current_por_rev_goal) <= 199.99 && (100*$dashboard_data->current_por_rev_goal) >= 195.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.84), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 18 && (100*$dashboard_data->current_month_gp_por)  < 18.99) && ((100*$dashboard_data->current_por_rev_goal) >= 195 && (100*$dashboard_data->current_por_rev_goal) <= 199.99 && (100*$dashboard_data->current_por_rev_goal) >= 195.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.93), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 19 && (100*$dashboard_data->current_month_gp_por)  < 19.99) && ((100*$dashboard_data->current_por_rev_goal) >= 195 && (100*$dashboard_data->current_por_rev_goal) <= 199.99 && (100*$dashboard_data->current_por_rev_goal) >= 195.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.03), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 20 && (100*$dashboard_data->current_month_gp_por)  < 20.99) && ((100*$dashboard_data->current_por_rev_goal) >= 195 && (100*$dashboard_data->current_por_rev_goal) <= 199.99 && (100*$dashboard_data->current_por_rev_goal) >= 195.00))class="selected" @else class="bg-info" @endif style="color:#fff">${{ number_format(($dashboard_data->monthly_target_incentive * 2.12), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 21 && (100*$dashboard_data->current_month_gp_por)  < 21.99) && ((100*$dashboard_data->current_por_rev_goal) >= 195 && (100*$dashboard_data->current_por_rev_goal) <= 199.99 && (100*$dashboard_data->current_por_rev_goal) >= 195.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.25), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 22 && (100*$dashboard_data->current_month_gp_por)  < 22.99) && ((100*$dashboard_data->current_por_rev_goal) >= 195 && (100*$dashboard_data->current_por_rev_goal) <= 199.99 && (100*$dashboard_data->current_por_rev_goal) >= 195.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.52), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 23 && (100*$dashboard_data->current_month_gp_por)  < 23.99) && ((100*$dashboard_data->current_por_rev_goal) >= 195 && (100*$dashboard_data->current_por_rev_goal) <= 199.99 && (100*$dashboard_data->current_por_rev_goal) >= 195.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.80), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 24 && (100*$dashboard_data->current_month_gp_por)  < 24.99) && ((100*$dashboard_data->current_por_rev_goal) >= 195 && (100*$dashboard_data->current_por_rev_goal) <= 199.99 && (100*$dashboard_data->current_por_rev_goal) >= 195.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.92), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 25) && ((100*$dashboard_data->current_por_rev_goal) >= 195 && (100*$dashboard_data->current_por_rev_goal) <= 199.99 && (100*$dashboard_data->current_por_rev_goal) >= 195.00))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 3.20), 2,".",",")}}</td>
                                           </tr>
                                           
                                           <tr>
                                           	<td class="bg-warning" style="color:#fff">${{ number_format(($dashboard_data->current_month_goal * 2.0), 0,".",",") }}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 15 && (100*$dashboard_data->current_month_gp_por)  < 15.99) && ((100*$dashboard_data->current_por_rev_goal) >= 200  ))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.70), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 16 && (100*$dashboard_data->current_month_gp_por)  < 16.99) && ((100*$dashboard_data->current_por_rev_goal) >= 200 ))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.79), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 17 && (100*$dashboard_data->current_month_gp_por)  < 17.99) && ((100*$dashboard_data->current_por_rev_goal) >= 200 ))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.89), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 18 && (100*$dashboard_data->current_month_gp_por)  < 18.99) && ((100*$dashboard_data->current_por_rev_goal) >= 200 ))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive *  1.98), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 19 && (100*$dashboard_data->current_month_gp_por)  < 19.99) && ((100*$dashboard_data->current_por_rev_goal) >= 200 ))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.08), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 20 && (100*$dashboard_data->current_month_gp_por)  < 20.99) && ((100*$dashboard_data->current_por_rev_goal) >= 200 ))class="selected" @else class="bg-info" @endif style="color:#fff">${{ number_format(($dashboard_data->monthly_target_incentive * 2.17), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 21 && (100*$dashboard_data->current_month_gp_por)  < 21.99) && ((100*$dashboard_data->current_por_rev_goal) >= 200 ))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.30), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 22 && (100*$dashboard_data->current_month_gp_por)  < 22.99) && ((100*$dashboard_data->current_por_rev_goal) >= 200 ))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.58), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 23 && (100*$dashboard_data->current_month_gp_por)  < 23.99) && ((100*$dashboard_data->current_por_rev_goal) >= 200 ))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.87), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 24 && (100*$dashboard_data->current_month_gp_por)  < 24.99) && ((100*$dashboard_data->current_por_rev_goal) >= 200 ))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 2.99), 2,".",",")}}</td>
                                            <td @if(( (100*$dashboard_data->current_month_gp_por) > 25) && ((100*$dashboard_data->current_por_rev_goal) >= 200 ))class="selected" @endif>${{ number_format(($dashboard_data->monthly_target_incentive * 3.28), 2,".",",")}}</td>
                                           </tr>
                                           
                                          
                                        </table>
                            
                           
  									</div><!-- end panel body-->
  								</div><!-- end panel primary-->
                                
                           		
                        </div><!-- end col-->
                       </div><!-- end row-->
                        
                      
                        
                         <div class="row" style="background-color:#2F343B; padding-top:20px">
                            <div class="col-md-4">
  								<div class="panel panel-primary">
  									<div class="panel-heading">
  										<h3 class="panel-title">
  											<i class="fa fa-bar-chart-o"></i>
  											Quaterly Performance
  											<span class="pull-right">
  												<a  href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a>
  												
  											</span>
  										</h3>
  									</div><!-- end panel heading-->
  									<div class="panel-body nopadding popovers">
  										<div class="list-group list-statistics">
  											<a href="#" class="list-group-item">
  												My Quaterly Sales
  												<span class="badge bg-success">${{ number_format($dashboard_data->quarter_sales, "2",".",",") }}</span>
  											</a>
  											<a href="#" class="list-group-item" >
  												My Quaterly Margin
  												<span class="badge bg-success">${{ number_format($dashboard_data->quarter_margin, "2",".",",") }}</span>
  											</a>
  											<a href="#" class="list-group-item">
  												My Quaterly Gross Profit %
  												<span class="badge bg-success">{{ 100*$dashboard_data->quarter_gp_por }}%</span>
  											</a>
  										</div><!--end list-->
  									</div><!-- end panel body-->
  								</div><!-- end panel primary-->	
  							</div><!-- end col-->
                            
                            <div class="col-md-4">
  								<div class="panel panel-primary">
  									<div class="panel-heading">
  										<h3 class="panel-title">
  											<i class="fa fa-bar-chart-o"></i>
  											Company Performance <a class="panel-title chart" href="{{ url('users/copchart')}}">Chart</a>
  											<span class="pull-right">
  												<a  href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a>
  												
  											</span>
  										</h3>
  									</div><!-- end panel heading-->
  									<div class="panel-body nopadding">
  										<div class="col-md-6">
                                        <script type="text/javascript">
											var chartCoPiece1 = <?php echo json_encode($chartCoPiece1_data); ?>
										</script>
                                        
										  <div id="chartCoPiece1" style="height:160px;"></div>
                                          <center>Current Revenue: ${{ number_format($dashboard_data->zmac_qtr_rev, 0, ",", ",")}}</center>
                                          <center>Goal: ${{ number_format($dashboard_data->zmac_qtr_goal, 0, ",", ",")}}</center>
                                        </div><!-- end col md 6-->
                                        
                                        <div class="col-md-6">
                                          <div id="chartCoPiece2" style="height:160px;"></div>
                                         	<script type="text/javascript">
												var chartCoPiece2 = <?php echo json_encode($chartCoPiece2_data); ?>
											</script>
                                           
										</div><!-- end col md 6-->
  									</div><!-- end panel body-->
  								</div><!-- end panel-->	
  							</div><!-- end row-->
                            <div class="col-md-4">
  								<div class="panel panel-primary">
  									<div class="panel-heading">
  										<h3 class="panel-title">
  											<i class="fa fa-bar-chart-o"></i>
  											Bank <a class="panel-title chart" href="{{ url('users/copchart')}}">Chart</a>
  											<span class="pull-right">
  												<a  href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a>
  												

  											</span>
  										</h3>
  									</div><!-- end panel heading-->
  									<div class="panel-body">
                                    	
                                      <a href="#">GP Dollar Level</a> <a href="#" class="btn btn-danger btn-md btn-animate-demo pull-right">${{ number_format($dashboard_data->bank_gp_dollar_level, "2",".",",") }}</a><br />
                                      <br class="clearfix" style="clear:both;"/>
                                      <a href="{{ url('users/copchart')}}">Next Threshold</a> <a href="#" class="btn btn-danger btn-md btn-animate-demo pull-right">${{ number_format($dashboard_data->bank_next_thrshld, "2",".",",") }}</a><br />
                                      <br class="clearfix" style="clear:both;"/>
                                      <a href="#">Amount to Next Threshold</a> <a href="#" class="btn btn-danger btn-md btn-animate-demo pull-right">${{ number_format($dashboard_data->bank_amount_to_next_threshold, "2",".",",") }}</a><br />
                                    
  									</div><!-- end panel body-->
  								</div><!-- end paneo primary-->	
  							</div><!-- end col md 4-->
  						</div><!-- end row-->

  						
<script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
{{HTML::script('js/jquery-ui-1.10.3.custom.min.js');}}
{{HTML::script('js/less-1.5.0.min.js');}}
{{HTML::script('js/jquery.ui.touch-punch.min.js');}}
{{HTML::script('js/bootstrap.min.js');}}
{{HTML::script('js/bootstrap-select.js');}}
{{HTML::script('js/bootstrap-switch.js');}}
{{HTML::script('js/jquery.tagsinput.js');}}
{{HTML::script('js/jquery.placeholder.js');}}


<!-- Load JS here for Faster site load =============================-->


<script src="../../js/bootstrap-typeahead.js"></script>
<script src="../../js/application.js"></script>
<script src="../../js/moment.min.js"></script>
<script src="../../js/jquery.dataTables.min.js"></script>
<script src="../../js/jquery.sortable.js"></script>
<script type="text/javascript" src="../../js/jquery.gritter.js"></script>
<script src="../../js/jquery.nicescroll.min.js"></script>
<script src=../../"js/prettify.min.js"></script>
<script src="../../js/jquery.noty.js"></script>
<script src="../../js/bic_calendar.js"></script>
<script src="../../js/jquery.accordion.js"></script>
<script src="../../js/skylo.js"></script>

<script src="../../js/theme-options.js"></script>


<script src="../../js/bootstrap-progressbar.js"></script>
<script src="../../js/bootstrap-progressbar-custom.js"></script>
<script src="../../js/bootstrap-colorpicker.min.js"></script>
<script src="../../js/bootstrap-colorpicker-custom.js"></script>



<script src="../../js/raphael-min.js"></script>
<script src="../../js/morris-0.4.3.min.js"></script>
<script src="../../js/morris-custom-3.js"></script>

<script src="../../js/charts/jquery.sparkline.min.js"></script>	



<script src="../../js/bootstrap-tour.js"></script>

<!-- Page script File  =============================-->
<script src="../../js/tooltips-popovers.js"></script>

<!-- AMDCharts ================================================-->
<script src="../../js/amcharts/amcharts.js" type="text/javascript"></script>
<script src="../../js/amcharts/serial.js" type="text/javascript"></script>
<script type="text/javascript">
		var chartData = <?php echo json_encode($chart_data); ?>
</script>

<script src="../../js/amcharts/custom-ce.js" type="text/javascript"></script>


<!-- Core Jquery File  =============================-->
<script src="../../js/core.js"></script>
<script src="../../js/dashboard-custom.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/less.js/1.5.0/less.min.js"></script>
  <script src="../../js/bootstrap-datatables.js"></script>
<script src="../../js/dataTables-custom.js"></script>
@stop