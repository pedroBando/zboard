@extends('layouts.main')

@section('content')
<div class="content"> 
	
    
    <div class="row">
  							<div class="col-mod-12">
								<!-- start breadcrumbs -->
  								<ul class="breadcrumb">
  									<li><a href="url('users.dashboard')">Dashboard</a></li>
                                    <li><a href="url('users.history')">History</a></li>
  								</ul><!-- end breadc-->

  								<div class="form-group hiddn-minibar pull-right">
  								

  								
  								</div><!-- end form group search-->

  								<h3 class="page-header"><i class="fa fa fa-dashboard"></i> History <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>

  								<blockquote class="page-information hidden">
  									<p>
  										<b>History Page</b> is the page where you can see details about your past paid bounties, bank, goals, and company piece.
  									</p>
  								</blockquote>
  							</div><!-- end col-md-12 -->
  						</div><!-- end row -->
						
                        <!-- Info Boxes -->
  						<div class="row panel-body" style="background-color:#2F343B">
  							<div class="col-md-4">
  								<div class="info-box  bg-info  text-white">
  									<div class="info-icon bg-info-dark">
  										<i class="fa fa-dollar fa-4x"></i>
  									</div>
  									<div class="info-details">
  										<h4>Total Paid LV1   <span class="pull-right">${{ number_format($dashboard_data->levelone_ytd, 0,".",",") }}
										  </span></h4>
  										<p style="visibility:hidden">Current Month <span class="badge pull-right bg-white text-info"> $<?php $number = $dashboard_data->onemonthago_revenue;
										  // english notation (default)
										  echo number_format($number, 2,'.', ',');?></span> </p>
  									</div>
  								</div><!-- end info box-->
  							</div><!-- end col-md4-->
  							<div class="col-md-4 ">
  								<div class="info-box  bg-success  text-white">
  									<div class="info-icon bg-success-dark">
  										<i class="fa fa-dollar fa-4x"></i>
  									</div>
  									<div class="info-details">
  										<h4>Total Paid LV2    <span class="pull-right">${{ number_format($dashboard_data->leveltwo_ytd, 0,".",",") }}</span></h4>
  										<p style="visibility:hidden">Current Month<span class="badge pull-right bg-white text-success"> {{ number_format($dashboard_data->onemonthago_gp_por*100) }}% </span> </p>
  									</div>
  								</div><!-- end info box-->
  							</div><!-- end col-md4-->
  							<div class="col-md-4">
  								<div class="info-box  bg-warning  text-white">
  									<div class="info-icon bg-warning-dark">
  										<i class="fa fa-dollar fa-4x"></i>
  									</div>
  									<div class="info-details">
  										<h4>Total Paid LV3   <span class="pull-right">${{ number_format($dashboard_data->levelthree_ytd, 0,".",",") }}</span></h4>
  										<p style="visibility:hidden">Current Month <span class="badge pull-right bg-white text-warning">{{ $dashboard_data->total_customers }}</span></p>
  									</div>
  								</div><!-- end info box-->
  							</div><!-- end col md 4-->
  						</div><!-- end row-->

  						<!-- Demo Panel -->

                        <div class="row">
  							<!--- Pie chart -->
  							<div class="col-md-4" style="background-color:#29a2d7; min-height:850px">
                                 <div class="panel-heading">
  										<h3 class="panel-title text-white">
  											Bounty Level 1
  										</h3>
  								 </div>	
                                 <!--<div id="bounty1"></div>-->
                                 <!-- Most visited and Traffic sources Graph -->
          
                       
                            <div class="panel">
                             <div class="panel-heading text-primary">
                              <h3 class="panel-title">
                               <i class="fa fa-pencil-square"></i>
                              Paid Level 1 
                              </h3>
                             </div><!-- end panel heading-->

                        <div class="panel-body" style="min-height:600px;">
                          <div class="alert alert-info alert-dismissable">
                           <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                           With a working search box, pagination ,ascending and descending buttons, you are able to navigate through your Level 1 bounty eligibles customers.
                         </div>
                         <table class="table table-bordered table-hover table-striped display" id="examplee" >
                           <thead>
                            <tr>
                             <th>Customers</th>
                             <th>Controller</th>
                             <th>Paid</th>
                           </tr>
                         </thead>
                         <tbody class="tooltips">
                              @foreach($customerData as $cData)
                              @if ($cData->l1_paid == 50)
                        	<tr>
                              <td><a href="{{url('/customers/l1history/' .$cData->customerid.'/'.$dashboard_data->userid)}}">{{ $cData->customername}}</a></td>
                              <td><a href="" data-original-title="{{ $cData->cp_contact}} {{ $cData->cp_phone}} | {{ $cData->cp_city}}, {{ $cData->cp_state}}" data-placement="left">{{ $cData->customerid}}</a></td>
                              <td><span class="label bg-info">${{$cData->l1_paid}}</span></td>
                            </tr>
                        	@endif
                        @endforeach				
                           </tbody>
                           <tfoot>
                            <tr>
                             <th>Customer Name</th>
                             <th>Controller</th>
                             <th>Paid</th>
                           </tr>	
                         </tfoot>
                       </table>
                     </div> <!-- /panel body -->
                   </div><!-- end panel-->
                 </div> <!-- /col-md-4 -->
                        
  							
                        
                            <div class="col-md-4" style="background-color:#77af3b; min-height:850px">
                                        <div class="panel-heading">
                                                    <h3 class="panel-title text-white">
                                                        Bounty Level 2
                                                    </h3>
                                        </div><!-- end panel heading-->
  										<!-- <div id="bounty2"></div> -->
                                        <div class="panel">
                             <div class="panel-heading text-primary">
                              <h3 class="panel-title">
                               <i class="fa fa-pencil-square"></i>
                              Paid Level 2  
                             </h3>
                            </div><!-- end panel heading-->
                        <div class="panel-body" style="min-height:600px;">
                          <div class="alert alert-info alert-dismissable">
                           <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                           With a working search box, pagination ,ascending and descending buttons, you are able to navigate through your Level 1 bounty eligibles customers.
                         </div>
                         <table  class="table table-bordered table-hover table-striped display tooltips" id="example2" >
                           <thead>
                            <tr>
                             <th>Customer Name</th>
                             <th>Controller</th>
                             <th>Paid</th>
                           </tr>
                         </thead>
                         <tbody>
                              @foreach($customerData as $cData)
                              @if ($cData->l2_paid > 0)
                        	<tr>
                              <td><a href="{{url('/customers/l2history/' .$cData->customerid.'/'.$dashboard_data->userid)}}">{{ $cData->customername}}</td>
                              <td><a href="" data-original-title="{{ $cData->cp_contact}} {{ $cData->cp_phone}} | {{ $cData->cp_city}}, {{ $cData->cp_state}}" data-placement="left">{{ $cData->customerid}}</a></td>
                              <td><span class="label bg-info ">${{$cData->l2_paid}}</span></td>
                            </tr>
                        	@endif
                        @endforeach			
                           </tbody>
                           <tfoot>
                            <tr>
                             <th>Customer Name</th>
                             <th>Controller</th>
                             <th>Paid</th>
                           </tr>	
                         </tfoot>
                       </table>
                     </div> <!-- /panel body -->
                   </div><!-- end panel-->
  				</div><!-- end col md 4-->
                            <div class="col-md-4" style="background-color:#f79219; min-height:850px">
                                        <div class="panel-heading">
                                                    <h3 class="panel-title text-white">
                                                        Bounty Level 3
                                                    </h3>
                                        </div>
  										<!-- <div id="bounty3"></div>-->
                                        
                                        <div class="panel">
                             <div class="panel-heading text-primary">
                              <h3 class="panel-title">
                               <i class="fa fa-pencil-square"></i>
                               Paid Level 3 
                             </h3>
                            </div><!-- end panel heading-->
                        <div class="panel-body" style="min-height:600px;">
                          <div class="alert alert-info alert-dismissable">
                           <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                           With a working search box, pagination ,ascending and descending buttons, you are able to navigate through your Level 1 bounty eligibles customers.
                         </div>
                         <table class="table table-bordered table-hover table-striped display tooltips" id="example3" >
                           <thead>
                            <tr>
                             <th>Customer Name</th>
                             <th>Controller</th>
                             <th>Paid</th>
                           </tr>
                         </thead>
                         <tbody>
                         
                        @foreach($customerData as $cData)
                              @if ($cData->l3_paid > 0)
                        	<tr>
                              <td><a href="{{url('/customers/l3history/' .$cData->customerid.'/'.$dashboard_data->userid)}}">{{ $cData->customername}}</td>
                              <td><a href="" data-original-title="{{ $cData->cp_contact}} {{ $cData->cp_phone}} | {{ $cData->cp_city}}, {{ $cData->cp_state}}" data-placement="left">{{ $cData->customerid}}</a></td>
                              <td><span class="label @if ($cData->l3_paid == '0') bg-danger @else  bg-info @endif">@if ($cData->l3_paid == 0) No @else ${{$cData->l3_paid}}@endif</span></td>
                            </tr>
                        	@endif
                        @endforeach
                          
                              

                         </tfoot>
                         <tfoot>
                            <tr>
                             <th>Customer Name</th>
                             <th>Controller</th>
                             <th>Paid</th>
                           </tr>	
                         </tfoot>
                       </table>
                     </div> <!-- /panel body -->
  							</div><!-- end panel-->
                            
  						</div><!-- end col md 4-->
                        </div><!-- end row-->
                        <div class="row" >
                        	<div class="col-md-6" style="margin-top:30px;">
  								<div class="info-box  bg-info  text-white">
  									<div class="info-icon bg-primary-dark">
  										<i class="fa fa-dollar fa-4x"></i>
  									</div>
  									<div class="info-details">
  										<h4>Quarter Gross Profit %   <span class="pull-right">{{number_format(($dashboard_data->quarter_gp_por * 100),"2",".",",")}}%</span></h4>
  										<p>Last Quarter <span class="badge pull-right bg-success text-white">${{number_format($dashboard_data->last_quarter_sales,"2",".",",")}}</span> </p>
  									</div>
  								</div>
  							</div>
                            <div class="col-md-6" style="margin-top:30px;">
  								<div class="info-box  bg-success  text-white">
  									<div class="info-icon bg-success-dark">
  										<i class="fa fa-dollar fa-4x"></i>
  									</div>
  									<div class="info-details">
  										<h4>Profit Margin (Quarter)   <span class="pull-right">${{number_format($dashboard_data->quarter_margin,"2",".",",")}}</span></h4>
  										<p>Last Quarter <span class="badge pull-right bg-dark-primary text-white"> ${{number_format($dashboard_data->last_quarter_margin,"2",".",",")}}  </span> </p>
  									</div>
  								</div><!-- end info box-->
  							</div><!-- end col-->
                        </div><!-- end row-->
                        
                        <!-- start co piece and bank history--><div class="row">
	<!-- start bank histopry--><div class="col-md-6" style="background-color:#29a2d7;min-height:500px;">
		<div class="panel-heading text-primary">
            	<h3 class="panel-title text-white">
                	<i class="fa fa-dollar"></i>
                    Quaterly Bank Paid
                </h3>
            </div><!-- end panel heading-->
    	<div class="panel">
        	<div class="panel-heading text-primary">
            	<h3 class="panel-title">
                	
                    Payouts
                </h3>
            </div><!-- end panel heading-->
       <div class="panel-body">
       	<table class="table table-bordered table-hover table-striped display" id="baHistory" >
            	<thead>
                    <th>Date</th>
                    <th>Payout</th>
                </thead>
                
                <tbody>
                
                @foreach($idata as $dati)
                @if($dati->quaterly_bank_paid > 0)
                	<tr>
                        <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d', $dati->date)->toFormattedDateString();}}</td>
                        <td>${{ number_format($dati->quaterly_bank_paid)}}</td>
                    </tr>
                    @endif
                @endforeach
                </tbody>
                
                <tfoot>
                    <th>Period</th>
                    <th>Incentive</th>
                </tfoot>
    	</table>

       </div><!-- end panel body-->
       </div><!-- end panel-->
    
    </div><!-- end bank history-->
    <!-- start copiece histopry--><div class="col-md-6" style="background-color:#F9A94A;min-height:500px;">
    <div class="panel-heading text-primary">
            	<h3 class="panel-title text-white">
                	<i class="fa fa-dollar"></i>
                    Company Performance <a class="panel-title chart" href="{{ url('users/copchart')}}">Chart</a>
                </h3>
            </div><!-- end panel heading-->
    	<div class="panel">
        	<div class="panel-heading text-primary">
            	<h3 class="panel-title">
                	
                    Payouts
                </h3>
            </div><!-- end panel heading-->
       <div class="panel-body">
       	<table class="table table-bordered table-hover table-striped display" id="coHistory" >
            	<thead>
                    <th>Period</th>
                    <th>Incentive</th>
                </thead>
                
                <tbody>
                
                @foreach($idatac as $dati)
                	<tr>
                        <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d', $dati->date)->toFormattedDateString();}}</td>
                        <td>${{ number_format($dati->qtr_company_incentive)}}</td>
                    </tr>
                @endforeach
                </tbody>
                
                <tfoot>
                    <th>Period</th>
                    <th>Incentive</th>
                </tfoot>
    	</table>

       </div><!-- end panel body-->
       </div><!-- end panel-->
    
    </div><!-- end copiece history-->
    
    

</div><!-- end row-->
                        
                        

                        
  					</div> <!-- /.content -->

 						
<script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
{{HTML::script('js/jquery-ui-1.10.3.custom.min.js');}}
{{HTML::script('js/less-1.5.0.min.js');}}
{{HTML::script('js/jquery.ui.touch-punch.min.js');}}
{{HTML::script('js/bootstrap.min.js');}}
{{HTML::script('js/bootstrap-select.js');}}
{{HTML::script('js/bootstrap-switch.js');}}
{{HTML::script('js/jquery.tagsinput.js');}}
{{HTML::script('js/jquery.placeholder.js');}}


<!-- Load JS here for Faster site load =============================-->


<script src="../../js/bootstrap-typeahead.js"></script>
<script src="../../js/application.js"></script>
<script src="../../js/moment.min.js"></script>
<script src="../../js/jquery.dataTables.min.js"></script>
<script src="../../js/jquery.sortable.js"></script>
<script type="text/javascript" src="../../js/jquery.gritter.js"></script>
<script src="../../js/jquery.nicescroll.min.js"></script>
<script src=../../"js/prettify.min.js"></script>
<script src="../../js/jquery.noty.js"></script>
<script src="../../js/bic_calendar.js"></script>
<script src="../../js/jquery.accordion.js"></script>
<script src="../../js/skylo.js"></script>

<script src="../../js/theme-options.js"></script>


<script src="../../js/bootstrap-progressbar.js"></script>
<script src="../../js/bootstrap-progressbar-custom.js"></script>
<script src="../../js/bootstrap-colorpicker.min.js"></script>
<script src="../../js/bootstrap-colorpicker-custom.js"></script>



<script src="../../js/raphael-min.js"></script>
<script src="../../js/morris-0.4.3.min.js"></script>
<script src="../../js/morris-custom.js"></script>
<script src="../../js/morris-custom-2.js"></script>

<script src="../../js/charts/jquery.sparkline.min.js"></script>	

<!-- NVD3 graphs  =============================-->
<script src="../../js/nvd3/lib/d3.v3.js"></script>
<script src="../../js/nvd3/nv.d3.js"></script>
<script src="../../js/nvd3/src/models/legend.js"></script>
<script src="../../js/nvd3/src/models/pie.js"></script>
<script src="../../js/nvd3/src/models/pieChart.js"></script>
<script src="../../js/nvd3/src/utils.js"></script>
<script src="../../js/nvd3/sample.nvd3.js"></script>
<script src="../../js/nvd3/nvd3-custom.js"></script>

<script src="../../js/bootstrap-tour.js"></script>


<!-- AMDCharts ================================================-->
<script src="../../js/amcharts/amcharts.js" type="text/javascript"></script>
<script src="../../js/amcharts/serial.js" type="text/javascript"></script>
<script src="../../js/amcharts/custom.js" type="text/javascript"></script>


<!-- Core Jquery File  =============================-->
<script src="../../js/core.js"></script>
<script src="../../js/dashboard-custom.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/less.js/1.5.0/less.min.js"></script>
  <script src="../../js/bootstrap-datatables.js"></script>
<script src="../../js/dataTables-custom.js"></script>
@stop