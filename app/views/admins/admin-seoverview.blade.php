@extends('layouts.main')

@section('content')
<div class="content"> 
	
    
    <div class="row">
  							<div class="col-mod-12">
								<!-- start breadcrumbs -->
  								<ul class="breadcrumb">
  									<li><a href="url('users.dashboard')">Dashboard</a></li>
                                    <li><a href="url('users.overview')">Overview</a></li>
  								</ul><!-- end breadc-->

  								<div class="form-group hiddn-minibar pull-right">
  								

  								
  								</div><!-- end form group search-->

  								<h3 class="page-header"><i class="fa fa fa-dashboard"></i> Overview <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>

  								<blockquote class="page-information hidden">
  									<p>
  										<b>Overview Page</b> is the page where you can see details about your bounties, bank, goals, and company piece.
  									</p>
  								</blockquote>
  							</div><!-- end col-md-12 -->
  						</div><!-- end row -->


  						<!-- Demo Panel -->

                        <div class="row">
  							<!--- start bounty level 1 -->
  							<div class="col-md-4" style="background-color:#29a2d7;min-height:900px;">
                                 <div class="panel-heading">
  										<h3 class="panel-title text-white">
  											Bounty Level 1
  										</h3>
  								 </div>	
                                 <!--<div id="bounty1"></div>-->
                                 <!-- Most visited and Traffic sources Graph -->
          
                       
                            <div class="panel">
                             <div class="panel-heading text-primary">
                              <h3 class="panel-title">
                               <i class="fa fa-pencil-square"></i>
                               Eligible Customer
                               
                          </h3>
                        </div>
                        <div class="panel-body">
                          <div class="alert alert-info alert-dismissable">
                           <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                           Requirements: Produce at least $5,000 resulting in at least 15% GP%.
                         </div>
                         <table class="table table-bordered table-hover table-striped display" id="example" >
                           <thead>
                            <tr>
                             <th style="cursor:pointer">Eligible Customers</th>
                             <th style="cursor:pointer">CP Code</th>
                             <th style="cursor:pointer">Paid</th>
                             <th style="cursor:pointer">Amount to Bounty at 25GP%</th>
                           </tr>
                         </thead>
                         <tbody class="tooltips">
                              @foreach($levelone as $cData)
                              @if ($cData->l1_eligible == "yes")
                        	<tr>
                              <td><a href="{{url('/customers/' .$cData->customerid)}}">{{ $cData->customername}}</a></td>
                              <td><a href="{{url('/customers/' .$cData->customerid)}}" data-original-title="{{ $cData->cp_contact}} {{ $cData->cp_phone}} | {{ $cData->cp_city}}, {{ $cData->cp_state}}" data-placement="left">{{ $cData->customerid}}</a></td>
                              <td><span class="label @if ($cData->l1_paid == '0') bg-danger @else  bg-info @endif">@if ($cData->l1_paid == 0) No @else ${{$cData->l1_paid}}@endif</span></td>
                              <td>${{ number_format($cData->level1_rev_reqd_at_25gp_por, 2,  ".", ",") }}</td>
                            </tr>
                        	@endif
                        @endforeach				
                           </tbody>
                           <tfoot>
                            <tr>
                             <th style="cursor:pointer">Eligible Customers</th>
                             <th style="cursor:pointer">CP Code</th>
                             <th style="cursor:pointer">Paid</th>
                             <th style="cursor:pointer">Amount to Bounty at 25GP%</th>
                           </tr>	
                         </tfoot>
                       </table>
                     </div> <!-- /panel body -->
                   </div><!-- end panel-->
                 </div> <!-- end col-->
                        
  							
                        
                  <!-- start bounty level 2-->
                  <div class="col-md-4" style="background-color:#77af3b;min-height:900px;">
                                        <div class="panel-heading">
                                                    <h3 class="panel-title text-white">
                                                        Bounty Level 2
                                                    </h3>
                                        </div>
  										<!-- <div id="bounty2"></div> -->
                                        <div class="panel">
                             <div class="panel-heading text-primary">
                              <h3 class="panel-title">
                               <i class="fa fa-pencil-square"></i>
                               Eligible Customers
                               
                          </h3>
                        </div>
                        <div class="panel-body">
                          <div class="alert alert-info alert-dismissable">
                           <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                           Requirements: Produce at least $20,000 resulting in at least 19% GP%.
                         </div>
                         <table  class="table table-bordered table-hover table-striped display tooltips" id="example2" >
                           <thead>
                            <tr>
                             <th style="cursor:pointer">Eligible Customers</th>
                             <th style="cursor:pointer">CP Code</th>
                             <th style="cursor:pointer">Paid</th>
                             <th style="cursor:pointer">Amount to Bounty at 25GP%</th>
                           </tr>
                         </thead>
                         <tbody>
                              @foreach($leveltwo as $cData)
                              @if ($cData->level12_revenue < 20000 && $cData->l2_eligible == 'yes')
                        	<tr>
                              <td><a href="{{url('/customers/' .$cData->customerid)}}">{{ $cData->customername}}</td>
                              <td><a href="" data-original-title="{{ $cData->cp_contact}} {{ $cData->cp_phone}} | {{ $cData->cp_city}}, {{ $cData->cp_state}}" data-placement="left">{{ $cData->customerid}}</a></td>
                              <td><span class="label @if ($cData->l2_paid == '0') bg-danger @else  bg-info @endif">@if ($cData->l2_paid == 0) No @else ${{$cData->l2_paid}}@endif</span></td>
                              <td>${{ number_format($cData->level2_rev_reqd_at_25gp_por, 2,  ".", ",") }}</td>
                            </tr>
                        	@endif
                        @endforeach			
                           </tbody>
                           <tfoot>
                            <tr>
                             <th style="cursor:pointer">Eligible Customers</th>
                             <th style="cursor:pointer">CP Code</th>
                             <th style="cursor:pointer">Paid</th>
                             <th style="cursor:pointer">Amount to Bounty at 25GP%</th>
                           </tr>	
                         </tfoot>
                       </table>
                     </div> <!-- /panel body -->
                   </div><!-- end panel-->
  				</div><!-- end col-->
                
                <!-- start bounty level 3-->
                <div class="col-md-4" style="background-color:#f79219;min-height:900px;">
                                        <div class="panel-heading">
                                                    <h3 class="panel-title text-white">
                                                        Bounty Level 3
                                                    </h3>
                                        </div>
  										
                                        
                                        <div class="panel">
                             <div class="panel-heading text-primary">
                              <h3 class="panel-title">
                               <i class="fa fa-pencil-square"></i>
                               Eligible Customers
                               
                          </h3>
                        </div>
                        <div class="panel-body">
                          <div class="alert alert-info alert-dismissable">
                           <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                           Requirements: Produce at least $50,000. 
                         </div>
                         <table  class="table table-bordered table-hover table-striped display tooltips" id="example3" >
                           <thead>
                            <tr>
                             <th style="cursor:pointer">Eligible Customers</th>
                             <th style="cursor:pointer">CP Code</th>
                             <th style="cursor:pointer">Paid</th>
                             <th style="cursor:pointer">Amount to Bounty at 25GP%</th>
                           </tr>
                         </thead>
                         <tbody>
                         
                        @foreach($levelthree as $cData)
                         @if ($cData->lthree_eligible == 'yes')
                        	<tr>
                              <td><a href="{{url('/customers/' .$cData->customerid)}}">{{ $cData->customername}}</td>
                              <td><a href="" data-original-title="{{ $cData->cp_contact}} {{ $cData->cp_phone}} | {{ $cData->cp_city}}, {{ $cData->cp_state}}" data-placement="left">{{ $cData->customerid}}</a></td>
                              <td><span class="label @if ($cData->l3_paid == '0') bg-danger @else  bg-info @endif">@if ($cData->l3_paid == 0) No @else ${{$cData->l3_paid}}@endif</span></td>
                              <td>${{ number_format($cData->level3_rev_reqd_at_25gp_por, 2,  ".", ",") }}</td>
                            </tr>
                        	@endif
                        @endforeach
                          
                              

                         </tfoot>
                         <tfoot>
                            <tr>
                             <th style="cursor:pointer">Eligible Customers</th>
                             <th style="cursor:pointer">CP Code</th>
                             <th style="cursor:pointer">Paid</th>
                             <th style="cursor:pointer">Amount to Bounty at 25GP%</th>
                           </tr>	
                         </tfoot>
                       </table>
                     </div> <!-- /panel body -->
  							</div>
                            
  						</div>
                        <div class="row" >
                        	<div class="col-md-6" style="margin-top:30px;">
  								<div class="info-box  bg-info  text-white">
  									<div class="info-icon bg-primary-dark">
  										<i class="fa fa-dollar fa-4x"></i>
  									</div>
  									<div class="info-details">
  										<h4>Sales (Quarter)   <span class="pull-right">${{number_format($dashboard_data->quarter_sales, "2",".",",")}}</span></h4>
  										<p>Last Quarter <span class="badge pull-right bg-success text-white">${{number_format($dashboard_data->last_quarter_sales, "2",".",",")}}</span> </p>
  									</div>
  								</div>
  							</div>
                            <div class="col-md-6" style="margin-top:30px;">
  								<div class="info-box  bg-success  text-white">
  									<div class="info-icon bg-success-dark">
  										<i class="fa fa-dollar fa-4x"></i>
  									</div>
  									<div class="info-details">
  										<h4>Profit Margin (Quarter)   <span class="pull-right">${{number_format($dashboard_data->quarter_margin, "2",".",",")}}</span></h4>
  										<p>Last Quarter <span class="badge pull-right bg-dark-primary text-white">${{number_format($dashboard_data->last_quarter_margin, "2",".",",")}}  </span> </p>
  									</div>
  								</div>
  							</div>
                        </div>
                        
                        <div class="row">
                        	<div class="col-md-12">
                            	<div class="panel">
                                
                                	<div class="panel-heading text-primary">
                              			<h3 class="panel-title">
                              				 <i class="fa fa-pencil-square"></i>
                               					All Customers<a name="allcustomers"></a>
                               
                          				</h3>
                       				</div><!-- end panel heading-->
                                	
                                    <div class="panel-body">
                                    <table  class="table table-bordered table-hover table-striped display tooltips" id="allCustomers" >
                           <thead>
                            <tr>
                             <th style="cursor:pointer">Customer Name</th>
                             <th style="cursor:pointer">CP Code</th>
                             <th style="cursor:pointer">Revenue</th>
                             <th style="cursor:pointer">Gross Profit %</th>
                           </tr>
                         </thead>
                         <tbody>
                         
                        @foreach($customerData as $cData)
                         
                        	<tr>
                              <td>{{ $cData->customername}}</td>
                              <td><a href="" data-original-title="{{ $cData->cp_contact}} {{ $cData->cp_phone}} | {{ $cData->cp_city}}, {{ $cData->cp_state}}" data-placement="left">{{ $cData->customerid}}</a></td>
                              <td>${{ number_format($cData->past_year_revenue)}}</td>
                              <td>{{ number_format(100*$cData->past_year_gp_por, 2,".",",")}}%</td>
                            </tr>
                        
                        @endforeach
                          
                              

                         </tfoot>
                         <tfoot>
                            <tr>
                             <th style="cursor:pointer">Customer Name</th>
                             <th style="cursor:pointer">CP Code</th>
                             <th style="cursor:pointer">Revenue</th>
                             <th style="cursor:pointer">Gross Profit %</th>
                           </tr>	
                         </tfoot>
                       </table>
                                    </div>
                                </div><!-- end panel-->
                            </div><!-- end col md 12-->
                        </div><!-- end row-->
                        
  					</div> <!-- /.content -->

  						
<script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
{{HTML::script('js/jquery-ui-1.10.3.custom.min.js');}}
{{HTML::script('js/less-1.5.0.min.js');}}
{{HTML::script('js/jquery.ui.touch-punch.min.js');}}
{{HTML::script('js/bootstrap.min.js');}}
{{HTML::script('js/bootstrap-select.js');}}
{{HTML::script('js/bootstrap-switch.js');}}
{{HTML::script('js/jquery.tagsinput.js');}}
{{HTML::script('js/jquery.placeholder.js');}}


<!-- Load JS here for Faster site load =============================-->


<script src="../../js/bootstrap-typeahead.js"></script>
<script src="../../js/application.js"></script>
<script src="../../js/moment.min.js"></script>
<script src="../../js/jquery.dataTables.min.js"></script>
<script src="../../js/jquery.sortable.js"></script>
<script type="text/javascript" src="../../js/jquery.gritter.js"></script>
<script src="../../js/jquery.nicescroll.min.js"></script>
<script src=../../"js/prettify.min.js"></script>
<script src="../../js/jquery.noty.js"></script>
<script src="../../js/bic_calendar.js"></script>
<script src="../../js/jquery.accordion.js"></script>
<script src="../../js/skylo.js"></script>

<script src="../../js/theme-options.js"></script>


<script src="../../js/bootstrap-progressbar.js"></script>
<script src="../../js/bootstrap-progressbar-custom.js"></script>
<script src="../../js/bootstrap-colorpicker.min.js"></script>
<script src="../../js/bootstrap-colorpicker-custom.js"></script>



<script src="../../js/raphael-min.js"></script>
<script src="../../js/morris-0.4.3.min.js"></script>
<script src="../../js/morris-custom.js"></script>
<script src="../../js/morris-custom-2.js"></script>

<script src="../../js/charts/jquery.sparkline.min.js"></script>	

<!-- NVD3 graphs  =============================-->
<script src="../../js/nvd3/lib/d3.v3.js"></script>
<script src="../../js/nvd3/nv.d3.js"></script>
<script src="../../js/nvd3/src/models/legend.js"></script>
<script src="../../js/nvd3/src/models/pie.js"></script>
<script src="../../js/nvd3/src/models/pieChart.js"></script>
<script src="../../js/nvd3/src/utils.js"></script>
<script src="../../js/nvd3/sample.nvd3.js"></script>
<script src="../../js/nvd3/nvd3-custom.js"></script>

<script src="../../js/bootstrap-tour.js"></script>

<!-- Page script File  =============================-->
<script src="../../js/tooltips-popovers.js"></script>

<!-- AMDCharts ================================================-->
<script src="../../js/amcharts/amcharts.js" type="text/javascript"></script>
<script src="../../js/amcharts/serial.js" type="text/javascript"></script>
<script src="../../js/amcharts/custom.js" type="text/javascript"></script>


<!-- Core Jquery File  =============================-->
<script src="../../js/core.js"></script>
<script src="../../js/dashboard-custom.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/less.js/1.5.0/less.min.js"></script>
  <script src="../../js/bootstrap-datatables.js"></script>
<script src="../../js/dataTables-custom.js"></script>
@stop