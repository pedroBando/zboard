@extends('layouts.main')

@section('content')
<div class="content"> 
	
    
    <div class="row">
  							<div class="col-mod-12">
								<!-- start breadcrumbs -->
  								<ul class="breadcrumb">
  									<li>Administration Dashboard {{ \Carbon\Carbon::now()->year}}</li>
  								</ul><!-- end breadc-->

  								

  								<h3 class="page-header"><i class="fa fa fa-dashboard"></i> Dashboard <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>
									 
  								<blockquote class="page-information hidden">
  									<p>
  										Dashboard is an "Easy To Read" real-time user interface, showing a graphical presentation of the current status and historical trends of an Zmac’s key performance indicators to enable that users can se their different stats.
  									</p>
  								</blockquote>
  							</div><!-- end col-md-12 -->
  						</div><!-- end row -->


  						<!-- Info Boxes -->
  						<div class="row panel-body" style="background-color:#2F343B">
  							<div class="col-md-4">
  								<div class="info-box  bg-info  text-white">
  									<div class="info-icon bg-info-dark">
  										<i class="fa fa-dollar fa-4x"></i>
  									</div>
  									<div class="info-details">
  										<h4>Monthly Revenue   <span class="pull-right">${{ number_format($dashboard_data->team_revenue, 2,".",",") }}</span></h4>
  										<p>Total Orders<span class="pull-right">{{$dashboard_data->total_orders}}</span></p>
  									</div>
  								</div><!-- end info box-->
  							</div><!-- end col-md4-->
  							<div class="col-md-4 ">
  								<div class="info-box  bg-success  text-white">
  									<div class="info-icon bg-success-dark">
  										<i class="fa fa-briefcase fa-4x"></i>
  									</div>
  									<div class="info-details">
  										<h4>Monthly Gross Profit   <span class="pull-right">${{ number_format($dashboard_data->team_gp, 2,".",",") }}</span></h4>
  										<p>Gross Profit %<span class="badge pull-right bg-white text-success">{{number_format($dashboard_data->team_gp_por*100, 2,".",",") }}%</span> </p>
  									</div>
  								</div><!-- end info box-->
  							</div><!-- end col-md4-->
  							<div class="col-md-4">
  								<div class="info-box  bg-warning  text-white">
  									<div class="info-icon bg-warning-dark">
  										<i class="fa fa-group fa-4x"></i>
  									</div>
  									<div class="info-details">
  										<h4>Monthly Goal<span class="pull-right">${{number_format($dashboard_data->team_goal, 2,".",",") }}</span></h4>
  										<p>% of Goal Achieved <span class="badge pull-right bg-white text-info">{{ number_format($dashboard_data->team_goal_por, 2,".",",") }}%</span> </p>
  									</div>
  								</div><!-- end info box-->
  							</div><!-- end col md 4-->
  						</div><!-- end row-->
                        
                        <!-- Charts -->
  						<div class="row" style="padding-top:20px; background-color:#fff;">
  							<!-- Discrete Bar Chart -->
  							<div class="col-md-6">
                              <div class="panel panel-primary">
                                <div class="panel-heading">
                                  <h3 class="panel-title">
  										<i class="fa fa-bar-chart-o"></i>
  											Monthly Totals & Goals
  											<span class="pull-right">
  												<a  href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a>
  												
  											</span>
  									</h3>
                                </div><!-- end panel heading-->
                                
                                
                                
                                <div class="panel-body">
                                  <div id="chartdiv" style="width:100%; height:400px;"></div>
                                </div><!-- end panel body-->
                              </div><!-- end panel-->
                              </div><!-- end col-->
                              
                              <div class="col-md-6">
                              	<div class="panel panel-warning">
                                <div class="panel-heading">
                                  <h3 class="panel-title">
  										<i class="fa fa-users"></i>
  											Monthly Team Summary
  											<span class="pull-right">
  												<a  href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a>
  												
  											</span>
  									</h3>
                                </div><!-- end panel heading-->
                                
                                
                                
                                <div class="panel-body">
                                	<table class="table table-bordered table-striped display" id="ray">
                                      <thead>
                                      <th>Employee Name</th>
                                      <th>Revenue</th>
                                      <th>Goal</th>
                                      <th>% Goal Achieved</th>
                                      <th>Gross Profit</th>
                                      <th>Gross Profit %</th>
                                      <th>Total Orders</th>
                                      </thead>
                                      @foreach($teams as $team)
                                      <tr>
                                      	<td><a href="{{ URL::to('/admin/'. $team->team_id_no .'/'.$team->level)}}">{{$team->team}}</a></td>
                                        <td>${{number_format($team->revenue)}}</td>
                                        <td>${{number_format($team->period_goal)}}</td>
                                        <td>{{number_format($team->por_goal_achieved*100, 2,".",",")}}%</td>
                                        <td>${{number_format($team->gp)}}</td>
                                        <td>{{number_format($team->gp_por*100, 2,".",",")}}%</td>
                                        <td>{{number_format($team->total_orders)}}</td>
                                      </tr>
                                      @endforeach
                                      <tfoot>
                                      <th>Employee Name</th>
                                      <th>Revenue</th>
                                      <th>Goal</th>
                                      <th>% Goal Achieved</th>
                                      <th>Gross Profit</th>
                                      <th>Gross Profit %</th>
                                      <th>Total Orders</th>
                                      </tfoot>
                                    </table>
                                  
                                </div><!-- end panel body-->
                              </div><!-- end panel-->
                              </div><!-- end col-->
                              
                              </div><!-- end row-->
                              
                              <div class="row">
                              <!-- start col--><div class="col-md-6">
                            	<!-- star panel--><div class="panel panel-primary">
                                <div class="panel-heading">
                                  <h3 class="panel-title">
  										<i class="fa fa-money"></i>
  											Company Performance Grid
  											<span class="pull-right">
  												<a  href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a>
  												
  											</span>
  									</h3>
                                </div><!-- end panel heading-->
                            		<!-- start bpanel body--><div class="panel-body" style="overflow:scroll">
                                    @if ($corp->current_qtr_company_revenue < ($corp->current_qtr_company_goal * .9))
                                    	<span class="label bg-danger" style="margin:auto auto">Current Quarter Revenue have not reach 90% of Revenue Goal</span><br/>
                                    @endif
                                    <span class="label bg-warning pull-left">Revenue Goal</span><span class="label bg-primary pull-right">Average Gross Margin</span><br/>
                                      <table class="table table-bordered table-striped display" id="incentives">
                                          <thead>
                                              <td></td>
                                              <td class="bg-primary" style="color:#fff">20.0%</td>
                                              <td class="bg-primary" style="color:#fff">20.4%</td>
                                              <td class="bg-primary" style="color:#fff">20.8%</td>
                                              <td class="bg-primary" style="color:#fff">21.2%</td>
                                              <td class="bg-primary" style="color:#fff">21.6%</td>
                                              <td class="bg-info" style="color:#fff">22.0%</td>
                                              <td class="bg-primary" style="color:#fff">22.6%</td>
                                              <td class="bg-primary" style="color:#fff">23.2%</td>
                                              <td class="bg-primary" style="color:#fff">23.8%</td>
                                              <td class="bg-primary" style="color:#fff">24.4%</td>
                                              <td class="bg-primary" style="color:#fff">25.0%</td>
                                          </thead>
                                          
                                          <tr>
                                              <td class="bg-warning" style="color:#fff">${{ number_format(($corp->current_qtr_company_goal * .9), 0, ".",",")}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 90 && 100*$corp->current_qtr_company_rev_por < 92.5) && (100*$corp->current_qtr_company_gp_por >= 20 && 100*$corp->current_qtr_company_gp_por < 20.4)) class="selected" @endif>${{ number_format($corp->target_incentive * .25)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 90 && 100*$corp->current_qtr_company_rev_por < 92.5) && (100*$corp->current_qtr_company_gp_por >= 20.4 && 100*$corp->current_qtr_company_gp_por < 20.8)) class="selected" @endif>${{ number_format($corp->target_incentive * .31)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 90 && 100*$corp->current_qtr_company_rev_por < 92.5) && (100*$corp->current_qtr_company_gp_por >= 20.8 && 100*$corp->current_qtr_company_gp_por < 21.2)) class="selected" @endif>${{ number_format($corp->target_incentive * .36)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 90 && 100*$corp->current_qtr_company_rev_por < 92.5) && (100*$corp->current_qtr_company_gp_por >= 21.2 && 100*$corp->current_qtr_company_gp_por < 21.6)) class="selected" @endif>${{ number_format($corp->target_incentive * .42)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 90 && 100*$corp->current_qtr_company_rev_por < 92.5) && (100*$corp->current_qtr_company_gp_por >= 21.6 && 100*$corp->current_qtr_company_gp_por < 22.0)) class="selected" @endif>${{ number_format($corp->target_incentive * .47)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 90 && 100*$corp->current_qtr_company_rev_por < 92.5) && (100*$corp->current_qtr_company_gp_por >= 22.0 && 100*$corp->current_qtr_company_gp_por < 22.6)) class="selected" @else class="bg-info"@endif style="color:#fff">${{ number_format($corp->target_incentive * .53)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 90 && 100*$corp->current_qtr_company_rev_por < 92.5) && (100*$corp->current_qtr_company_gp_por >= 22.6 && 100*$corp->current_qtr_company_gp_por < 23.2)) class="selected" @endif >${{ number_format($corp->target_incentive * .60)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 90 && 100*$corp->current_qtr_company_rev_por < 92.5) && (100*$corp->current_qtr_company_gp_por >= 23.2 && 100*$corp->current_qtr_company_gp_por < 23.8)) class="selected" @endif>${{ number_format($corp->target_incentive * .68)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 90 && 100*$corp->current_qtr_company_rev_por < 92.5) && (100*$corp->current_qtr_company_gp_por >= 23.8 && 100*$corp->current_qtr_company_gp_por < 24.4)) class="selected" @endif>${{ number_format($corp->target_incentive * .75)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 90 && 100*$corp->current_qtr_company_rev_por < 92.5) && (100*$corp->current_qtr_company_gp_por >= 24.4 && 100*$corp->current_qtr_company_gp_por < 25.0)) class="selected" @endif>${{ number_format($corp->target_incentive * .83)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 90 && 100*$corp->current_qtr_company_rev_por < 92.5) && (100*$corp->current_qtr_company_gp_por >= 25)) class="selected" @endif>${{ number_format($corp->target_incentive * .90)}}</td>
                                          </tr>
                                          
                                          <tr>
                                              <td class="bg-warning" style="color:#fff">${{ number_format(($corp->current_qtr_company_goal * .925), 0, ".",",")}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 92.5 && 100*$corp->current_qtr_company_rev_por < 95.0) && (100*$corp->current_qtr_company_gp_por >= 20 && 100*$corp->current_qtr_company_gp_por < 20.4)) class="selected" @endif>${{ number_format($corp->target_incentive * .32)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 92.5 && 100*$corp->current_qtr_company_rev_por < 95.0) && (100*$corp->current_qtr_company_gp_por >= 20.4 && 100*$corp->current_qtr_company_gp_por < 20.8)) class="selected" @endif>${{ number_format($corp->target_incentive * .39)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 92.5 && 100*$corp->current_qtr_company_rev_por < 95.0) && (100*$corp->current_qtr_company_gp_por >= 20.8 && 100*$corp->current_qtr_company_gp_por < 21.2)) class="selected" @endif>${{ number_format($corp->target_incentive * .45)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 92.5 && 100*$corp->current_qtr_company_rev_por < 95.0) && (100*$corp->current_qtr_company_gp_por >= 21.2 && 100*$corp->current_qtr_company_gp_por < 21.6)) class="selected" @endif>${{ number_format($corp->target_incentive * .52)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 92.5 && 100*$corp->current_qtr_company_rev_por < 95.0) && (100*$corp->current_qtr_company_gp_por >= 21.6 && 100*$corp->current_qtr_company_gp_por < 22)) class="selected" @endif>${{ number_format($corp->target_incentive * .58)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 92.5 && 100*$corp->current_qtr_company_rev_por < 95.0) && (100*$corp->current_qtr_company_gp_por >= 22 && 100*$corp->current_qtr_company_gp_por < 22.6)) class="selected" @else class="bg-info"@endif style="color:#fff">${{ number_format($corp->target_incentive * .65)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 92.5 && 100*$corp->current_qtr_company_rev_por < 95.0) && (100*$corp->current_qtr_company_gp_por >= 22.6 && 100*$corp->current_qtr_company_gp_por < 23.2)) class="selected" @endif>${{ number_format($corp->target_incentive * .72)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 92.5 && 100*$corp->current_qtr_company_rev_por < 95.0) && (100*$corp->current_qtr_company_gp_por >= 23.2 && 100*$corp->current_qtr_company_gp_por < 23.8)) class="selected" @endif>${{ number_format($corp->target_incentive * .80)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 92.5 && 100*$corp->current_qtr_company_rev_por < 95.0) && (100*$corp->current_qtr_company_gp_por >= 23.8 && 100*$corp->current_qtr_company_gp_por < 24.4)) class="selected" @endif>${{ number_format($corp->target_incentive * .87)}}</td>
                                              <td  @if((100*$corp->current_qtr_company_rev_por >= 92.5 && 100*$corp->current_qtr_company_rev_por < 95.0) && (100*$corp->current_qtr_company_gp_por >= 24.4 && 100*$corp->current_qtr_company_gp_por < 25)) class="selected" @endif>${{ number_format($corp->target_incentive * .94)}}</td>
                                              <td  @if((100*$corp->current_qtr_company_rev_por >= 92.5 && 100*$corp->current_qtr_company_rev_por < 95.0) && (100*$corp->current_qtr_company_gp_por >= 25)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.02)}}</td>
                                          </tr>
                                          
                                          <tr>
                                              <td class="bg-warning" style="color:#fff">${{ number_format(($corp->current_qtr_company_goal * .95), 0, ".",",")}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 95 && 100*$corp->current_qtr_company_rev_por < 97.5) && (100*$corp->current_qtr_company_gp_por >= 20 && 100*$corp->current_qtr_company_gp_por < 20.4)) class="selected" @endif>${{ number_format($corp->target_incentive * .39)}}</td>
                                              <td  @if((100*$corp->current_qtr_company_rev_por >= 95 && 100*$corp->current_qtr_company_rev_por < 97.5) && (100*$corp->current_qtr_company_gp_por >= 20.4 && 100*$corp->current_qtr_company_gp_por < 20.8)) class="selected" @endif>${{ number_format($corp->target_incentive * .46)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 95 && 100*$corp->current_qtr_company_rev_por < 97.5) && (100*$corp->current_qtr_company_gp_por >= 20.8 && 100*$corp->current_qtr_company_gp_por < 21.2)) class="selected" @endif>${{ number_format($corp->target_incentive * .54)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 95 && 100*$corp->current_qtr_company_rev_por < 97.5) && (100*$corp->current_qtr_company_gp_por >= 21.2 && 100*$corp->current_qtr_company_gp_por < 21.6)) class="selected" @endif>${{ number_format($corp->target_incentive * .61)}}</td>
                                              <td  @if((100*$corp->current_qtr_company_rev_por >= 95 && 100*$corp->current_qtr_company_rev_por < 97.5) && (100*$corp->current_qtr_company_gp_por >= 21.6 && 100*$corp->current_qtr_company_gp_por < 22)) class="selected" @endif>${{ number_format($corp->target_incentive * .69)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 95 && 100*$corp->current_qtr_company_rev_por < 97.5) && (100*$corp->current_qtr_company_gp_por >= 22 && 100*$corp->current_qtr_company_gp_por < 22.6)) class="selected" @else  class="bg-info" @endif style="color:#fff">${{ number_format($corp->target_incentive * .76)}}</td>
                                              <td  @if((100*$corp->current_qtr_company_rev_por >= 95 && 100*$corp->current_qtr_company_rev_por < 97.5) && (100*$corp->current_qtr_company_gp_por >= 22.6 && 100*$corp->current_qtr_company_gp_por < 23.2)) class="selected" @endif>${{ number_format($corp->target_incentive * .84)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 95 && 100*$corp->current_qtr_company_rev_por < 97.5) && (100*$corp->current_qtr_company_gp_por >= 23.2 && 100*$corp->current_qtr_company_gp_por < 23.8)) class="selected" @endif>${{ number_format($corp->target_incentive * .91)}}</td>
                                              <td  @if((100*$corp->current_qtr_company_rev_por >= 95 && 100*$corp->current_qtr_company_rev_por < 97.5) && (100*$corp->current_qtr_company_gp_por >= 23.8 && 100*$corp->current_qtr_company_gp_por < 24.4)) class="selected" @endif>${{ number_format($corp->target_incentive * .99)}}</td>
                                              <td  @if((100*$corp->current_qtr_company_rev_por >= 95 && 100*$corp->current_qtr_company_rev_por < 97.5) && (100*$corp->current_qtr_company_gp_por >= 24.4 && 100*$corp->current_qtr_company_gp_por < 25)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.06)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 95 && 100*$corp->current_qtr_company_rev_por < 97.5) && (100*$corp->current_qtr_company_gp_por >= 25)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.14)}}</td>
                                          </tr>
                                          
                                          <tr>
                                              <td class="bg-warning" style="color:#fff">${{ number_format(($corp->current_qtr_company_goal * .975), 0, ".",",")}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 97.5 && 100*$corp->current_qtr_company_rev_por < 100) && (100*$corp->current_qtr_company_gp_por >= 20 && 100*$corp->current_qtr_company_gp_por < 20.4)) class="selected" @endif>${{ number_format($corp->target_incentive * .46)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 97.5 && 100*$corp->current_qtr_company_rev_por < 100) && (100*$corp->current_qtr_company_gp_por >= 20.4 && 100*$corp->current_qtr_company_gp_por < 20.8)) class="selected" @endif>${{ number_format($corp->target_incentive * .54)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 97.5 && 100*$corp->current_qtr_company_rev_por < 100) && (100*$corp->current_qtr_company_gp_por >= 20.8 && 100*$corp->current_qtr_company_gp_por < 21.2)) class="selected" @endif>${{ number_format($corp->target_incentive * .63)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 97.5 && 100*$corp->current_qtr_company_rev_por < 100) && (100*$corp->current_qtr_company_gp_por >= 21.2 && 100*$corp->current_qtr_company_gp_por < 21.6)) class="selected" @endif>${{ number_format($corp->target_incentive * .71)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 97.5 && 100*$corp->current_qtr_company_rev_por < 100) && (100*$corp->current_qtr_company_gp_por >= 21.6 && 100*$corp->current_qtr_company_gp_por < 22)) class="selected" @endif>${{ number_format($corp->target_incentive * .80)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 97.5 && 100*$corp->current_qtr_company_rev_por < 100) && (100*$corp->current_qtr_company_gp_por >= 22.0 && 100*$corp->current_qtr_company_gp_por < 22.6)) class="selected" @else class="bg-info" @endif style="color:#fff">${{ number_format($corp->target_incentive * .88)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 97.5 && 100*$corp->current_qtr_company_rev_por < 100) && (100*$corp->current_qtr_company_gp_por >= 22.6 && 100*$corp->current_qtr_company_gp_por < 23.2)) class="selected" @endif>${{ number_format($corp->target_incentive * .96)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 97.5 && 100*$corp->current_qtr_company_rev_por < 100) && (100*$corp->current_qtr_company_gp_por >= 23.2 && 100*$corp->current_qtr_company_gp_por < 23.8)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.03)}}</td>
                                              <td  @if((100*$corp->current_qtr_company_rev_por >= 97.5 && 100*$corp->current_qtr_company_rev_por < 100) && (100*$corp->current_qtr_company_gp_por >= 23.8 && 100*$corp->current_qtr_company_gp_por < 24.4)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.11)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 97.5 && 100*$corp->current_qtr_company_rev_por < 100) && (100*$corp->current_qtr_company_gp_por >= 24.4 && 100*$corp->current_qtr_company_gp_por < 25)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.18)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 97.5 && 100*$corp->current_qtr_company_rev_por < 100) && (100*$corp->current_qtr_company_gp_por >= 25)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.25)}}</td>
                                          </tr>
                                          
                                          <tr>
                                              <td class="bg-info" style="color:#fff">${{ number_format(($corp->current_qtr_company_goal * 1), 0, ".",",")}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 100 && 100*$corp->current_qtr_company_rev_por < 105) && (100*$corp->current_qtr_company_gp_por >= 20.0 && 100*$corp->current_qtr_company_gp_por < 20.4)) class="selected" @else class="bg-info" @endif style="color:#fff">${{ number_format($corp->target_incentive * .53)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 100 && 100*$corp->current_qtr_company_rev_por < 105) && (100*$corp->current_qtr_company_gp_por >= 20.4 && 100*$corp->current_qtr_company_gp_por < 20.8)) class="selected" @else class="bg-info" @endif style="color:#fff">${{ number_format($corp->target_incentive * .62)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 100 && 100*$corp->current_qtr_company_rev_por < 105) && (100*$corp->current_qtr_company_gp_por >= 20.8 && 100*$corp->current_qtr_company_gp_por < 21.2)) class="selected" @else class="bg-info" @endif style="color:#fff">${{ number_format($corp->target_incentive * .72)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 100 && 100*$corp->current_qtr_company_rev_por < 105) && (100*$corp->current_qtr_company_gp_por >= 21.2 && 100*$corp->current_qtr_company_gp_por < 21.6)) class="selected" @else class="bg-info" @endif style="color:#fff">${{ number_format($corp->target_incentive * .81)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 100 && 100*$corp->current_qtr_company_rev_por < 105) && (100*$corp->current_qtr_company_gp_por >= 21.6 && 100*$corp->current_qtr_company_gp_por < 22)) class="selected" @else class="bg-info" @endif style="color:#fff">${{ number_format($corp->target_incentive * .91)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 100 && 100*$corp->current_qtr_company_rev_por < 105) && (100*$corp->current_qtr_company_gp_por >= 22 && 100*$corp->current_qtr_company_gp_por < 22.6)) class="selected" @else class="bg-info" @endif style="color:#fff">${{ number_format($corp->target_incentive * 1.00)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 100 && 100*$corp->current_qtr_company_rev_por < 105) && (100*$corp->current_qtr_company_gp_por >= 22.6 && 100*$corp->current_qtr_company_gp_por < 23.2)) class="selected" @else class="bg-info" @endif style="color:#fff">${{ number_format($corp->target_incentive * 1.07)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 100 && 100*$corp->current_qtr_company_rev_por < 105) && (100*$corp->current_qtr_company_gp_por >= 23.2 && 100*$corp->current_qtr_company_gp_por < 23.8)) class="selected" @else class="bg-info" @endif style="color:#fff">${{ number_format($corp->target_incentive * 1.15)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 100 && 100*$corp->current_qtr_company_rev_por < 105) && (100*$corp->current_qtr_company_gp_por >= 23.8 && 100*$corp->current_qtr_company_gp_por < 24.4)) class="selected" @else class="bg-info" @endif style="color:#fff">${{ number_format($corp->target_incentive * 1.22)}}%</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 100 && 100*$corp->current_qtr_company_rev_por < 105) && (100*$corp->current_qtr_company_gp_por >= 24.4 && 100*$corp->current_qtr_company_gp_por < 25)) class="selected" @else class="bg-info" @endif style="color:#fff">${{ number_format($corp->target_incentive * 1.30)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 100 && 100*$corp->current_qtr_company_rev_por < 105) && (100*$corp->current_qtr_company_gp_por >= 25)) class="selected" @else class="bg-info" @endif style="color:#fff">${{ number_format($corp->target_incentive * 1.3)}}</td>
                                          </tr>
                                          
                                          <tr>
                                              <td class="bg-warning" style="color:#fff">${{ number_format(($corp->current_qtr_company_goal * 1.5), 0, ".",",")}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 105 && 100*$corp->current_qtr_company_rev_por < 110) && (100*$corp->current_qtr_company_gp_por >= 20 && 100*$corp->current_qtr_company_gp_por < 20.4)) class="selected" @endif>${{ number_format($corp->target_incentive * .62)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 105 && 100*$corp->current_qtr_company_rev_por < 110) && (100*$corp->current_qtr_company_gp_por >= 20.4 && 100*$corp->current_qtr_company_gp_por < 20.8)) class="selected" @endif>${{ number_format($corp->target_incentive * .72)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 105 && 100*$corp->current_qtr_company_rev_por < 110) && (100*$corp->current_qtr_company_gp_por >= 20.8 && 100*$corp->current_qtr_company_gp_por < 21.2)) class="selected" @endif>${{ number_format($corp->target_incentive * .81)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 105 && 100*$corp->current_qtr_company_rev_por < 110) && (100*$corp->current_qtr_company_gp_por >= 21.2 && 100*$corp->current_qtr_company_gp_por < 21.6)) class="selected" @endif>${{ number_format($corp->target_incentive * .90)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 105 && 100*$corp->current_qtr_company_rev_por < 110) && (100*$corp->current_qtr_company_gp_por >= 21.6 && 100*$corp->current_qtr_company_gp_por < 22)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.00)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 105 && 100*$corp->current_qtr_company_rev_por < 110) && (100*$corp->current_qtr_company_gp_por >= 22 && 100*$corp->current_qtr_company_gp_por < 22.6)) class="selected" @else class="bg-info" @endif style="color:#fff">${{ number_format($corp->target_incentive * 1.09)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 105 && 100*$corp->current_qtr_company_rev_por < 110) && (100*$corp->current_qtr_company_gp_por >= 22.6 && 100*$corp->current_qtr_company_gp_por < 23.2)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.18)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 105 && 100*$corp->current_qtr_company_rev_por < 110) && (100*$corp->current_qtr_company_gp_por >= 23.2 && 100*$corp->current_qtr_company_gp_por < 23.8)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.27)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 105 && 100*$corp->current_qtr_company_rev_por < 110) && (100*$corp->current_qtr_company_gp_por >= 23.8 && 100*$corp->current_qtr_company_gp_por < 24.4)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.35)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 105 && 100*$corp->current_qtr_company_rev_por < 110) && (100*$corp->current_qtr_company_gp_por >= 24.4 && 100*$corp->current_qtr_company_gp_por < 25)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.44)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 105 && 100*$corp->current_qtr_company_rev_por < 110) && (100*$corp->current_qtr_company_gp_por >= 25)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.53)}}</td>
                                          </tr>
                                          
                                          <tr>
                                              <td class="bg-warning" style="color:#fff">${{ number_format(($corp->current_qtr_company_goal * 1.10), 0, ".",",")}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 110 && 100*$corp->current_qtr_company_rev_por < 115) && (100*$corp->current_qtr_company_gp_por >= 20 && 100*$corp->current_qtr_company_gp_por < 20.4)) class="selected" @endif>${{ number_format($corp->target_incentive * .71)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 110 && 100*$corp->current_qtr_company_rev_por < 115) && (100*$corp->current_qtr_company_gp_por >= 20.4 && 100*$corp->current_qtr_company_gp_por < 20.8)) class="selected" @endif>${{ number_format($corp->target_incentive * .81)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 110 && 100*$corp->current_qtr_company_rev_por < 115) && (100*$corp->current_qtr_company_gp_por >= 20.8 && 100*$corp->current_qtr_company_gp_por < 21.2)) class="selected" @endif>${{ number_format($corp->target_incentive * .90)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 110 && 100*$corp->current_qtr_company_rev_por < 115) && (100*$corp->current_qtr_company_gp_por >= 21.2 && 100*$corp->current_qtr_company_gp_por < 21.6)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.00)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 110 && 100*$corp->current_qtr_company_rev_por < 115) && (100*$corp->current_qtr_company_gp_por >= 21.6 && 100*$corp->current_qtr_company_gp_por < 22)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.09)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 110 && 100*$corp->current_qtr_company_rev_por < 115) && (100*$corp->current_qtr_company_gp_por >= 22 && 100*$corp->current_qtr_company_gp_por < 22.6)) class="selected" @else class="bg-info" @endif style="color:#fff">${{ number_format($corp->target_incentive * 1.19)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 110 && 100*$corp->current_qtr_company_rev_por < 115) && (100*$corp->current_qtr_company_gp_por >= 22.6 && 100*$corp->current_qtr_company_gp_por < 23.2)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.29)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 110 && 100*$corp->current_qtr_company_rev_por < 115) && (100*$corp->current_qtr_company_gp_por >= 23.2 && 100*$corp->current_qtr_company_gp_por < 23.8)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.39)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 110 && 100*$corp->current_qtr_company_rev_por < 115) && (100*$corp->current_qtr_company_gp_por >= 28.8 && 100*$corp->current_qtr_company_gp_por < 24.4)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.49)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 110 && 100*$corp->current_qtr_company_rev_por < 115) && (100*$corp->current_qtr_company_gp_por >= 24.4 && 100*$corp->current_qtr_company_gp_por < 25)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.59)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 110 && 100*$corp->current_qtr_company_rev_por < 115) && (100*$corp->current_qtr_company_gp_por >= 25)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.69)}}</td>
                                          </tr>
                                          
                                          <tr>
                                              <td class="bg-warning" style="color:#fff">${{ number_format(($corp->current_qtr_company_goal * 1.15), 0, ".",",")}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 115 && 100*$corp->current_qtr_company_rev_por < 120) && (100*$corp->current_qtr_company_gp_por >= 20 && 100*$corp->current_qtr_company_gp_por < 20.4)) class="selected" @endif>${{ number_format($corp->target_incentive * .81)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 115 && 100*$corp->current_qtr_company_rev_por < 120) && (100*$corp->current_qtr_company_gp_por >= 20.4 && 100*$corp->current_qtr_company_gp_por < 20.8)) class="selected" @endif>${{ number_format($corp->target_incentive * .90)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 115 && 100*$corp->current_qtr_company_rev_por < 120) && (100*$corp->current_qtr_company_gp_por >= 20.8 && 100*$corp->current_qtr_company_gp_por < 21.2)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.00)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 115 && 100*$corp->current_qtr_company_rev_por < 120) && (100*$corp->current_qtr_company_gp_por >= 21.2 && 100*$corp->current_qtr_company_gp_por < 21.6)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.09)}}</td>
                                              <td  @if((100*$corp->current_qtr_company_rev_por >= 115 && 100*$corp->current_qtr_company_rev_por < 120) && (100*$corp->current_qtr_company_gp_por >= 21.6 && 100*$corp->current_qtr_company_gp_por < 22)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.18)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 115 && 100*$corp->current_qtr_company_rev_por < 120) && (100*$corp->current_qtr_company_gp_por >= 22 && 100*$corp->current_qtr_company_gp_por < 22.6)) class="selected" @else class="bg-info" @endif style="color:#fff">${{ number_format($corp->target_incentive * 1.28)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 115 && 100*$corp->current_qtr_company_rev_por < 120) && (100*$corp->current_qtr_company_gp_por >= 22.6 && 100*$corp->current_qtr_company_gp_por < 23.2)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.39)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 115 && 100*$corp->current_qtr_company_rev_por < 120) && (100*$corp->current_qtr_company_gp_por >= 23.2 && 100*$corp->current_qtr_company_gp_por < 23.8)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.50)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 115 && 100*$corp->current_qtr_company_rev_por < 120) && (100*$corp->current_qtr_company_gp_por >= 23.8 && 100*$corp->current_qtr_company_gp_por < 24.4)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.62)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 115 && 100*$corp->current_qtr_company_rev_por < 120) && (100*$corp->current_qtr_company_gp_por >= 24.4 && 100*$corp->current_qtr_company_gp_por < 25)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.73)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 115 && 100*$corp->current_qtr_company_rev_por < 120) && (100*$corp->current_qtr_company_gp_por >= 25)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.84)}}</td>
                                           </tr>
                                           
                                           <tr>
                                           <td class="bg-warning" style="color:#fff">${{ number_format(($corp->current_qtr_company_goal * 1.20), 0, ".",",")}}</td>
                                           <td @if((100*$corp->current_qtr_company_rev_por >= 120) && (100*$corp->current_qtr_company_gp_por >= 20 && 100*$corp->current_qtr_company_gp_por < 20.4)) class="selected" @endif>${{ number_format($corp->target_incentive * .90)}}</td>
                                           <td @if((100*$corp->current_qtr_company_rev_por >= 120 ) && (100*$corp->current_qtr_company_gp_por >= 20.4 && 100*$corp->current_qtr_company_gp_por < 20.8)) class="selected" @endif>${{ number_format($corp->target_incentive * .99)}}</td>
                                           <td @if((100*$corp->current_qtr_company_rev_por >= 120) && (100*$corp->current_qtr_company_gp_por >= 20.8 && 100*$corp->current_qtr_company_gp_por < 21.2)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.09)}}</td>
                                           <td @if((100*$corp->current_qtr_company_rev_por >= 120) && (100*$corp->current_qtr_company_gp_por >= 21.2 && 100*$corp->current_qtr_company_gp_por < 21.6)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.18)}}</td>
                                           <td @if((100*$corp->current_qtr_company_rev_por >= 120) && (100*$corp->current_qtr_company_gp_por >= 21.6 && 100*$corp->current_qtr_company_gp_por < 22)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.28)}}</td>
                                           <td @if((100*$corp->current_qtr_company_rev_por >= 120) && (100*$corp->current_qtr_company_gp_por >= 22 && 100*$corp->current_qtr_company_gp_por < 22.6)) class="selected" @else class="bg-info" @endif style="color:#fff">${{ number_format($corp->target_incentive * 1.37)}}</td>
                                           <td @if((100*$corp->current_qtr_company_rev_por >= 120) && (100*$corp->current_qtr_company_gp_por >= 22.6 && 100*$corp->current_qtr_company_gp_por < 23.2)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.50)}}</td>
                                           <td @if((100*$corp->current_qtr_company_rev_por >= 120) && (100*$corp->current_qtr_company_gp_por >= 23.2 && 100*$corp->current_qtr_company_gp_por < 23.8)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.62)}}</td>
                                           <td @if((100*$corp->current_qtr_company_rev_por >= 120) && (100*$corp->current_qtr_company_gp_por >= 23.8 && 100*$corp->current_qtr_company_gp_por < 24.4)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.75)}}</td>
                                           <td @if((100*$corp->current_qtr_company_rev_por >= 120) && (100*$corp->current_qtr_company_gp_por >= 24.4 && 100*$corp->current_qtr_company_gp_por < 25)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.87)}}</td>
                                           <td @if((100*$corp->current_qtr_company_rev_por >= 120) && (100*$corp->current_qtr_company_gp_por >= 25)) class="selected" @endif>${{ number_format($corp->target_incentive * 2.00)}}</td>
                                           </tr>
                                      </table>
                                      </div><!-- end panel body-->
                                      </div> <!-- end panel-->
					</div><!-- end col-->
                    
                    <div class="col-md-6">
                    	<div class="info-box  bg-info  text-white">
  									<div class="info-icon bg-info-dark">
  										<i class="fa fa-dollar fa-4x"></i>
  									</div>
  									<div class="info-details">
  										<h4>Quarter Revenue   <span class="pull-right">${{ number_format($dashboard_dataq->team_revenue, 2,".",",") }}</span></h4>
  										<p>Team EED Posible/Achieved<span class="pull-right">{{$dashboard_data->team_eed_possible}}/{{$dashboard_data->team_eed_achieved}}</span></p>
  									</div>
  								</div><!-- end info box-->
                                
                                <div class="info-box  bg-success  text-white">
  									<div class="info-icon bg-success-dark">
  										<i class="fa fa-briefcase fa-4x"></i>
  									</div>
  									<div class="info-details">
  										<h4>Quarter Gross Profit   <span class="pull-right">${{ number_format($dashboard_dataq->team_gp, 2,".",",") }}</span></h4>
  										<p>Gross Profit %  <span class="badge pull-right bg-white text-info">{{ number_format($dashboard_data->team_gp_por*100, 2,".",",") }}%</span> </p>
  									</div>
  								</div><!-- end info box-->
                                
                                
                                
                                <div class="info-box  bg-warning  text-white">
  									<div class="info-icon bg-warning-dark">
  										<i class="fa fa-road fa-4x"></i>
  									</div>
  									<div class="info-details">
  										<h4>Quarter Team Goal   <span class="pull-right">${{ number_format($dashboard_dataq->team_goal, 2,".",",") }}</span></h4>
  										<p>Team Goal % <span class="badge pull-right bg-white text-info">{{ number_format($dashboard_data->team_goal_por*100, 2,".",",") }}%</span> </p>
  									</div>
  								</div><!-- end info box-->
                                
                                <div class="panel panel-primary">
  							  	<div class="panel-heading bg-purple">
  									<h3 class="panel-title">
  										<i class="fa fa-bar-chart-o"></i>
  											Post to Message Board
  											<span class="pull-right">
  												<a  href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a>
  												
  											</span>
  									</h3>
  								</div><!-- end panel heading-->
  							  
                                <div class="panel-body nopadding">
  										
								  <div style="height:207px"></div>

  							    </div><!-- end panel body-->
  							  </div><!-- end panel primary-->
                    </div><!-- end col-->
                              
                              </div><!-- end row-->

  						

  	<script type="text/javascript">
		var chartData = <?php echo json_encode($chart_data); ?>
</script>					
<script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
{{HTML::script('js/jquery-ui-1.10.3.custom.min.js');}}
{{HTML::script('js/less-1.5.0.min.js');}}
{{HTML::script('js/jquery.ui.touch-punch.min.js');}}
{{HTML::script('js/bootstrap.min.js');}}
{{HTML::script('js/bootstrap-select.js');}}
{{HTML::script('js/bootstrap-switch.js');}}
{{HTML::script('js/jquery.tagsinput.js');}}
{{HTML::script('js/jquery.placeholder.js');}}


<!-- Load JS here for Faster site load =============================-->


<script src="../../js/bootstrap-typeahead.js"></script>
<script src="../../js/application.js"></script>
<script src="../../js/moment.min.js"></script>
<script src="../../js/jquery.dataTables.min.js"></script>
<script src="../../js/jquery.sortable.js"></script>
<script type="text/javascript" src="../../js/jquery.gritter.js"></script>
<script src="../../js/jquery.nicescroll.min.js"></script>
<script src=../../"js/prettify.min.js"></script>
<script src="../../js/jquery.noty.js"></script>
<script src="../../js/bic_calendar.js"></script>
<script src="../../js/jquery.accordion.js"></script>
<script src="../../js/skylo.js"></script>

<script src="../../js/theme-options.js"></script>


<script src="../../js/bootstrap-progressbar.js"></script>
<script src="../../js/bootstrap-progressbar-custom.js"></script>
<script src="../../js/bootstrap-colorpicker.min.js"></script>
<script src="../../js/bootstrap-colorpicker-custom.js"></script>



<script src="../../js/raphael-min.js"></script>
<script src="../../js/morris-0.4.3.min.js"></script>
<script src="../../js/morris-custom.js"></script>
<script src="../../js/morris-custom-2.js"></script>

<script src="../../js/charts/jquery.sparkline.min.js"></script>	



<script src="../../js/bootstrap-tour.js"></script>

<!-- Page script File  =============================-->
<script src="../../js/tooltips-popovers.js"></script>

<!-- AMDCharts ================================================-->
<script src="../../js/amcharts/amcharts.js" type="text/javascript"></script>
<script src="../../js/amcharts/serial.js" type="text/javascript"></script>


<script src="../../js/amcharts/custom.js" type="text/javascript"></script>


<!-- Core Jquery File  =============================-->
<script src="../../js/core.js"></script>
<script src="../../js/dashboard-custom.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/less.js/1.5.0/less.min.js"></script>
  <script src="../../js/bootstrap-datatables.js"></script>
<script src="../../js/dataTables-custom.js"></script>
@stop