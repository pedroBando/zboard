<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
 
    <title>Authentication App With Laravel 4</title>
    {{ HTML::style('packages/bootstrap/css/bootstrap.min.css') }}
    {{ HTML::style('css/style.css')}}
    {{ HTML::style('css/login.css')}}
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
  </head>
  
  <body>
  	<div class="list-group side-menu ">
        {{ HTML::link('users/register', 'Register', array('class' => 'list-group-item') )  }}   
        {{ HTML::link('users/login', 'Login', array('class' => 'list-group-item')) }}
  	</div>
  
  	
        
        
        {{ $content }}
  	
  </body>
</html>