
            <!-- .left-sidebar -->
            <div class="left-sidebar">
              <div class="sidebar-holder">
                <ul class="nav  nav-list">
                  <!-- sidebar to mini Sidebar toggle -->
                  <li class="nav-toggle">
                    <button class="btn btn-nav-toggle text-primary"><i class="fa fa-angle-double-left toggle-left"></i> </button>
                  </li>
                  @if(Auth::user()->type == "mg")
                  <li><a href="{{ url('users/adashboard')}}" alt="Dashboard"><i class="fa fa-dashboard"></i>Dashboard</a></li>
                  @else
                   <li><a href="{{ url('users/dashboard')}}" alt="Dashboard"><i class="fa fa-dashboard"></i>Dashboard</a></li>
                  @endif 
					@if(Auth::user()->type == "se")
                    <li><a href="{{ url('users/overview')}}" alt="Overview"><i class="fa fa-info-circle"></i>Overview</a></li>
                    <li><a href="{{ url('users/history')}}" alt="History"><i class="fa fa-indent"></i>History</a></li>
                    @elseif(Auth::user()->type == "ae")
                    <li><a href="{{ url('users/acchistory')}}" alt="Overview"><i class="fa fa-indent"></i>History</a></li>
                    <li><a href="{{ url('users/aecustomerslist')}}" alt="Overview"><i class="fa fa-indent"></i>My Customers</a></li>
                    @elseif(Auth::user()->type == "ce")
                    <li><a href="{{ url('users/carrierhistory')}}" alt="History"><i class="fa fa-indent"></i>History</a></li>
                    <li><a href="{{ url('users/carrierlist')}}" alt="History"><i class="fa fa-indent"></i>Top Carriers</a></li>
                   @endif
					<li><a href="{{ url('users/support')}}" alt="Support"><i class="fa fa-gear"></i>Support</a></li>
                </ul>
                <!-- if(Route::currentRouteName() == "adminEmployees")
                
                		<div class="panel-heading"><h3 style="font-size:1.2em">Employee's Navigation</h3></div>
           				<ul class="nav  nav-list">
                          - sidebar to mini Sidebar toggle 
                         
                            if($emid->type == "se")
                            <li><a href=" URL::to('admin/odetail/'.$emid->id)}}" alt="Overview"><i class="fa fa-info-circle"></i>Overview</a></li>
                            <li><a href=" URL::to('admin/'.$emid->id .'/det/adminse-history')}}" alt="History"><i class="fa fa-indent"></i>History</a></li>
                            elseif($emid->type == "ae")
                            <li><a href=" URL::to('admin/'.$emid->id .'/det/adminae-acchistory')}}" alt="Overview"><i class="fa fa-indent"></i>History</a></li>
                            <li><a href=" URL::to('admin/'.$emid->id .'/det/adminae-acchistory')}}" alt="Overview"><i class="fa fa-indent"></i>My Customers</a></li>
                            elseif($emid->type == "ce")
                            <li><a href=" URL::to('admin/'.$emid->id .'/det/admince-carrierhistory')}}" alt="History"><i class="fa fa-indent"></i>History</a></li>
                            <li><a href=" URL::to('admin/'.$emid->id .'/det/carrierlist')}}" alt="History"><i class="fa fa-indent"></i>Top Carriers</a></li>
                           endif
                        </ul>
           		endif-->
               
              </div>
  									
              <div class="panel-heading">
                <h3 class="panel-title active" style="font-size:14px"> <i class="fa fa-bar-chart-o"></i> Sales Rep Rankings</h3>{{Route::currentRouteName()}}
			  </div>
              <div class="list-group projects">
                  <a class="list-group-item" href="#">Coming Soon	
                      <div class="exemple4" data-average="10" data-id="5" style="height: 20px; width: 115px; overflow: hidden; z-index: 1; position: relative;">
                        <div class="jRatingColor" style="width: 57.5px;"></div>
                        <div class="jRatingAverage" style="width: 0px; top: -20px;"></div>
                        <div class="jStar" style="width: 115px; height: 20px; top: -40px; background-image: url(http://bootstrapguru.com/preview/cascade/jquery/icons/stars.png); background-position: initial initial; background-repeat: repeat no-repeat;"></div>
                      </div>
                  </a>
              </div>
              
             
            	
            </div> <!-- /.left-sidebar -->
        