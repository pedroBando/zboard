<?php

  
     //if (Request::getClientIp() != '98.103.191.130' || Request::getClientIp() != '74.219.230.166') {
      //     header("Location: http://www.zmactransport.com");
     // } 
//Code End. ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
 
    <title>ZBoard 1.0</title>
    {{ HTML::style('packages/bootstrap/css/bootstrap.css') }}
    {{ HTML::style('css/style.css')}}
    {{ HTML::style('less/style.less')}}
    {{ HTML::style('css/custom.css')}}
  
    
    
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    <?php 
	$userrid = Auth::user()->id;
    	$pieces = explode('/',$_SERVER['REQUEST_URI']);  
  		$page=end($pieces); 
		if(strpos($page,"extended-modals") !== false ) { ?>
   			<link href="css/bootstrap-modal-bs3fix.css" rel="stylesheet" type="text/css"> 
    <?php } ?>
    
    <link rel="shortcut icon" href="images/favicon.ico">
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
      <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <![endif]-->
    
  </head>
  
  <body>
  	<div class="site-holder">
    	<!-- .navbar -->
      <nav class="navbar" role="navigation">

        <!-- Brand and toggle get grouped for better mobile display -->
        
        <div class="navbar-header">
          <a class="navbar-brand" href="#">{{HTML::image('/css/images/logo.png', 'ZBoard 1.0', array('class' => 'dash-logo'))}}</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav user-menu navbar-right " id="user-menu">
            <li><a href="{{url('users/profile') }}" class="user dropdown-toggle" data-toggle="dropdown"><span class="username">{{HTML::image('/uploads/profiles/'.Auth::user()->avatar, 'Auth::user()->firstname', array('class' => 'user-avatar'))}}
   
          
          {{   Auth::user()->firstname  }} {{ Auth::user()->lastname; }}</span></a>
             <ul class="dropdown-menu">
                <li><a href="{{url('users/profile') }}"><i class="fa fa-user"></i> My Profile</a></li>
        
                <li class="divider"></li>
                <li><a href="#" class="text-danger"><i class="fa fa-lock"></i> Logout</a></li>
              </ul>
             
                <li><a href="#"  class="settings dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell animated shake"></i><span class="badge bg-success"></span></a>
                  <ul class="dropdown-menu notifications">
                  
           
                	<li>
           
                      <a href="#"><i class="fa fa-clock-o noty-icon bg-warning"></i> 
                        <span class="description">Order number Order No from Customer that will fal off from Bounty Level 1 and 2 on </span>
                        <span class="time"> <i class="fa fa-clock-o"></i> </span>
                      </a>
                    </li>
                    
              
                

                    <li><a href="#" class="btn bg-primary">View All</a></li>
                  </ul>
                </li>
                <li><a href="#" class="settings"><i class="fa fa-cogs settings-toggle"></i><span class="badge bg-info"></span></a></li>
              </ul>
            </div><!-- /.navbar-collapse -->
          </nav> <!-- /.navbar -->
          
           <!-- .box-holder -->
          <div class="box-holder">

           @include('layouts.navigation')
           
           
          <!-- end header-->
          
          
@yield('content') 
<div class="footer">
  							© 2014 <a href="http://zmactransport.com">ZMac Transportation</a>
  						</div>

  					</div> <!-- /.content -->

  					<!-- .right-sidebar -->
  					<div class="right-sidebar right-sidebar-hidden">
  						<div class="right-sidebar-holder">

  							<a href="{{ url('logout')}}" class="btn btn-danger btn-block">Logout </a>


  							<h4 class="page-header text-primary text-center">
  								Theme Options
  								<a  href="#"  class="theme-panel-close text-primary pull-right"><strong><i class="fa fa-times"></i></strong></a>
  							</h4>

  							<ul class="list-group theme-options">
  								<li class="list-group-item" href="#">	
  									
  									<div class="form-group backgroundImage hidden" >
  										<label class="control-label">Paste Image Url and then hit enter</label>
  										<input type="text" class="form-control" id="backgroundImageUrl" />
  									</div>
					
		</li>
		<li class="list-group-item" href="#">Predefined Themes
			<ul class="list-inline predefined-themes"> 
				<li><a class="badge" style="background-color:#54b5df" data-color-primary="#54b5df" data-color-secondary="#2f4051" data-color-link="#FFFFFF"> &nbsp; </a></li>
				<li><a class="badge" style="background-color:#d85f5c" data-color-primary="#d85f5c" data-color-secondary="#f0f0f0" data-color-link="#474747"> &nbsp; </a></li>
				<li><a class="badge" style="background-color:#3d4a5d" data-color-primary="#3d4a5d" data-color-secondary="#edf0f1" data-color-link="#777e88"> &nbsp; </a></li>
				<li><a class="badge" style="background-color:#A0B448" data-color-primary="#A0B448" data-color-secondary="#485257" data-color-link="#AFB5AA"> &nbsp; </a></li>
				<li><a class="badge" style="background-color:#2FA2D1" data-color-primary="#2FA2D1" data-color-secondary="#484D51" data-color-link="#A5B1B7"> &nbsp; </a></li>
				<li><a class="badge" style="background-color:#2f343b" data-color-primary="#2f343b" data-color-secondary="#525a65" data-color-link="#FFFFFF"> &nbsp; </a></li>
			</ul>
		</li>
	</ul>
</div>	<!-- /.right-sidebar-holder -->
</div>  <!-- /.right-sidebar -->

<!-- /rightside bar -->

</div> <!-- /.box-holder -->
</div><!-- /.site-holder -->
   
    <!-- <p>Month1  //$dashboard_data->month1_revenue </p>-->
  	
		</div><!-- end site holder-->
    </body>
</html>