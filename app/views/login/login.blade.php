<section class="login">



<div class="login-box" style="width:400px; margin:10% auto 0 auto; background:#FFF; padding:24px; border-radius:10px; -moz-border-radius:10px; -webkit-border-radius:10px;">
 
<h2 style="color:#2F4051; margin-bottom:25px;"><i class="fa fa-users"></i> ZBoard Login Page</h2>
		@if(Session::has('message'))
            <p class="alert">{{ Session::get('message') }}</p>
        @endif
        
     <div class="input-box">
      <div class="row">
        <div class="col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1">
			{{ Form::open(array('url'=>'users/signin', 'role' =>'role')) }}
   
          <div class="input-group form-group">
            <span style="background:#fff" class="input-group-addon"><i class='fa fa-envelope'></i></span>
            {{ Form::text('email', null, array('class'=>'input-block-level form-control', 'placeholder'=>'Email Address', 'style' =>'color:#000')) }}
          </div>
          <div class="input-group form-group">
          <span style="background:#fff" class="input-group-addon"><i class='fa fa-key'></i></span>
          {{ Form::password('password', array('class'=>'input-block-level form-control', 'placeholder'=>'Password', 'style' =>'color:#000')) }}
          </div>
 			
    {{ Form::submit('Login', array('class'=>'btn btn-large btn-primary btn-block'))}}
    {{Form::token()}}
{{ Form::close() }}




</div></div>
</div></div>


</section>