<section id="register">
    <div class="row animated fadeILeftBig">
     <div class="login-holder col-md-6 col-md-offset-3">
<h2 style="color:#2F4051"><i class="fa fa-users"></i> ZBoard Registration Page</h2>
<ul>
        @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
{{ Form::open(array('url'=>'users/create', 'class'=>'form-signup', 'role' => 'form', 'name' => 'register_form')) }}
   
 
    
 	<div class="form-group">
    {{ Form::text('firstname', null, array('class'=>'input-block-level form-control', 'placeholder'=>'First Name')) }}
    </div>
    <div class="form-group">
    {{ Form::text('lastname', null, array('class'=>'input-block-level form-control', 'placeholder'=>'Last Name')) }}
    </div>
    <div class="form-group">
    {{ Form::text('email', null, array('class'=>'input-block-level form-control', 'placeholder'=>'Email Address')) }}
    </div>
    <div class="form-group">
    {{ Form::password('password', array('class'=>'input-block-level form-control', 'placeholder'=>'Password')) }}
    </div>
    <div class="form-group">
    {{ Form::password('password_confirmation', array('class'=>'input-block-level form-control', 'placeholder'=>'Confirm Password')) }}
    </div>
 
    {{ Form::submit('Register', array('class'=>'btn btn-large btn-primary btn-block'))}}
{{ Form::close() }}

</div></div></section>