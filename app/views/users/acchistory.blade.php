@extends('layouts.main')

@section('content')
<div class="content"> 

  <div class="row">
  							<div class="col-mod-12">
								<!-- start breadcrumbs -->
  								<ul class="breadcrumb">
  									<li><a href="{{url('users/dashboard')}}">Dashboard</a></li>
                                    <li>History</li>
  								</ul><!-- end breadc-->

  							

  							

  								<h3 class="page-header"><i class="fa fa fa-dashboard"></i> History <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>
									 
  								<blockquote class="page-information hidden">
  									<p>
  										Click on a customer to view order history. 
  									</p>
  								</blockquote>
  							</div><!-- end col-md-12 -->
  						</div><!-- end row -->
                        
                        <!-- Info Boxes -->
  						<div class="row panel-body" style="background-color:#2F343B">
  							<div class="col-md-4">
  								<div class="info-box  bg-info  text-white">
  									<div class="info-icon bg-info-dark">
  										<i class="fa fa-dollar fa-4x"></i>
  									</div>
  									<div class="info-details">
  										<h4>Last Quarter Sales   <span class="pull-right">${{number_format($dashboard_data->last_quarter_sales, 2, '.',',')}}
										  </span></h4>
  										<p style="visibility:hidden">Last Month <span class="badge pull-right bg-white text-info"> $</span> </p>
  									</div>
  								</div><!-- end info box-->
  							</div><!-- end col-md4-->
  							<div class="col-md-4 ">
  								<div class="info-box  bg-success  text-white">
  									<div class="info-icon bg-success-dark">
  										<i class="fa fa-road fa-4x"></i>
  									</div>
  									<div class="info-details">
  										<h4>Last Quarter Margin   <span class="pull-right">${{ number_format($dashboard_data->last_quarter_margin) }}</span></h4>
  										<p style="visibility:hidden">Last Month<span class="badge pull-right bg-white text-success">  </span> </p>
  									</div>
  								</div><!-- end info box-->
  							</div><!-- end col-md4-->
  							<div class="col-md-4">
  								<div class="info-box  bg-warning  text-white">
  									<div class="info-icon bg-warning-dark">
  										<i class="fa fa-group fa-4x"></i>
  									</div>
  									<div class="info-details">
  										<h4>Last Quarter GP%   <span class="pull-right">{{ $dashboard_data->last_quarter_gp_por*100 }}%</span></h4>
  										<p style="visibility:hidden">Last Quarter GP %</p>
  									</div>
  								</div><!-- end info box-->
  							</div><!-- end col md 4-->
  						</div><!-- end row-->
                        
                        
	<!-- Basic Wizard -->
          <div class="row">
           <div class="col-md-12">
            <div class="panel panel-dat">
             <div class="panel-heading">
              <h3 class="panel-title text-primary">
               Incentives History
               <span class="pull-right">
                <a href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a>
                <a href="#" class="panel-close"><i class="fa fa-times"></i></a>
              </span>
            </h3>
          </div>
          <div class="panel-body">

			<table class="table table-bordered table-hover table-striped" id="aeHistory" >
            	<thead>
                    <th>April 2014</th>
                    <th>March 2014</th>
                    <th>February 2014</th>
                    <th>January 2014</th>
                    <th>December 2013</th>
                    <th>Novemeber 2013</th>
                    <th>October 2013</th>
                    <th>September 2014</th>
                </thead>
                <tbody>
               
                	<tr>
                        <th>${{$incentives->april14}}</th>
                        <th>${{$incentives->march14}}</th>
                        <th>${{$incentives->february14}}</th>
                        <th>${{$incentives->january14}}</th>
                        <th>${{$incentives->december13}}</th>
                        <th>${{$incentives->novemeber13}}</th>
                        <th>${{$incentives->october13}}</th>
                        <th>${{$incentives->september13}}</th>
                    </tr>
               
                </tbody>
                <tfoot>
                    <th>April 2014</th>
                    <th>March 2014</th>
                    <th>February 2014</th>
                    <th>January 2014</th>
                    <th>December 2013</th>
                    <th>Novemeber 2013</th>
                    <th>October 2013</th>
                    <th>September 2014</th>
                </tfoot>
    	</table>



</div> <!-- /panel body -->	
</div><!-- end panel-->	
</div><!-- end col-->
</div><!-- end row-->

<!-- start co piece and bank history--><div class="row">
	<!-- start bank histopry--><div class="col-md-6" style="background-color:#29a2d7;min-height:500px;">
		<div class="panel-heading text-primary">
            	<h3 class="panel-title text-white">
                	<i class="fa fa-dollar"></i>
                    Quaterly Bank Paid
                </h3>
            </div><!-- end panel heading-->
    	<div class="panel">
        	<div class="panel-heading text-primary">
            	<h3 class="panel-title">
                	
                    Payouts
                </h3>
            </div><!-- end panel heading-->
       <div class="panel-body">
       	<table class="table table-bordered table-hover table-striped display" id="baHistory" >
            	<thead>
                    <th>Date</th>
                    <th>Payout</th>
                </thead>
                
                <tbody>
                
                @foreach($idata as $dati)
                @if($dati->quaterly_bank_paid > 0)
                	<tr>
                        <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d', $dati->date)->toFormattedDateString();}}</td>
                        <td>${{ number_format($dati->quaterly_bank_paid)}}</td>
                    </tr>
                    @endif
                @endforeach
                </tbody>
                
                <tfoot>
                    <th>Period</th>
                    <th>Incentive</th>
                </tfoot>
    	</table>

       </div><!-- end panel body-->
       </div><!-- end panel-->
    
    </div><!-- end bank history-->
    <!-- start copiece histopry--><div class="col-md-6" style="background-color:#F9A94A;min-height:500px;">
    <div class="panel-heading text-primary">
            	<h3 class="panel-title text-white">
                	<i class="fa fa-dollar"></i>
                    Company Performance <a class="panel-title chart" href="{{ url('users/copchart')}}">Chart</a>
                </h3>
            </div><!-- end panel heading-->
    	<div class="panel">
        	<div class="panel-heading text-primary">
            	<h3 class="panel-title">
                	
                    Payouts
                </h3>
            </div><!-- end panel heading-->
       <div class="panel-body">
       	<table class="table table-bordered table-hover table-striped display" id="coHistory" >
            	<thead>
                    <th>Period</th>
                    <th>Incentive</th>
                </thead>
                
                <tbody>
                
                @foreach($idatac as $dati)
                	<tr>
                        <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d', $dati->date)->toFormattedDateString();}}</td>
                        <td>${{ number_format($dati->qtr_company_incentive)}}</td>
                    </tr>
                @endforeach
                </tbody>
                
                <tfoot>
                    <th>Period</th>
                    <th>Incentive</th>
                </tfoot>
    	</table>

       </div><!-- end panel body-->
       </div><!-- end panel-->
    
    </div><!-- end copiece history-->
    

</div><!-- end row-->



  						
<script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
{{HTML::script('js/jquery-ui-1.10.3.custom.min.js');}}
{{HTML::script('js/less-1.5.0.min.js');}}
{{HTML::script('js/jquery.ui.touch-punch.min.js');}}
{{HTML::script('js/bootstrap.min.js');}}
{{HTML::script('js/bootstrap-select.js');}}
{{HTML::script('js/bootstrap-switch.js');}}
{{HTML::script('js/jquery.tagsinput.js');}}
{{HTML::script('js/jquery.placeholder.js');}}


<!-- Load JS here for Faster site load =============================-->


<script src="../../js/bootstrap-typeahead.js"></script>
<script src="../../js/application.js"></script>
<script src="../../js/moment.min.js"></script>
<script src="../../js/jquery.dataTables.min.js"></script>
<script src="../../js/jquery.sortable.js"></script>
<script type="text/javascript" src="../../js/jquery.gritter.js"></script>
<script src="../../js/jquery.nicescroll.min.js"></script>
<script src=../../"js/prettify.min.js"></script>
<script src="../../js/jquery.noty.js"></script>
<script src="../../js/bic_calendar.js"></script>
<script src="../../js/jquery.accordion.js"></script>
<script src="../../js/skylo.js"></script>

<script src="../../js/theme-options.js"></script>


<script src="../../js/bootstrap-progressbar.js"></script>
<script src="../../js/bootstrap-progressbar-custom.js"></script>
<script src="../../js/bootstrap-colorpicker.min.js"></script>
<script src="../../js/bootstrap-colorpicker-custom.js"></script>



<script src="../../js/raphael-min.js"></script>
<script src="../../js/morris-0.4.3.min.js"></script>
<script src="../../js/morris-custom-3.js"></script>

<script src="../../js/charts/jquery.sparkline.min.js"></script>	



<script src="../../js/bootstrap-tour.js"></script>

<!-- Page script File  =============================-->
<script src="../../js/tooltips-popovers.js"></script>




<!-- Core Jquery File  =============================-->
<script src="../../js/core.js"></script>
<script src="../../js/dashboard-custom.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/less.js/1.5.0/less.min.js"></script>
  <script src="../../js/bootstrap-datatables.js"></script>
<script src="../../js/dataTables-custom.js"></script>
@stop