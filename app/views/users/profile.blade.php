@extends('layouts.main')

@section('content')
<div class="content">

<div class="row">

	<div class="col-mod-12">
								<!-- start breadcrumbs -->
  								<ul class="breadcrumb">
  									<li><a href="{{url('users/dashboard')}}">Dashboard</a></li>
                                    <li>My Profile</li>
  								</ul><!-- end breadc-->

  								

  								<h3 class="page-header"><i class="fa fa fa-dashboard"></i> Profile <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>

  								<blockquote class="page-information hidden">
  									<p>
  										The profile page is where you are going to be able to change your avatar, color theme, and more coming.
  									</p>
  								</blockquote>
  							</div><!-- end col-md-12 -->
    
</div><!-- end row-->

<div class="row">
	<div class="col-md-6"> 
      
     <div class="panel panel-cascade">
     <div class="panel-heading bg-primary">
            	<h3 class="panel-title text-white">
                	<i class="fa fa-camera"></i>
                    My Profile Picture
                </h3>
            </div><!-- end panel heading-->
		<div class="panel-body">
      	{{ Form::open(array('action' => 'UsersController@post_upload', 'files'=>true, 'class'=>'upload_avatar')) }}
         
          <div class="form-group">
          FIle Upload {{ Form::input('file', null) }}
          </div>
          {{ Form::submit('Upload', array('class'=>'btn btn-default btn-primary'))}}
      {{ Form::close() }}
      	</div><!-- end panel body-->
     </div><!-- end panel-->            
      
	</div><!-- end col-->
    
    <div class="col-md-6">
    
    <div class="panel panel-cascade">
     <div class="panel-heading bg-primary">
            	<h3 class="panel-title text-white">
                	<i class="fa fa-lock"></i>
                    Change Password
                </h3>
            </div><!-- end panel heading-->
		<div class="panel-body">
        @if(Session::has('message'))
            <p class="alert">{{ Session::get('message') }}  </p>
            
            @if($errors)<ul>
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
        	@endforeach
            </ul>@endif
        @endif
        
        
      	{{ Form::open(array('action' => 'UsersController@postUserPasswordChange', 'files'=>true, 'class'=>'upload_avatar')) }}
         
          <div class="form-group">
          	{{ Form::password('password', array('class'=>'input-block-level', 'placeholder'=>'Old Password')) }}
           </div>
           <div class="form-group">
    		{{ Form::password('new_password', array('class'=>'input-block-level', 'placeholder'=>'New Password')) }}
             </div>
             <div class="form-group">
    		{{ Form::password('confirm_new_password', array('class'=>'input-block-level', 'placeholder'=>'Confirm New Password')) }}
             </div>
         
          {{ Form::submit('Change Password', array('class'=>'btn btn-default btn-primary'))}}
          {{Form::token()}}
      {{ Form::close() }}
      	</div><!-- end panel body-->
     </div><!-- end panel--> 
    
    </div><!-- end col 6-->

</div><!-- end row-->

<script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
{{HTML::script('js/jquery-ui-1.10.3.custom.min.js');}}
{{HTML::script('js/less-1.5.0.min.js');}}
{{HTML::script('js/jquery.ui.touch-punch.min.js');}}
{{HTML::script('js/bootstrap.min.js');}}
{{HTML::script('js/bootstrap-select.js');}}
{{HTML::script('js/bootstrap-switch.js');}}
{{HTML::script('js/jquery.tagsinput.js');}}
{{HTML::script('js/jquery.placeholder.js');}}

<script src="../../js/bootstrap-typeahead.js"></script>
<script src="../../js/application.js"></script>
<script src="../../js/moment.min.js"></script>
<script src="../../js/jquery.dataTables.min.js"></script>
<script src="../../js/jquery.sortable.js"></script>
<script type="text/javascript" src="../../js/jquery.gritter.js"></script>
<script src="../../js/jquery.nicescroll.min.js"></script>
<script src=../../"js/prettify.min.js"></script>
<script src="../../js/jquery.noty.js"></script>

<script src="../../js/jquery.accordion.js"></script>
<script src="../../js/skylo.js"></script>

<script src="../../js/theme-options.js"></script>


<script src="../../js/bootstrap-progressbar.js"></script>
<script src="../../js/bootstrap-progressbar-custom.js"></script>
<script src="../../js/bootstrap-colorpicker.min.js"></script>
<script src="../../js/bootstrap-colorpicker-custom.js"></script>



<!-- Page script File  =============================-->
<script src="../../js/tooltips-popovers.js"></script>




<!-- Core Jquery File  =============================-->
<script src="../../js/core.js"></script>
<script src="../../js/dashboard-custom.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/less.js/1.5.0/less.min.js"></script>
  <script src="../../js/bootstrap-datatables.js"></script>
<script src="../../js/dataTables-custom.js"></script>
@stop
