@extends('layouts.main')

@section('content')
<div class="content"> 
	
    
    <div class="row">
  							<div class="col-mod-12">
								<!-- start breadcrumbs -->
  								<ul class="breadcrumb">
  									<li><a href="url('users.dashboard')">Dashboard</a></li>
                                    <li>History</li>
  								</ul><!-- end breadc-->

  								<div class="form-group hiddn-minibar pull-right">
  								

  								
  								</div><!-- end form group search-->

  								<h3 class="page-header"><i class="fa fa fa-dashboard"></i>History <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>

  								<blockquote class="page-information hidden">
  									<p>
  										<b>History Page</b> is the page where you can see details about your loads for the customer.
  									</p>
  								</blockquote>
  							</div><!-- end col-md-12 -->
  						</div><!-- end row -->
						
                        <!-- Invoice -->
          <div class="panel panel-cascade panel-invoice">
           <div class="panel-heading">
            <h3 class="panel-title">
             {{$customer_detail->customername}}
             
          </h3>
        </div>
        <div class="panel-body">
          Employee: <strong>{{$dashboard_data->salesperson}}</strong>
          <div class="row">
           <div class="col-md-6 invoice-from">
            <ul class="list-unstyled">
             <li>
              <strong>Client</strong>
              <span>{{$customer_detail->customerid}} | {{$customer_detail->customername}}</span>
              <span>{{$customer_detail->cp_city}},{{$customer_detail->cp_state}}</span>
              <span>{{$customer_detail->cp_phone}}</span>
            </li>
          </ul>
        </div>
        <div class="col-md-6 invoice-to">
          
      </div>
    </div>
    <div class="row">
     <div class="col-md-12">
     
    
    	<div class="panel">
        	 <div class="panel-heading bg-primary text-primary">
            	<h3 class="panel-title  text-white">
                	<i class="fa fa-dollar"></i>
                   Client Loads
                </h3>
            </div><!-- end panel heading-->
       <div class="panel-body">
      <table class="table table-bordered table-hover table-striped display" id="pHistory">
           <thead>
            <tr>
             <th style="cursor:pointer">Customer Name</th>
             <th style="cursor:pointer">Order No.</th>
             <th style="cursor:pointer">Load Amount</th>
             <th style="cursor:pointer">Order Date</th>
             <th style="cursor:pointer">GP %</th>
           </tr>
         </thead>
         <tbody>
         @foreach ($orderData as $orData)
          <tr>
           <td>{{$orData->customername}}</td>
           <td>{{$orData->orderno}}</td>
           <td>${{number_format($orData->load_amount)}}</td>
           <td>{{$orData->order_date}}</td>
           <td>{{$orData->profit_por * 100}}</td>
          </tr>
          @endforeach
          </tbody>
          <tfoot>
           <tr>
             <th style="cursor:pointer">Customer Name</th>
             <th style="cursor:pointer">Order No.</th>
             <th style="cursor:pointer">Load Amount</th>
             <th style="cursor:pointer">Order Date</th>
             <th style="cursor:pointer">GP %</th>
           </tr>
         </tfoot>
       </table>
       </div><!-- end panel body-->
       </div><!-- end panel-->
  </div>
</div>
</div>
</div>
</div>

                        
  					</div> <!-- /.content -->


  						
<script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
{{HTML::script('js/jquery-ui-1.10.3.custom.min.js');}}
{{HTML::script('js/less-1.5.0.min.js');}}
{{HTML::script('js/jquery.ui.touch-punch.min.js');}}
{{HTML::script('js/bootstrap.min.js');}}
{{HTML::script('js/bootstrap-select.js');}}
{{HTML::script('js/bootstrap-switch.js');}}
{{HTML::script('js/jquery.tagsinput.js');}}
{{HTML::script('js/jquery.placeholder.js');}}


<!-- Load JS here for Faster site load =============================-->


<script src="../../js/bootstrap-typeahead.js"></script>
<script src="../../js/application.js"></script>
<script src="../../js/moment.min.js"></script>
<script src="../../js/jquery.dataTables.min.js"></script>
<script src="../../js/jquery.sortable.js"></script>
<script type="text/javascript" src="../../js/jquery.gritter.js"></script>
<script src="../../js/jquery.nicescroll.min.js"></script>
<script src=../../"js/prettify.min.js"></script>
<script src="../../js/jquery.noty.js"></script>
<script src="../../js/bic_calendar.js"></script>
<script src="../../js/jquery.accordion.js"></script>
<script src="../../js/skylo.js"></script>

<script src="../../js/theme-options.js"></script>


<script src="../../js/bootstrap-progressbar.js"></script>
<script src="../../js/bootstrap-progressbar-custom.js"></script>
<script src="../../js/bootstrap-colorpicker.min.js"></script>
<script src="../../js/bootstrap-colorpicker-custom.js"></script>






<!-- Core Jquery File  =============================-->
<script src="../../js/core.js"></script>
<script src="../../js/dashboard-custom.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/less.js/1.5.0/less.min.js"></script>
  <script src="../../js/bootstrap-datatables.js"></script>
@stop
