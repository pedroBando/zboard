@extends('layouts.main')

@section('content')
<div class="content"> 

  <div class="row">
  							<div class="col-mod-12">
								<!-- start breadcrumbs -->
  								<ul class="breadcrumb">
  									<li><a href="{{url('users/dashboard')}}">Dashboard</a></li>
                                    <li>My Customers</li>
  								</ul><!-- end breadc-->

  							

  							

  								<h3 class="page-header"><i class="fa fa fa-dashboard"></i>My Customers <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>
									 
  								<blockquote class="page-information hidden">
  									<p>
  										My Customers. 
  									</p>
  								</blockquote>
  							</div><!-- end col-md-12 -->
  						</div><!-- end row -->
                        
                        
                        
	<!-- Basic Wizard -->
          <div class="row">
           <div class="col-md-12">
            <div class="panel panel-dat">
             <div class="panel-heading">
              <h3 class="panel-title text-primary">
               Customers
               <span class="pull-right">
                <a href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a>
                <a href="#" class="panel-close"><i class="fa fa-times"></i></a>
              </span>
            </h3>
          </div>
          <div class="panel-body">

			<table class="table table-bordered table-hover table-striped tooltips" id="aeList" >
            	<thead>
                    <th style="cursor:pointer">Customer Name</th>
                    <th style="cursor:pointer">CP Code</th>
                    <th style="cursor:pointer">Revenue</th>
                    <th style="cursor:pointer">GP %</th>
                    <th style="cursor:pointer">Orders Count</th>
                </thead>
                <tbody>
                @foreach($cData as $dat)
                	<tr>
                        <td>{{ $dat->cp_name}}</td>
                        <td>{{ $dat->cp_code}}</td>
                        <td>${{ number_format($dat->ae_revenue, "2",".",",")}}</td>
                        <td>{{ number_format($dat->ae_gp_por*100, 2,".",",")}}%</td>
                        <td>{{ $dat->ae_order_count}}</td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                    <th style="cursor:pointer">Customer Name</th>
                    <th style="cursor:pointer">CP Code</th>
                    <th style="cursor:pointer">Revenue</th>
                    <th style="cursor:pointer">GP %</th>
                    <th style="cursor:pointer">Orders Count</th>
                </tfoot>
    	</table>



</div> <!-- /panel body -->	
</div><!-- end panel-->	
</div><!-- end col-->
</div><!-- end row-->




  						
<script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
{{HTML::script('js/jquery-ui-1.10.3.custom.min.js');}}
{{HTML::script('js/less-1.5.0.min.js');}}
{{HTML::script('js/jquery.ui.touch-punch.min.js');}}
{{HTML::script('js/bootstrap.min.js');}}
{{HTML::script('js/bootstrap-select.js');}}
{{HTML::script('js/bootstrap-switch.js');}}
{{HTML::script('js/jquery.tagsinput.js');}}
{{HTML::script('js/jquery.placeholder.js');}}


<!-- Load JS here for Faster site load =============================-->


<script src="../../js/bootstrap-typeahead.js"></script>
<script src="../../js/application.js"></script>
<script src="../../js/moment.min.js"></script>
<script src="../../js/jquery.dataTables.min.js"></script>
<script src="../../js/jquery.sortable.js"></script>
<script type="text/javascript" src="../../js/jquery.gritter.js"></script>
<script src="../../js/jquery.nicescroll.min.js"></script>
<script src=../../"js/prettify.min.js"></script>
<script src="../../js/jquery.noty.js"></script>
<script src="../../js/bic_calendar.js"></script>
<script src="../../js/jquery.accordion.js"></script>
<script src="../../js/skylo.js"></script>

<script src="../../js/theme-options.js"></script>


<script src="../../js/bootstrap-progressbar.js"></script>
<script src="../../js/bootstrap-progressbar-custom.js"></script>
<script src="../../js/bootstrap-colorpicker.min.js"></script>
<script src="../../js/bootstrap-colorpicker-custom.js"></script>



<script src="../../js/raphael-min.js"></script>
<script src="../../js/morris-0.4.3.min.js"></script>
<script src="../../js/morris-custom-3.js"></script>

<script src="../../js/charts/jquery.sparkline.min.js"></script>	



<script src="../../js/bootstrap-tour.js"></script>

<!-- Page script File  =============================-->
<script src="../../js/tooltips-popovers.js"></script>




<!-- Core Jquery File  =============================-->
<script src="../../js/core.js"></script>
<script src="../../js/dashboard-custom.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/less.js/1.5.0/less.min.js"></script>
  <script src="../../js/bootstrap-datatables.js"></script>
<script src="../../js/dataTables-custom.js"></script>
@stop