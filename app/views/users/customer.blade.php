@extends('layouts.main')

@section('content')
<div class="content"> 
	
    
    <div class="row">
  							<div class="col-mod-12">
								<!-- start breadcrumbs -->
  								<ul class="breadcrumb">
  									<li><a href="url('users.dashboard')">Dashboard</a></li>
                                    <li><a href="url('users.overview')">Overview</a></li>
                                    <li><a href="#">Customer</a></li>
  								</ul><!-- end breadc-->
                             </div> <!-- end col md 12 -->
    </div><!-- end row-->
    
    <div class="row">
							<div class="col-mod-6">
  							 <div class="form-group hiddn-minibar pull-right">
  								
								<label for="inputEmail1" class="col-lg-2 col-md-3 control-label">Customers</label><br/>
                            
								<div class="col-lg-10 col-md-9">
								<select id="customers" class="form-control" onChange="document.location = this.value" value="GO">
									@foreach($customerData as $cData)                       	
                             		 <option value="{{url('/customers/' .$cData->customerid)}}" {{$mycustomer == $cData->customerid ? 'selected':''}}><a  href="/customers/{{$cData->customerid}}">{{ $cData->customername}} - {{ $cData->customerid}}</a></option>
 	                       			@endforeach
								</select>
                               
								</div><!-- end col-->	
  							</div><!-- end form group search-->
                           </div><!-- end col 6-->
  							
                            <div class="col-md-6">
                            <h3 class="page-header"><i class="fa fa fa-dashboard"></i> Customer: {{ $customer_detail->customername}} - {{ $customer_detail->customerid}} <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3><br/>    
                            
                            

  								<blockquote class="page-information hidden">
  									<p>
  										<b>Customer Overview Page</b> is the page where you can see your current bounty status with the customer..
  									</p>
  								</blockquote>
                               
                               
                                
                                 
                             
  							</div><!-- end col-md-12 -->  
  						</div><!-- end row -->
                        
                        <div class="row">
                         <div class="col-md-12">
                         
                            <div class="col-md-4">	
                                <div class="alert alert-info alert-dismissable">
                                    	<b>Company Name: </b>{{$customer_detail->customername}}<br />
                                    	<b>Company Controller:</b> {{$customer_detail->customerid}}   
                           		</div><!-- end alert-->
                            </div><!-- end col-->
                                
                            <div class="col-md-4">	
                                <div class="alert alert-info alert-dismissable">
                                    	<b>Contact Name: </b>{{$customer_detail->cp_contact}}<br />
                                    	<b>Contact Phone:</b> 1-{{$customer_detail->cp_phone}}   
                           		</div><!-- end alert-->
                            </div><!-- end col-->    
                            
                            <div class="col-md-4">    
                                <div class="alert alert-info alert-dismissable">
                                    	<b>City:</b> {{$customer_detail->cp_city}}<br />
                                    	<b>State:</b> {{$customer_detail->cp_state}}
                           		</div><!-- end alert-->
                            </div><!-- end col-->
                         </div><!-- end col-->
                         
                         
                        </div><!-- end row--->

						<!-- Info Boxes -->
  						<div class="row panel-body" style="background-color:#2F343B">
  							<div class="col-md-4">
  								<div class="info-box  bg-info  text-white">
  									<div class="info-icon bg-info-dark">
  										<i class="fa fa-dollar fa-4x"></i>
  									</div>
  									<div class="info-details">
  										<h4>Bounty Level 1 Revenue   <span class="pull-right">$ {{ number_format($customer_detail->level12_revenue, 2, ".", ",")}}
										  </span></h4>
  										<p >Gross Profit % <span class="badge pull-right bg-white text-info"> {{number_format(($customer_detail->level12_gp_por*100),2,".",",")}}%</span> </p>
  									</div>
  								</div><!-- end info box-->
  							</div><!-- end col-md4-->
  							<div class="col-md-4 ">
  								<div class="info-box  bg-success  text-white">
  									<div class="info-icon bg-success-dark">
  										<i class="fa fa-road fa-4x"></i>
  									</div>
  									<div class="info-details">
  										<h4>Bounty Level 2 Revenue   <span class="pull-right">$ {{ number_format($customer_detail->level12_revenue, 2, ".", ",")}}
										  </span></h4>
  										<p >Gross Profit % <span class="badge pull-right bg-white text-info"> {{number_format(($customer_detail->level12_gp_por*100),2,".",",")}}%</span> </p>
  									</div>
  								</div><!-- end info box-->
  							</div><!-- end col-md4-->
  							<div class="col-md-4">
  								<div class="info-box  bg-warning  text-white">
  									<div class="info-icon bg-warning-dark">
  										<i class="fa fa-group fa-4x"></i>
  									</div>
  									<div class="info-details">
  										<h4>Bounty Level 3 Revenue   <span class="pull-right">$ {{ number_format($customer_detail->level3_revenue, 2, ".", ",")}}
										  </span></h4>
  										<p >Gross Profit % <span class="badge pull-right bg-white text-info"> {{number_format(($customer_detail->level3_gp_por*100),2,".",",")}}%</span> </p>
  									</div>
  								</div><!-- end info box-->
  							</div><!-- end col md 4-->
  						</div><!-- end row-->

  						<!-- Demo Panel -->

                        <div class="row">
  							<!--- Pie chart -->
  							<div class="col-md-4" style="background-color:#29a2d7">
                                 <div class="panel-heading">
  										<h3 class="panel-title text-white">
  											<i class="fa fa-dollar"></i> Bounty Level 1
  										</h3>
  							</div>	
                            <div id="bounty1"></div>  
                            </div>
                
                            <div class="col-md-4" style="background-color:#77af3b">
                                        <div class="panel-heading">
                                                    <h3 class="panel-title text-white">
                                                        <i class="fa fa-dollar"></i> Bounty Level 2
                                                    </h3>
                                        </div>
  										<div id="bounty2"></div>          
                   			</div>
  							
                            <div class="col-md-4" style="background-color:#f79219">
                                        <div class="panel-heading">
                                                    <h3 class="panel-title text-white">
                                                        <i class="fa fa-dollar"></i> Bounty Level 3
                                                    </h3>
                                        </div>
  									    <div id="bounty3"></div>
  							</div>
                            
  						</div>
                        <div class="row">
                     	  <div class="col-md-4 nopadding">
                          <div class="panel">
  									<div class="panel-heading text-white" style="background-color:#0B62A4">
  										<h3 class="panel-title">
  											<i class="fa fa-check-square-o"></i>
  											To Do List
  											<span class="pull-right ">
  												<a href="#" class="panel-minimize text-white"><i class="fa fa-chevron-up"></i></a>
  												<a href="#" class="panel-close text-white"><strong><i class="fa fa-times"></i></strong></a>
  											</span>
  										</h3>
  									</div>
  									<div class="panel-body">
  										<ul class="list-group list-todo">
  											@if ($customer_detail->level12_revenue > '5000')
  												<li class="list-group-item finished">
                                            @else
                                            	<li class="list-group-item">
                                            @endif
  												<i class="fa fa-check-square-o finish fa-square-o "></i>
  												<span>Produce $5,000 in revenue. You need ${{number_format((5000 - $customer_detail->level12_revenue ),2,".",",")}}</span>
                                                @if ($customer_detail->level12_revenue > '5000')
  													<label class="label label-success pull-right">Completed</label>
                                                @else
                                                	<label class="label label-danger pull-right">Not Completed</label>
                                                @endif
  											</li>
                                            @if ($customer_detail->level12_gp_por > .149)
  												<li class="list-group-item finished">
                                            @else
                                            	<li class="list-group-item">
                                            @endif
  												<i class="fa fa-check-square-o finish fa-square-o "></i>
  												<span>Results in at least 15% GP%</span>
                                                @if ($customer_detail->level12_gp_por > .149)
  													<label class="label label-success pull-right">Completed</label>
                                                @else
                                                	<label class="label label-danger pull-right">Not Completed</label>
                                                @endif
  											</li>
                                            @if ($customer_detail->level12_gp_por > .149 && $customer_detail->level12_revenue > '5000')
  												<li class="list-group-item finished">
                                            @else
                                            	<li class="list-group-item">
                                            @endif
  												<i class="fa fa-check-square-o finish fa-square-o "></i>
  												<span>All requirement met.</span>
                                                @if ($customer_detail->level12_gp_por > .149 && $customer_detail->level12_revenue > '5000')
  													<label class="label label-sucess pull-right">Not Completed</label>
                                                @else
                                                	<label class="label label-danger pull-right">Not Completed</label>
                                                @endif
  											</li>
                                            
                                            
  										</ul>
                                       
                                        <script type="text/javascript">
											var chartBounty1 = <?php echo json_encode($chartBounty1); ?>
										</script>
  									</div>
  								</div>
                          </div>
                          
                           <div class="col-md-4 nopadding">
                          <div class="panel ">
  									<div class="panel-heading text-white" style="background-color:#347413">
  										<h3 class="panel-title">
  											<i class="fa fa-check-square-o"></i>
  											To Do List
  											<span class="pull-right ">
  												<a href="#" class="panel-minimize text-white"><i class="fa fa-chevron-up"></i></a>
  												<a href="#" class="panel-close text-white"><strong><i class="fa fa-times"></i></strong></a>
  											</span>
  										</h3>
  									</div>
  									<div class="panel-body">
  										<ul class="list-group list-todo">
  											@if ($customer_detail->level12_revenue > '20000')
  												<li class="list-group-item finished">
                                            @else
                                            	<li class="list-group-item">
                                            @endif
  												<i class="fa fa-check-square-o finish fa-square-o "></i>
  												<span>Produce $20,000 in revenue. You need ${{number_format((20000 - $customer_detail->level12_revenue ),2,".",",")}}</span>
                                                @if ($customer_detail->level12_revenue > '20000')
  													<label class="label label-success pull-right">Completed</label>
                                                @else
                                                	<label class="label label-danger pull-right">Not Completed</label>
                                                @endif
  											</li>
                                            @if ($customer_detail->level12_gp_por > .189)
  												<li class="list-group-item finished">
                                            @else
                                            	<li class="list-group-item">
                                            @endif
  												<i class="fa fa-check-square-o finish fa-square-o "></i>
  												<span>Results in at least 19% GP%</span>
                                                @if ($customer_detail->level12_gp_por > .189)
  													<label class="label label-success pull-right">Completed</label>
                                                @else
                                                	<label class="label label-danger pull-right">Not Completed</label>
                                                @endif
  											</li>
                                            @if ($customer_detail->level12_gp_por > .189 && $customer_detail->level12_revenue > '20000')
  												<li class="list-group-item finished">
                                            @else
                                            	<li class="list-group-item">
                                            @endif
  												<i class="fa fa-check-square-o finish fa-square-o "></i>
  												<span>All requirement met.</span>
                                                @if ($customer_detail->level12_gp_por > .189 && $customer_detail->level12_revenue > '20000')
  													<label class="label label-sucess pull-right">Not Completed</label>
                                                @else
                                                	<label class="label label-danger pull-right">Not Completed</label>
                                                @endif
  											</li>
  										</ul>
                                        <script type="text/javascript">
											var chartBounty2 = <?php echo json_encode($chartBounty2); ?>
										</script>
  									</div>
  								</div>
                          </div>
                           <div class="col-md-4 nopadding">
                          <div class="panel ">
  									<div class="panel-heading text-white" style="background-color:#A14000">
  										<h3 class="panel-title">
  											<i class="fa fa-check-square-o"></i>
  											To Do List
  											<span class="pull-right ">
  												<a href="#" class="panel-minimize text-white"><i class="fa fa-chevron-up"></i></a>
  												<a href="#" class="panel-close text-white"><strong><i class="fa fa-times"></i></strong></a>
  											</span>
  										</h3>
  									</div>
  									<div class="panel-body ">
  										<ul class="list-group list-todo">
  											@if ($customer_detail->level3_revenue > '50000')
  												<li class="list-group-item finished">
                                            @else
                                            	<li class="list-group-item">
                                            @endif
  												<i class="fa fa-check-square-o finish fa-square-o "></i>
  												<span>Produce $50,000 in revenue. You need ${{number_format((50000 - $customer_detail->level3_revenue ),2,".",",")}}</span>
                                                @if ($customer_detail->level3_revenue > '50000')
  													<label class="label label-success pull-right">Completed</label>
                                                @else
                                                	<label class="label label-danger pull-right">Not Completed</label>
                                                @endif
  											</li>
                                            
                                            <li class="list-group-item"><span>Average revenue per order.</span> <label class="label label-danger pull-right">${{ number_format($customer_detail->level3_revpororder, 2, ".",",")}}</label></li>
                                            
                                            
                                            
  										</ul>
                                        <script type="text/javascript">
											var chartBounty3 = <?php echo json_encode($chartBounty3); ?>
											
										</script>
                                        <div class="popovers">
                                        
                                        <div class="btn btn-lg btn-block btn-warning" onclick="var target=childNodes[1];target.style.display='block';setTimeout(function(){target.style.display='none'},5000)">
  Bounty Level 3 Table
  <img src="{{ URL::asset('css/images/l3.png') }}" style="display:none;"></div>
                                        
                                        </div>
  									</div>
  								</div>
                          </div>
                        
                        </div>
                        <div class="row">
                        <div class="col-md-12">
            <div class="panel">
             <div class="panel-heading text-primary">
              <h3 class="panel-title">
               <i class="fa fa-truck"></i>
               Load Orders
               <span class="pull-right">
                <div class="btn-group code">
                 <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Classes used"><i class="fa fa-code"></i></a>
                 <ul class="dropdown-menu pull-right list-group" role="menu">
                  <li class="list-group-item"><code>.table-bordered</code></li>
                  <li class="list-group-item"><code>.table-hover</code></li>
                  <li class="list-group-item"><code>.table-striped</code></li>
                  <li class="list-group-item"><code>.display</code></li>
                </ul>
              </div>
              <a href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a>
              <a href="#" class="panel-close"><i class="fa fa-times"></i></a>
            </span>
          </h3>
        </div>
        <div class="panel-body">
          <div class="alert alert-info alert-dismissable">
           <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
           These are the load orders that are counting towards your bounties.
         </div>
         <table  class="table table-bordered table-hover table-striped display" id="loadOrders" >
           <thead>
            <tr>
             <th>Customer Name</th>
             <th>Load Amount</th>
             <th>Order Date</th>
             <th>GP%</th>
           </tr>
         </thead>
         <tbody>
         	@foreach ($orderData as $orData)
          <tr class="odd gradeX">
           <td>{{$orData->customername}}</td>
           <td>${{number_format($orData->load_amount, 0, ".",",")}}</td>
            <td>{{ $orData->order_date }}</td>
            <td class="center"> {{$orData->profit_por * 100}}</td>
          </tr>
          @endforeach
          
         </tfoot>
       </table>
     </div> <!-- /panel body -->
   </div>
 </div> <!-- /col-md-12 -->
                        </div>
                        <div class="row" >
                        	<div class="col-md-6" style="margin-top:30px;">
  								<div class="info-box  bg-info  text-white">
  									<div class="info-icon bg-primary-dark">
  										<i class="fa fa-dollar fa-4x"></i>
  									</div>
  									<div class="info-details">
  										<h4>Profit Margin (Month)   <span class="pull-right">21%</span></h4>
  										<p>Last Month <span class="badge pull-right bg-success text-white"> 18%  </span> </p>
  									</div>
  								</div>
  							</div>
                            <div class="col-md-6" style="margin-top:30px;">
  								<div class="info-box  bg-success  text-white">
  									<div class="info-icon bg-success-dark">
  										<i class="fa fa-dollar fa-4x"></i>
  									</div>
  									<div class="info-details">
  										<h4>Profit Margin (Quarter)   <span class="pull-right">21%</span></h4>
  										<p>Last Quarter <span class="badge pull-right bg-dark-primary text-white"> 18%  </span> </p>
  									</div>
  								</div>
  							</div>
                        </div>
                        
  					</div> <!-- /.content -->

  						
<script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
{{HTML::script('js/jquery-ui-1.10.3.custom.min.js');}}
{{HTML::script('js/less-1.5.0.min.js');}}
{{HTML::script('js/jquery.ui.touch-punch.min.js');}}
{{HTML::script('js/bootstrap.min.js');}}
{{HTML::script('js/bootstrap-select.js');}}
{{HTML::script('js/bootstrap-switch.js');}}
{{HTML::script('js/jquery.tagsinput.js');}}
{{HTML::script('js/jquery.placeholder.js');}}


<!-- Load JS here for Faster site load =============================-->


<script src="../../js/bootstrap-typeahead.js"></script>
<script src="../../js/application.js"></script>
<script src="../../js/moment.min.js"></script>
<script src="../../js/jquery.dataTables.min.js"></script>
<script src="../../js/jquery.sortable.js"></script>
<script type="text/javascript" src="../../js/jquery.gritter.js"></script>
<script src="../../js/jquery.nicescroll.min.js"></script>
<script src=../../"js/prettify.min.js"></script>
<script src="../../js/jquery.noty.js"></script>
<script src="../../js/bic_calendar.js"></script>
<script src="../../js/jquery.accordion.js"></script>
<script src="../../js/skylo.js"></script>

<script src="../../js/theme-options.js"></script>


<script src="../../js/bootstrap-progressbar.js"></script>
<script src="../../js/bootstrap-progressbar-custom.js"></script>
<script src="../../js/bootstrap-colorpicker.min.js"></script>
<script src="../../js/bootstrap-colorpicker-custom.js"></script>



<script src="../../js/raphael-min.js"></script>
<script src="../../js/morris-0.4.3.min.js"></script>
<script src="../../js/morris-custom.js"></script>
<script src="../../js/morris-custom-2.js"></script>

<script src="../../js/charts/jquery.sparkline.min.js"></script>	

<!-- NVD3 graphs  =============================-->
<script src="../../js/nvd3/lib/d3.v3.js"></script>
<script src="../../js/nvd3/nv.d3.js"></script>
<script src="../../js/nvd3/src/models/legend.js"></script>
<script src="../../js/nvd3/src/models/pie.js"></script>
<script src="../../js/nvd3/src/models/pieChart.js"></script>
<script src="../../js/nvd3/src/utils.js"></script>
<script src="../../js/nvd3/sample.nvd3.js"></script>
<script src="../../js/nvd3/nvd3-custom.js"></script>

<script src="../../js/bootstrap-tour.js"></script>

<!-- Page script File  =============================-->
<script src="../../js/tooltips-popovers.js"></script>

<!-- AMDCharts ================================================-->
<script src="../../js/amcharts/amcharts.js" type="text/javascript"></script>
<script src="../../js/amcharts/serial.js" type="text/javascript"></script>
<script src="../../js/amcharts/custom.js" type="text/javascript"></script>


<!-- Core Jquery File  =============================-->
<script src="../../js/core.js"></script>
<script src="../../js/dashboard-custom.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/less.js/1.5.0/less.min.js"></script>
  <script src="../../js/bootstrap-datatables.js"></script>
<script src="../../js/dataTables-custom.js"></script>
@stop