@extends('layouts.main')

@section('content')
<div class="content"> 
	
    
    <div class="row">
  							<div class="col-mod-12">
								<!-- start breadcrumbs -->
  								<ul class="breadcrumb">
  									<li>Dashboard</li>
  								</ul><!-- end breadc-->

  								

  								<h3 class="page-header"><i class="fa fa fa-dashboard"></i>Employee Dashboard for: {{$dashboard_data->salesperson}}<i class="fa fa-info-circle animated bounceInDown show-info"></i> <span class="pull-right">dddd</span></h3>
									 
  								<blockquote class="page-information hidden">
  									<p>
  										Dashboard is an "Easy To Read" real-time user interface, showing a graphical presentation of the current status and historical trends of an Zmac’s key performance indicators to enable that users can se their different stats.
  									</p>
  								</blockquote>
  							</div><!-- end col-md-12 -->
  						</div><!-- end row -->


  						<!-- Info Boxes -->
  						<div class="row panel-body" style="background-color:#2F343B">
  							<div class="col-md-4">
  								<div class="info-box  bg-info  text-white">
  									<div class="info-icon bg-info-dark">
  										<i class="fa fa-dollar fa-4x"></i>
  									</div>
  									<div class="info-details">
  										<h4>Current Month Sales   <span class="pull-right">$<?php $currentMOnthSales = $dashboard_data->current_month_sales;
										  // english notation (default)
										  echo number_format($currentMOnthSales, 2, '.',',');?>
										  </span></h4>
  										<p>Last Month <span class="badge pull-right bg-white text-info"> $<?php $number = $dashboard_data->onemonthago_revenue;
										  // english notation (default)
										  echo number_format($number, 2,'.', ',');?></span> </p>
  									</div>
  								</div><!-- end info box-->
  							</div><!-- end col-md4-->
  							<div class="col-md-4 ">
  								<div class="info-box  bg-success  text-white">
  									<div class="info-icon bg-success-dark">
  										<i class="fa fa-road fa-4x"></i>
  									</div>
  									<div class="info-details">
  										<h4>Current Month Gross Profit %    <span class="pull-right">{{ number_format($dashboard_data->current_month_gp_por*100) }}%</span></h4>
  										<p>Last Month<span class="badge pull-right bg-white text-success"> {{ number_format($dashboard_data->onemonthago_gp_por*100) }}% </span> </p>
  									</div>
  								</div><!-- end info box-->
  							</div><!-- end col-md4-->
  							<div class="col-md-4">
  								<div class="info-box  bg-warning  text-white">
  									<div class="info-icon bg-warning-dark">
  										<i class="fa fa-group fa-4x"></i>
  									</div>
  									<div class="info-details">
  										<h4>New Customers   <span class="pull-right">{{ $dashboard_data->new_customers }}</span></h4>
  										<p><a href="{{url('users/overview#allcustomers')}}">Total Customers</a> <span class="badge pull-right bg-white text-warning"><a href="{{url('users/overview#allcustomers')}}">{{ $dashboard_data->total_customers }}</a></span></p>
  									</div>
  								</div><!-- end info box-->
  							</div><!-- end col md 4-->
  						</div><!-- end row-->

  						<!-- Charts -->
  						<div class="row" style="padding-top:20px; background-color:#fff;">
  							<!-- Discrete Bar Chart -->
  							<div class="col-md-6">
                              <div class="panel panel-primary">
                                <div class="panel-heading">
                                  <h3 class="panel-title">
  										<i class="fa fa-bar-chart-o"></i>
  											Monthly Sales Totals & Goal
  											<span class="pull-right">
  												<a  href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a>
  												
  											</span>
  									</h3>
                                </div><!-- end panel heading-->
                                
                                
                                
                                <div class="panel-body">
                                  <div id="chartdiv" style="width:100%; height:400px;"></div>
                                </div><!-- end panel body-->
                              </div><!-- end panel-->
                              
                              <div class="info-box  bg-info  text-white">
  									<div class="info-icon bg-info-dark">
  										<i class="fa fa-truck fa-4x"></i>
  									</div>
  									<div class="info-details">
  										<h4>Current Month Loads  <span class="pull-right">{{ $dashboard_data->current_month_orders}}</span></h4>
  										<p>Last Month <span class="badge pull-right bg-white text-info">{{ $dashboard_data->last_month_orders }}</span> </p>
  									</div>
  								</div><!-- end info box--><!-- end info box-->
                              
                              <div class="panel panel-primary">
  							  	<div class="panel-heading bg-purple">
  									<h3 class="panel-title">
  										<i class="fa fa-bar-chart-o"></i>
  											Message Board
  											<span class="pull-right">
  												<a  href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a>
  												
  											</span>
  									</h3>
  								</div><!-- end panel heading-->
  							  
                                <div class="panel-body nopadding">
  										
								  <div style="height:125px"></div>

  							    </div><!-- end panel body-->
  							  </div><!-- end panel primary-->
                         	</div><!-- end col-->
                         
                            <!--<div class="col-md-6" style=" opacity:1; position:absolute; z-index:999; width:50%">
                            <span class="btn btn-info">Monthly Sales Totals</span>
                            <div id="chart2" style="background:transparent">
  									<svg></svg>
  							</div>
                            </div>-->
                            <div class="col-md-6">
  							  <div class="panel panel-warning">
                                <div class="panel-heading">
                                  <h3 class="panel-title">
                                    <i class="fa fa-pencil-square"></i>
                                      Top Bounty Eligible Customers
                                      <span class="pull-right">
  												<a  href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a>
  												
  									  </span>
                                  </h3>
                                </div><!-- end panel heading-->
                       			<div class="panel-body" style="overflow:scroll; max-height:710px">
                          			<div class="alert alert-info alert-dismissable">These are your top bounty eligible customers.</div>
                                 
                                 
                                 <table  class="table table-bordered table-hover table-striped display" id="example5" >
                                   <thead>
                                    <tr>
                                     <th style="cursor:pointer">Customer Name</th>
                                     <th style="cursor:pointer">CP Code</th>
                                     <th style="cursor:pointer">Total Rev. Lv1 Lv2</th>
                                     <th style="cursor:pointer">Total Rev. Lv3</th>
                                     <th style="cursor:pointer">Amount to Next Bounty</th>
                                     <th style="cursor:pointer">Bounty Level</th>
                                   </tr>
                                 </thead>
                                 <tbody>
                                  @foreach($customerData as $cData)
                                  @if($cData->l1_eligible == 'yes' && $cData->level12_revenue < 20000 && $cData->level1_rev_reqd_at_25gp_por != 5000)
                                  <tr>
                              <td><a href="{{url('/customers/' .$cData->customerid)}}" data-original-title="{{ $cData->customername}}" data-placement="top">{{ $cData->customername}}</a></td>
                              <td>{{ $cData->customerid  }}</td>
                              <td>${{ number_format($cData->level12_revenue, 2,".",",")  }}</td>
                              <td>${{ number_format($cData->level3_revenue, 2,".",",") }}</td>
                              <td>${{ number_format($cData->level1_rev_reqd_at_25gp_por, 2,".",",") }}</td>
                              <td><span class="label @if ($cData->level12_revenue == '0' && $cData->level3_revenue == '0') bg-danger @elseif($cData->level12_revenue == '0' && $cData->level3_revenue != '0') bg-danger @else bg-info @endif">LV 1</span></td>
                              </tr>
                              @endif
                              @endforeach
                              @foreach($customerData as $cData)
                                  @if($cData->l2_eligible == 'yes' && $cData->l1_eligible == 'no'&& $cData->level12_revenue < 20000 && $cData->level12_revenue > 5000 && $cData->level12_revenue < 50000)
                                  <tr>
                              <td><a href="{{url('/customers/' .$cData->customerid)}}" data-original-title="{{ $cData->customername}}" data-placement="top">{{ $cData->customername}}</a></td>
                              <td>{{ $cData->customerid  }}</td>
                              <td>${{ number_format($cData->level12_revenue, 2,".",",")  }}</td>
                              <td>${{ number_format($cData->level3_revenue, 2,".",",") }}</td>
                              <td>${{ number_format($cData->level2_rev_reqd_at_25gp_por, 2,".",",") }}</td>
                              <td><span class="label @if ($cData->level12_revenue == '0' && $cData->level3_revenue == '0') bg-danger @elseif($cData->level12_revenue == '0' && $cData->level3_revenue != '0') bg-danger @else bg-success @endif">LV 2</span></td>
                              </tr>
                              @endif
                              @endforeach
                              @foreach($customerData as $cData)
                                  @if($cData->lthree_eligible == 'yes' && $cData->level3_revenue < 50000 && $cData->level3_revenue > 20000)
                                  <tr>
                              <td><a href="{{url('/customers/' .$cData->customerid)}}" data-original-title="{{ $cData->customername}}" data-placement="top">{{ $cData->customername}}</a></td>
                              <td>{{ $cData->customerid  }}</td>
                              <td>${{ number_format($cData->level12_revenue, 2,".",",")  }}</td>
                              <td>${{ number_format($cData->level3_revenue, 2,".",",") }}</td>
                              <td>${{ number_format($cData->level3_rev_reqd_at_25gp_por, 2,".",",") }}</td>
                              <td><span class="label bg-warning">LV 3</span></td>
                              </tr>
                              @endif
                              @endforeach	
                                   </tbody>
                                   <tfoot>
                                    <tr>
                                     <th>Customer Name</th>
                                     <th>CP Code</th>
                                     <th>Total Rev. Lv1 Lv2</th>
                                     <th>Total Rev. Lv3</th>
                                     <th>Amount to Next Bounty</th>
                                     <th>Level Bounty</th>
                                   </tr>	
                                 </tfoot>
                               </table>
                     		</div> <!-- /panel body -->
  						  </div><!-- end panel -->
                        </div><!-- end col-->
                       </div><!-- end row-->
                       
                        <div class="row" style="background-color:#2F343B; padding-top:20px">
                            <div class="col-md-4">
  								<div class="panel panel-primary">
  									<div class="panel-heading">
  										<h3 class="panel-title">
  											<i class="fa fa-bar-chart-o"></i>
  											Paid Bounties YTD
  											<span class="pull-right">
  												<a  href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a>
  												
  											</span>
  										</h3>
  									</div><!-- end panel heading-->
  									<div class="panel-body nopadding popovers">
  										<div class="list-group list-statistics">
  											<a href="{{url('users/history')}}" class="list-group-item" >
  												Level One
  												<span class="badge bg-success">${{ number_format($dashboard_data->levelone_ytd, 0,".",",") }}</span>
  											</a>
  											<a href="{{url('users/history')}}" class="list-group-item">
  												Level Two
  												<span class="badge bg-success">${{ number_format($dashboard_data->leveltwo_ytd, 0,".",",") }}</span>
  											</a>
  											<a href="{{url('users/history')}}" class="list-group-item">
  												Level Three
  												<span class="badge bg-success">${{ number_format($dashboard_data->levelthree_ytd, 0,".",",") }}</span>
  											</a>
  										</div><!--end list-->
  									</div><!-- end panel body-->
  								</div><!-- end panel primary-->	
  							</div><!-- end col-->
                            
                            <div class="col-md-4">
  								<div class="panel panel-primary">
  									<div class="panel-heading">
  										<h3 class="panel-title">
  											<i class="fa fa-bar-chart-o"></i>
  											Company Performance <a class="panel-title chart" href="{{ url('users/copchart')}}">Chart</a>
  											<span class="pull-right">
  												<a  href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a>
  												
  											</span>
  										</h3>
  									</div><!-- end panel heading-->
  									<div class="panel-body nopadding">
  										<div class="col-md-6">
                                        <script type="text/javascript">
											var chartBounty4 = <?php echo json_encode($chartBounty4); ?>
										</script>
                                        <script type="text/javascript">
											var chartBounty5 = <?php echo json_encode($chartBounty5); ?>
										</script>
										  <div id="bounty4" style="height:160px;"></div>
                                          <center>Current Revenue: ${{ number_format($dashboard_data->zmac_qtr_revenue, 0, ",", ",")}}</center>
                                          <center>Goal: ${{ number_format($dashboard_data->zmac_qtr_goal, 0, ",", ",")}}</center>
                                        </div><!-- end col md 6-->
                                        
                                        <div class="col-md-6">
                                          <div id="bounty5" style="height:160px;"></div>
                                         
                                           
										</div><!-- end col md 6-->
  									</div><!-- end panel body-->
  								</div><!-- end panel-->	
  							</div><!-- end row-->
                            <div class="col-md-4">
  								<div class="panel panel-primary">
  									<div class="panel-heading">
  										<h3 class="panel-title">
  											<i class="fa fa-bar-chart-o"></i>
  											Bank <a class="panel-title chart" href="{{ url('users/copchart')}}">Chart</a>
  											<span class="pull-right">
  												<a  href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a>
  												
  											</span>
  										</h3>
  									</div><!-- end panel heading-->
  									<div class="panel-body">
                                      <a href="{{url('users/history')}}">GP Dollar Level</a> <a href="{{url('users/history')}}" class="btn btn-danger btn-md btn-animate-demo pull-right">${{ number_format($dashboard_data->bank_gp_dollar_level, 0,".",",") }}</a><br />
                                      <br class="clearfix" style="clear:both;"/>
                                      <a href="{{url('users/history')}}">Next Threshold</a> <a href="{{url('users/history')}}" class="btn btn-danger btn-md btn-animate-demo pull-right">${{ number_format($dashboard_data->bank_next_thrshld, 0,".",",") }}</a><br />
                                      <br class="clearfix" style="clear:both;"/>
                                      <a href="{{url('users/history')}}">Amount to Next Threshold</a> <a href="{{url('users/history')}}" class="btn btn-danger btn-md btn-animate-demo pull-right">${{ number_format($dashboard_data->bank_amount_to_next_threshold, 0,".",",") }}</a><br />
  									</div><!-- end panel body-->
  								</div><!-- end paneo primary-->	
  							</div><!-- end col md 4-->
  						</div><!-- end row-->

  						
<script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
{{HTML::script('js/jquery-ui-1.10.3.custom.min.js');}}
{{HTML::script('js/less-1.5.0.min.js');}}
{{HTML::script('js/jquery.ui.touch-punch.min.js');}}
{{HTML::script('js/bootstrap.min.js');}}
{{HTML::script('js/bootstrap-select.js');}}
{{HTML::script('js/bootstrap-switch.js');}}
{{HTML::script('js/jquery.tagsinput.js');}}
{{HTML::script('js/jquery.placeholder.js');}}


<!-- Load JS here for Faster site load =============================-->


<script src="../../js/bootstrap-typeahead.js"></script>
<script src="../../js/application.js"></script>
<script src="../../js/moment.min.js"></script>
<script src="../../js/jquery.dataTables.min.js"></script>
<script src="../../js/jquery.sortable.js"></script>
<script type="text/javascript" src="../../js/jquery.gritter.js"></script>
<script src="../../js/jquery.nicescroll.min.js"></script>
<script src=../../"js/prettify.min.js"></script>
<script src="../../js/jquery.noty.js"></script>
<script src="../../js/bic_calendar.js"></script>
<script src="../../js/jquery.accordion.js"></script>
<script src="../../js/skylo.js"></script>

<script src="../../js/theme-options.js"></script>


<script src="../../js/bootstrap-progressbar.js"></script>
<script src="../../js/bootstrap-progressbar-custom.js"></script>
<script src="../../js/bootstrap-colorpicker.min.js"></script>
<script src="../../js/bootstrap-colorpicker-custom.js"></script>



<script src="../../js/raphael-min.js"></script>
<script src="../../js/morris-0.4.3.min.js"></script>
<script src="../../js/morris-custom.js"></script>
<script src="../../js/morris-custom-2.js"></script>

<script src="../../js/charts/jquery.sparkline.min.js"></script>	



<script src="../../js/bootstrap-tour.js"></script>

<!-- Page script File  =============================-->
<script src="../../js/tooltips-popovers.js"></script>

<!-- AMDCharts ================================================-->
<script src="../../js/amcharts/amcharts.js" type="text/javascript"></script>
<script src="../../js/amcharts/serial.js" type="text/javascript"></script>
<script type="text/javascript">
		var chartData = <?php echo json_encode($chart_data); ?>
</script>

<script src="../../js/amcharts/custom.js" type="text/javascript"></script>


<!-- Core Jquery File  =============================-->
<script src="../../js/core.js"></script>
<script src="../../js/dashboard-custom.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/less.js/1.5.0/less.min.js"></script>
  <script src="../../js/bootstrap-datatables.js"></script>
<script src="../../js/dataTables-custom.js"></script>
@stop