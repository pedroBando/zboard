@extends('layouts.main')

@section('content')
{{ HTML::style('css/404.css')}}
<div class="row">
  		<div class="col-md-6 col-md-offset-3 widget-holder text-white">
  			<i class="fa fa-chain-broken text-warning fa-5"></i> 
  			<h1 class="page-header"> <label class="label">404</label>  Page Not Found</h1>
  			<p>	Sorry, the page you are looking for is not available. Maybe you want to perform a search?</p>
  			

  		</div>
  	</div>

  						
<script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
{{HTML::script('js/jquery-ui-1.10.3.custom.min.js');}}
{{HTML::script('js/less-1.5.0.min.js');}}
{{HTML::script('js/jquery.ui.touch-punch.min.js');}}
{{HTML::script('js/bootstrap.min.js');}}
{{HTML::script('js/bootstrap-select.js');}}
{{HTML::script('js/bootstrap-switch.js');}}
{{HTML::script('js/jquery.tagsinput.js');}}
{{HTML::script('js/jquery.placeholder.js');}}


<!-- Load JS here for Faster site load =============================-->


<script src="../../js/bootstrap-typeahead.js"></script>
<script src="../../js/application.js"></script>
<script src="../../js/moment.min.js"></script>
<script src="../../js/jquery.dataTables.min.js"></script>
<script src="../../js/jquery.sortable.js"></script>
<script type="text/javascript" src="../../js/jquery.gritter.js"></script>
<script src="../../js/jquery.nicescroll.min.js"></script>
<script src=../../"js/prettify.min.js"></script>
<script src="../../js/jquery.noty.js"></script>
<script src="../../js/bic_calendar.js"></script>
<script src="../../js/jquery.accordion.js"></script>
<script src="../../js/skylo.js"></script>

<script src="../../js/theme-options.js"></script>


<script src="../../js/bootstrap-progressbar.js"></script>
<script src="../../js/bootstrap-progressbar-custom.js"></script>
<script src="../../js/bootstrap-colorpicker.min.js"></script>
<script src="../../js/bootstrap-colorpicker-custom.js"></script>



<script src="../../js/raphael-min.js"></script>
<script src="../../js/morris-0.4.3.min.js"></script>
<script src="../../js/morris-custom-3.js"></script>

<script src="../../js/charts/jquery.sparkline.min.js"></script>	



<script src="../../js/bootstrap-tour.js"></script>

<!-- Page script File  =============================-->
<script src="../../js/tooltips-popovers.js"></script>

<!-- AMDCharts ================================================-->
<script src="../../js/amcharts/amcharts.js" type="text/javascript"></script>
<script src="../../js/amcharts/serial.js" type="text/javascript"></script>


<script src="../../js/amcharts/custom-ce.js" type="text/javascript"></script>


<!-- Core Jquery File  =============================-->
<script src="../../js/core.js"></script>
<script src="../../js/dashboard-custom.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/less.js/1.5.0/less.min.js"></script>
  <script src="../../js/bootstrap-datatables.js"></script>
<script src="../../js/dataTables-custom.js"></script>
@stop