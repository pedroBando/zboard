@extends('layouts.main')

@section('content')
<div class="content"> 
	
    <div class="row">
  							<div class="col-mod-12">
								<!-- start breadcrumbs -->
  								<ul class="breadcrumb">
  									<li><a href="{{url('users/dashboard')}}">Dashboard</a></li>
                                    <li>Company Performance Chart</li>
  								</ul><!-- end breadc-->

  								<div class="form-group hiddn-minibar pull-right">
  								
								
								<div class="col-lg-10 col-md-9">
								
						
                                   
								</select>
                               	
								</div><!-- end col-->	
  							</div><!-- end form group search-->

  								<h3 class="page-header"><i class="fa fa fa-dashboard"></i>Company Performance Grid <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>
									 
  								<blockquote class="page-information hidden">
  									<p>
  										Company performance grid. 
  									</p>
  								</blockquote>
  							</div><!-- end col-md-12 -->
  						</div><!-- end row -->
                        
                        <!-- Info Boxes -->
  						<div class="row panel-body">
  							<div class="col-md-4">
  								<div class="info-box  bg-info  text-white">
  									<div class="info-icon bg-info-dark">
  										<i class="fa fa-dollar fa-4x"></i>
  									</div>
  									<div class="info-details">
  										<h4>Quarter Revenue   <span class="pull-right">${{ number_format($dashboard_data->zmac_qtr_revenue) }}
										  </span></h4>
  										<p style="visibility:hidden">Last Month <span class="badge pull-right bg-white text-info"> </span> </p>
  									</div>
  								</div><!-- end info box-->
  							</div><!-- end col-md4-->
  							<div class="col-md-4 ">
  								<div class="info-box  bg-success  text-white">
  									<div class="info-icon bg-success-dark">
  										<i class="fa fa-road fa-4x"></i>
  									</div>
  									<div class="info-details">
  										<h4>Quarter Revenue Goal   <span class="pull-right">${{ number_format($dashboard_data->zmac_qtr_goal) }}</span></h4>
  										<p style="visibility:hidden">Last Month<span class="badge pull-right bg-white text-success"> {{ number_format($dashboard_data->onemonthago_gp_por*100) }}% </span> </p>
  									</div>
  								</div><!-- end info box-->
  							</div><!-- end col-md4-->
  							<div class="col-md-4">
  								<div class="info-box  bg-warning  text-white">
  									<div class="info-icon bg-warning-dark">
  										<i class="fa fa-group fa-4x"></i>
  									</div>
  									<div class="info-details">
  										<h4>Quarter GP %   <span class="pull-right">{{ $dashboard_data->zmac_qtr_gp_por*100 }}%</span></h4>
  										<p style="visibility:hidden"><a href="customers.php">Total Customers</a> <span class="badge pull-right bg-white text-warning"><a href="customers.php">{{ $dashboard_data->total_customers }}</a></span></p>
  									</div>
  								</div><!-- end info box-->
  							</div><!-- end col md 4-->
  						</div><!-- end row-->
    

						<!-- start table row--><div class="row">
                        	<!-- start col--><div class="col-md-6">
                            	<!-- star panel--><div class="panel panel-primary">
                                <div class="panel-heading">
                                  <h3 class="panel-title">
  										<i class="fa fa-bar-chart-o"></i>
  											Company Performance Grid
  											<span class="pull-right">
  												<a  href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a>
  												
  											</span>
  									</h3>
                                </div><!-- end panel heading-->
                            		<!-- start bpanel body--><div class="panel-body" style="overflow:scroll">
                                    @if ($corp->current_qtr_company_revenue < ($corp->current_qtr_company_goal * .9))
                                    	<span class="label bg-danger" style="margin:auto auto">Current Quarter Revenue have not reach 90% of Revenue Goal</span><br/>
                                    @endif
                                    <span class="label bg-warning pull-left">Revenue Goal</span><span class="label bg-primary pull-right">Average Gross Margin</span><br/>
                                      <table class="table table-bordered table-striped display" id="incentives">
                                          <thead>
                                              <td></td>
                                              <td class="bg-primary" style="color:#fff">20.0%</td>
                                              <td class="bg-primary" style="color:#fff">20.4%</td>
                                              <td class="bg-primary" style="color:#fff">20.8%</td>
                                              <td class="bg-primary" style="color:#fff">21.2%</td>
                                              <td class="bg-primary" style="color:#fff">21.6%</td>
                                              <td class="bg-info" style="color:#fff">22.0%</td>
                                              <td class="bg-primary" style="color:#fff">22.6%</td>
                                              <td class="bg-primary" style="color:#fff">23.2%</td>
                                              <td class="bg-primary" style="color:#fff">23.8%</td>
                                              <td class="bg-primary" style="color:#fff">24.4%</td>
                                              <td class="bg-primary" style="color:#fff">25.0%</td>
                                          </thead>
                                          
                                          <tr>
                                              <td class="bg-warning" style="color:#fff">${{ number_format(($corp->current_qtr_company_goal * .9), 0, ".",",")}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 90 && 100*$corp->current_qtr_company_rev_por < 92.5) && (100*$corp->current_qtr_company_gp_por >= 20 && 100*$corp->current_qtr_company_gp_por < 20.4)) class="selected" @endif>${{ number_format($corp->target_incentive * .25)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 90 && 100*$corp->current_qtr_company_rev_por < 92.5) && (100*$corp->current_qtr_company_gp_por >= 20.4 && 100*$corp->current_qtr_company_gp_por < 20.8)) class="selected" @endif>${{ number_format($corp->target_incentive * .31)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 90 && 100*$corp->current_qtr_company_rev_por < 92.5) && (100*$corp->current_qtr_company_gp_por >= 20.8 && 100*$corp->current_qtr_company_gp_por < 21.2)) class="selected" @endif>${{ number_format($corp->target_incentive * .36)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 90 && 100*$corp->current_qtr_company_rev_por < 92.5) && (100*$corp->current_qtr_company_gp_por >= 21.2 && 100*$corp->current_qtr_company_gp_por < 21.6)) class="selected" @endif>${{ number_format($corp->target_incentive * .42)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 90 && 100*$corp->current_qtr_company_rev_por < 92.5) && (100*$corp->current_qtr_company_gp_por >= 21.6 && 100*$corp->current_qtr_company_gp_por < 22.0)) class="selected" @endif>${{ number_format($corp->target_incentive * .47)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 90 && 100*$corp->current_qtr_company_rev_por < 92.5) && (100*$corp->current_qtr_company_gp_por >= 22.0 && 100*$corp->current_qtr_company_gp_por < 22.6)) class="selected" @else class="bg-info"@endif style="color:#fff">${{ number_format($corp->target_incentive * .53)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 90 && 100*$corp->current_qtr_company_rev_por < 92.5) && (100*$corp->current_qtr_company_gp_por >= 22.6 && 100*$corp->current_qtr_company_gp_por < 23.2)) class="selected" @endif >${{ number_format($corp->target_incentive * .60)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 90 && 100*$corp->current_qtr_company_rev_por < 92.5) && (100*$corp->current_qtr_company_gp_por >= 23.2 && 100*$corp->current_qtr_company_gp_por < 23.8)) class="selected" @endif>${{ number_format($corp->target_incentive * .68)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 90 && 100*$corp->current_qtr_company_rev_por < 92.5) && (100*$corp->current_qtr_company_gp_por >= 23.8 && 100*$corp->current_qtr_company_gp_por < 24.4)) class="selected" @endif>${{ number_format($corp->target_incentive * .75)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 90 && 100*$corp->current_qtr_company_rev_por < 92.5) && (100*$corp->current_qtr_company_gp_por >= 24.4 && 100*$corp->current_qtr_company_gp_por < 25.0)) class="selected" @endif>${{ number_format($corp->target_incentive * .83)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 90 && 100*$corp->current_qtr_company_rev_por < 92.5) && (100*$corp->current_qtr_company_gp_por >= 25)) class="selected" @endif>${{ number_format($corp->target_incentive * .90)}}</td>
                                          </tr>
                                          
                                          <tr>
                                              <td class="bg-warning" style="color:#fff">${{ number_format(($corp->current_qtr_company_goal * .925), 0, ".",",")}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 92.5 && 100*$corp->current_qtr_company_rev_por < 95.0) && (100*$corp->current_qtr_company_gp_por >= 20 && 100*$corp->current_qtr_company_gp_por < 20.4)) class="selected" @endif>${{ number_format($corp->target_incentive * .32)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 92.5 && 100*$corp->current_qtr_company_rev_por < 95.0) && (100*$corp->current_qtr_company_gp_por >= 20.4 && 100*$corp->current_qtr_company_gp_por < 20.8)) class="selected" @endif>${{ number_format($corp->target_incentive * .39)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 92.5 && 100*$corp->current_qtr_company_rev_por < 95.0) && (100*$corp->current_qtr_company_gp_por >= 20.8 && 100*$corp->current_qtr_company_gp_por < 21.2)) class="selected" @endif>${{ number_format($corp->target_incentive * .45)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 92.5 && 100*$corp->current_qtr_company_rev_por < 95.0) && (100*$corp->current_qtr_company_gp_por >= 21.2 && 100*$corp->current_qtr_company_gp_por < 21.6)) class="selected" @endif>${{ number_format($corp->target_incentive * .52)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 92.5 && 100*$corp->current_qtr_company_rev_por < 95.0) && (100*$corp->current_qtr_company_gp_por >= 21.6 && 100*$corp->current_qtr_company_gp_por < 22)) class="selected" @endif>${{ number_format($corp->target_incentive * .58)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 92.5 && 100*$corp->current_qtr_company_rev_por < 95.0) && (100*$corp->current_qtr_company_gp_por >= 22 && 100*$corp->current_qtr_company_gp_por < 22.6)) class="selected" @else class="bg-info"@endif style="color:#fff">${{ number_format($corp->target_incentive * .65)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 92.5 && 100*$corp->current_qtr_company_rev_por < 95.0) && (100*$corp->current_qtr_company_gp_por >= 22.6 && 100*$corp->current_qtr_company_gp_por < 23.2)) class="selected" @endif>${{ number_format($corp->target_incentive * .72)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 92.5 && 100*$corp->current_qtr_company_rev_por < 95.0) && (100*$corp->current_qtr_company_gp_por >= 23.2 && 100*$corp->current_qtr_company_gp_por < 23.8)) class="selected" @endif>${{ number_format($corp->target_incentive * .80)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 92.5 && 100*$corp->current_qtr_company_rev_por < 95.0) && (100*$corp->current_qtr_company_gp_por >= 23.8 && 100*$corp->current_qtr_company_gp_por < 24.4)) class="selected" @endif>${{ number_format($corp->target_incentive * .87)}}</td>
                                              <td  @if((100*$corp->current_qtr_company_rev_por >= 92.5 && 100*$corp->current_qtr_company_rev_por < 95.0) && (100*$corp->current_qtr_company_gp_por >= 24.4 && 100*$corp->current_qtr_company_gp_por < 25)) class="selected" @endif>${{ number_format($corp->target_incentive * .94)}}</td>
                                              <td  @if((100*$corp->current_qtr_company_rev_por >= 92.5 && 100*$corp->current_qtr_company_rev_por < 95.0) && (100*$corp->current_qtr_company_gp_por >= 25)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.02)}}</td>
                                          </tr>
                                          
                                          <tr>
                                              <td class="bg-warning" style="color:#fff">${{ number_format(($corp->current_qtr_company_goal * .95), 0, ".",",")}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 95 && 100*$corp->current_qtr_company_rev_por < 97.5) && (100*$corp->current_qtr_company_gp_por >= 20 && 100*$corp->current_qtr_company_gp_por < 20.4)) class="selected" @endif>${{ number_format($corp->target_incentive * .39)}}</td>
                                              <td  @if((100*$corp->current_qtr_company_rev_por >= 95 && 100*$corp->current_qtr_company_rev_por < 97.5) && (100*$corp->current_qtr_company_gp_por >= 20.4 && 100*$corp->current_qtr_company_gp_por < 20.8)) class="selected" @endif>${{ number_format($corp->target_incentive * .46)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 95 && 100*$corp->current_qtr_company_rev_por < 97.5) && (100*$corp->current_qtr_company_gp_por >= 20.8 && 100*$corp->current_qtr_company_gp_por < 21.2)) class="selected" @endif>${{ number_format($corp->target_incentive * .54)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 95 && 100*$corp->current_qtr_company_rev_por < 97.5) && (100*$corp->current_qtr_company_gp_por >= 21.2 && 100*$corp->current_qtr_company_gp_por < 21.6)) class="selected" @endif>${{ number_format($corp->target_incentive * .61)}}</td>
                                              <td  @if((100*$corp->current_qtr_company_rev_por >= 95 && 100*$corp->current_qtr_company_rev_por < 97.5) && (100*$corp->current_qtr_company_gp_por >= 21.6 && 100*$corp->current_qtr_company_gp_por < 22)) class="selected" @endif>${{ number_format($corp->target_incentive * .69)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 95 && 100*$corp->current_qtr_company_rev_por < 97.5) && (100*$corp->current_qtr_company_gp_por >= 22 && 100*$corp->current_qtr_company_gp_por < 22.6)) class="selected" @else  class="bg-info" @endif style="color:#fff">${{ number_format($corp->target_incentive * .76)}}</td>
                                              <td  @if((100*$corp->current_qtr_company_rev_por >= 95 && 100*$corp->current_qtr_company_rev_por < 97.5) && (100*$corp->current_qtr_company_gp_por >= 22.6 && 100*$corp->current_qtr_company_gp_por < 23.2)) class="selected" @endif>${{ number_format($corp->target_incentive * .84)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 95 && 100*$corp->current_qtr_company_rev_por < 97.5) && (100*$corp->current_qtr_company_gp_por >= 23.2 && 100*$corp->current_qtr_company_gp_por < 23.8)) class="selected" @endif>${{ number_format($corp->target_incentive * .91)}}</td>
                                              <td  @if((100*$corp->current_qtr_company_rev_por >= 95 && 100*$corp->current_qtr_company_rev_por < 97.5) && (100*$corp->current_qtr_company_gp_por >= 23.8 && 100*$corp->current_qtr_company_gp_por < 24.4)) class="selected" @endif>${{ number_format($corp->target_incentive * .99)}}</td>
                                              <td  @if((100*$corp->current_qtr_company_rev_por >= 95 && 100*$corp->current_qtr_company_rev_por < 97.5) && (100*$corp->current_qtr_company_gp_por >= 24.4 && 100*$corp->current_qtr_company_gp_por < 25)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.06)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 95 && 100*$corp->current_qtr_company_rev_por < 97.5) && (100*$corp->current_qtr_company_gp_por >= 25)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.14)}}</td>
                                          </tr>
                                          
                                          <tr>
                                              <td class="bg-warning" style="color:#fff">${{ number_format(($corp->current_qtr_company_goal * .975), 0, ".",",")}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 97.5 && 100*$corp->current_qtr_company_rev_por < 100) && (100*$corp->current_qtr_company_gp_por >= 20 && 100*$corp->current_qtr_company_gp_por < 20.4)) class="selected" @endif>${{ number_format($corp->target_incentive * .46)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 97.5 && 100*$corp->current_qtr_company_rev_por < 100) && (100*$corp->current_qtr_company_gp_por >= 20.4 && 100*$corp->current_qtr_company_gp_por < 20.8)) class="selected" @endif>${{ number_format($corp->target_incentive * .54)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 97.5 && 100*$corp->current_qtr_company_rev_por < 100) && (100*$corp->current_qtr_company_gp_por >= 20.8 && 100*$corp->current_qtr_company_gp_por < 21.2)) class="selected" @endif>${{ number_format($corp->target_incentive * .63)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 97.5 && 100*$corp->current_qtr_company_rev_por < 100) && (100*$corp->current_qtr_company_gp_por >= 21.2 && 100*$corp->current_qtr_company_gp_por < 21.6)) class="selected" @endif>${{ number_format($corp->target_incentive * .71)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 97.5 && 100*$corp->current_qtr_company_rev_por < 100) && (100*$corp->current_qtr_company_gp_por >= 21.6 && 100*$corp->current_qtr_company_gp_por < 22)) class="selected" @endif>${{ number_format($corp->target_incentive * .80)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 97.5 && 100*$corp->current_qtr_company_rev_por < 100) && (100*$corp->current_qtr_company_gp_por >= 22.0 && 100*$corp->current_qtr_company_gp_por < 22.6)) class="selected" @else class="bg-info" @endif style="color:#fff">${{ number_format($corp->target_incentive * .88)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 97.5 && 100*$corp->current_qtr_company_rev_por < 100) && (100*$corp->current_qtr_company_gp_por >= 22.6 && 100*$corp->current_qtr_company_gp_por < 23.2)) class="selected" @endif>${{ number_format($corp->target_incentive * .96)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 97.5 && 100*$corp->current_qtr_company_rev_por < 100) && (100*$corp->current_qtr_company_gp_por >= 23.2 && 100*$corp->current_qtr_company_gp_por < 23.8)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.03)}}</td>
                                              <td  @if((100*$corp->current_qtr_company_rev_por >= 97.5 && 100*$corp->current_qtr_company_rev_por < 100) && (100*$corp->current_qtr_company_gp_por >= 23.8 && 100*$corp->current_qtr_company_gp_por < 24.4)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.11)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 97.5 && 100*$corp->current_qtr_company_rev_por < 100) && (100*$corp->current_qtr_company_gp_por >= 24.4 && 100*$corp->current_qtr_company_gp_por < 25)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.18)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 97.5 && 100*$corp->current_qtr_company_rev_por < 100) && (100*$corp->current_qtr_company_gp_por >= 25)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.25)}}</td>
                                          </tr>
                                          
                                          <tr>
                                              <td class="bg-info" style="color:#fff">${{ number_format(($corp->current_qtr_company_goal * 1), 0, ".",",")}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 100 && 100*$corp->current_qtr_company_rev_por < 105) && (100*$corp->current_qtr_company_gp_por >= 20.0 && 100*$corp->current_qtr_company_gp_por < 20.4)) class="selected" @else class="bg-info" @endif style="color:#fff">${{ number_format($corp->target_incentive * .53)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 100 && 100*$corp->current_qtr_company_rev_por < 105) && (100*$corp->current_qtr_company_gp_por >= 20.4 && 100*$corp->current_qtr_company_gp_por < 20.8)) class="selected" @else class="bg-info" @endif style="color:#fff">${{ number_format($corp->target_incentive * .62)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 100 && 100*$corp->current_qtr_company_rev_por < 105) && (100*$corp->current_qtr_company_gp_por >= 20.8 && 100*$corp->current_qtr_company_gp_por < 21.2)) class="selected" @else class="bg-info" @endif style="color:#fff">${{ number_format($corp->target_incentive * .72)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 100 && 100*$corp->current_qtr_company_rev_por < 105) && (100*$corp->current_qtr_company_gp_por >= 21.2 && 100*$corp->current_qtr_company_gp_por < 21.6)) class="selected" @else class="bg-info" @endif style="color:#fff">${{ number_format($corp->target_incentive * .81)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 100 && 100*$corp->current_qtr_company_rev_por < 105) && (100*$corp->current_qtr_company_gp_por >= 21.6 && 100*$corp->current_qtr_company_gp_por < 22)) class="selected" @else class="bg-info" @endif style="color:#fff">${{ number_format($corp->target_incentive * .91)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 100 && 100*$corp->current_qtr_company_rev_por < 105) && (100*$corp->current_qtr_company_gp_por >= 22 && 100*$corp->current_qtr_company_gp_por < 22.6)) class="selected" @else class="bg-info" @endif style="color:#fff">${{ number_format($corp->target_incentive * 1.00)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 100 && 100*$corp->current_qtr_company_rev_por < 105) && (100*$corp->current_qtr_company_gp_por >= 22.6 && 100*$corp->current_qtr_company_gp_por < 23.2)) class="selected" @else class="bg-info" @endif style="color:#fff">${{ number_format($corp->target_incentive * 1.07)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 100 && 100*$corp->current_qtr_company_rev_por < 105) && (100*$corp->current_qtr_company_gp_por >= 23.2 && 100*$corp->current_qtr_company_gp_por < 23.8)) class="selected" @else class="bg-info" @endif style="color:#fff">${{ number_format($corp->target_incentive * 1.15)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 100 && 100*$corp->current_qtr_company_rev_por < 105) && (100*$corp->current_qtr_company_gp_por >= 23.8 && 100*$corp->current_qtr_company_gp_por < 24.4)) class="selected" @else class="bg-info" @endif style="color:#fff">${{ number_format($corp->target_incentive * 1.22)}}%</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 100 && 100*$corp->current_qtr_company_rev_por < 105) && (100*$corp->current_qtr_company_gp_por >= 24.4 && 100*$corp->current_qtr_company_gp_por < 25)) class="selected" @else class="bg-info" @endif style="color:#fff">${{ number_format($corp->target_incentive * 1.30)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 100 && 100*$corp->current_qtr_company_rev_por < 105) && (100*$corp->current_qtr_company_gp_por >= 25)) class="selected" @else class="bg-info" @endif style="color:#fff">${{ number_format($corp->target_incentive * 1.3)}}</td>
                                          </tr>
                                          
                                          <tr>
                                              <td class="bg-warning" style="color:#fff">${{ number_format(($corp->current_qtr_company_goal * 1.5), 0, ".",",")}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 105 && 100*$corp->current_qtr_company_rev_por < 110) && (100*$corp->current_qtr_company_gp_por >= 20 && 100*$corp->current_qtr_company_gp_por < 20.4)) class="selected" @endif>${{ number_format($corp->target_incentive * .62)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 105 && 100*$corp->current_qtr_company_rev_por < 110) && (100*$corp->current_qtr_company_gp_por >= 20.4 && 100*$corp->current_qtr_company_gp_por < 20.8)) class="selected" @endif>${{ number_format($corp->target_incentive * .72)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 105 && 100*$corp->current_qtr_company_rev_por < 110) && (100*$corp->current_qtr_company_gp_por >= 20.8 && 100*$corp->current_qtr_company_gp_por < 21.2)) class="selected" @endif>${{ number_format($corp->target_incentive * .81)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 105 && 100*$corp->current_qtr_company_rev_por < 110) && (100*$corp->current_qtr_company_gp_por >= 21.2 && 100*$corp->current_qtr_company_gp_por < 21.6)) class="selected" @endif>${{ number_format($corp->target_incentive * .90)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 105 && 100*$corp->current_qtr_company_rev_por < 110) && (100*$corp->current_qtr_company_gp_por >= 21.6 && 100*$corp->current_qtr_company_gp_por < 22)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.00)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 105 && 100*$corp->current_qtr_company_rev_por < 110) && (100*$corp->current_qtr_company_gp_por >= 22 && 100*$corp->current_qtr_company_gp_por < 22.6)) class="selected" @else class="bg-info" @endif style="color:#fff">${{ number_format($corp->target_incentive * 1.09)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 105 && 100*$corp->current_qtr_company_rev_por < 110) && (100*$corp->current_qtr_company_gp_por >= 22.6 && 100*$corp->current_qtr_company_gp_por < 23.2)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.18)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 105 && 100*$corp->current_qtr_company_rev_por < 110) && (100*$corp->current_qtr_company_gp_por >= 23.2 && 100*$corp->current_qtr_company_gp_por < 23.8)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.27)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 105 && 100*$corp->current_qtr_company_rev_por < 110) && (100*$corp->current_qtr_company_gp_por >= 23.8 && 100*$corp->current_qtr_company_gp_por < 24.4)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.35)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 105 && 100*$corp->current_qtr_company_rev_por < 110) && (100*$corp->current_qtr_company_gp_por >= 24.4 && 100*$corp->current_qtr_company_gp_por < 25)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.44)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 105 && 100*$corp->current_qtr_company_rev_por < 110) && (100*$corp->current_qtr_company_gp_por >= 25)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.53)}}</td>
                                          </tr>
                                          
                                          <tr>
                                              <td class="bg-warning" style="color:#fff">${{ number_format(($corp->current_qtr_company_goal * 1.10), 0, ".",",")}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 110 && 100*$corp->current_qtr_company_rev_por < 115) && (100*$corp->current_qtr_company_gp_por >= 20 && 100*$corp->current_qtr_company_gp_por < 20.4)) class="selected" @endif>${{ number_format($corp->target_incentive * .71)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 110 && 100*$corp->current_qtr_company_rev_por < 115) && (100*$corp->current_qtr_company_gp_por >= 20.4 && 100*$corp->current_qtr_company_gp_por < 20.8)) class="selected" @endif>${{ number_format($corp->target_incentive * .81)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 110 && 100*$corp->current_qtr_company_rev_por < 115) && (100*$corp->current_qtr_company_gp_por >= 20.8 && 100*$corp->current_qtr_company_gp_por < 21.2)) class="selected" @endif>${{ number_format($corp->target_incentive * .90)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 110 && 100*$corp->current_qtr_company_rev_por < 115) && (100*$corp->current_qtr_company_gp_por >= 21.2 && 100*$corp->current_qtr_company_gp_por < 21.6)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.00)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 110 && 100*$corp->current_qtr_company_rev_por < 115) && (100*$corp->current_qtr_company_gp_por >= 21.6 && 100*$corp->current_qtr_company_gp_por < 22)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.09)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 110 && 100*$corp->current_qtr_company_rev_por < 115) && (100*$corp->current_qtr_company_gp_por >= 22 && 100*$corp->current_qtr_company_gp_por < 22.6)) class="selected" @else class="bg-info" @endif style="color:#fff">${{ number_format($corp->target_incentive * 1.19)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 110 && 100*$corp->current_qtr_company_rev_por < 115) && (100*$corp->current_qtr_company_gp_por >= 22.6 && 100*$corp->current_qtr_company_gp_por < 23.2)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.29)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 110 && 100*$corp->current_qtr_company_rev_por < 115) && (100*$corp->current_qtr_company_gp_por >= 23.2 && 100*$corp->current_qtr_company_gp_por < 23.8)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.39)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 110 && 100*$corp->current_qtr_company_rev_por < 115) && (100*$corp->current_qtr_company_gp_por >= 28.8 && 100*$corp->current_qtr_company_gp_por < 24.4)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.49)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 110 && 100*$corp->current_qtr_company_rev_por < 115) && (100*$corp->current_qtr_company_gp_por >= 24.4 && 100*$corp->current_qtr_company_gp_por < 25)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.59)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 110 && 100*$corp->current_qtr_company_rev_por < 115) && (100*$corp->current_qtr_company_gp_por >= 25)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.69)}}</td>
                                          </tr>
                                          
                                          <tr>
                                              <td class="bg-warning" style="color:#fff">${{ number_format(($corp->current_qtr_company_goal * 1.15), 0, ".",",")}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 115 && 100*$corp->current_qtr_company_rev_por < 120) && (100*$corp->current_qtr_company_gp_por >= 20 && 100*$corp->current_qtr_company_gp_por < 20.4)) class="selected" @endif>${{ number_format($corp->target_incentive * .81)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 115 && 100*$corp->current_qtr_company_rev_por < 120) && (100*$corp->current_qtr_company_gp_por >= 20.4 && 100*$corp->current_qtr_company_gp_por < 20.8)) class="selected" @endif>${{ number_format($corp->target_incentive * .90)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 115 && 100*$corp->current_qtr_company_rev_por < 120) && (100*$corp->current_qtr_company_gp_por >= 20.8 && 100*$corp->current_qtr_company_gp_por < 21.2)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.00)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 115 && 100*$corp->current_qtr_company_rev_por < 120) && (100*$corp->current_qtr_company_gp_por >= 21.2 && 100*$corp->current_qtr_company_gp_por < 21.6)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.09)}}</td>
                                              <td  @if((100*$corp->current_qtr_company_rev_por >= 115 && 100*$corp->current_qtr_company_rev_por < 120) && (100*$corp->current_qtr_company_gp_por >= 21.6 && 100*$corp->current_qtr_company_gp_por < 22)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.18)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 115 && 100*$corp->current_qtr_company_rev_por < 120) && (100*$corp->current_qtr_company_gp_por >= 22 && 100*$corp->current_qtr_company_gp_por < 22.6)) class="selected" @else class="bg-info" @endif style="color:#fff">${{ number_format($corp->target_incentive * 1.28)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 115 && 100*$corp->current_qtr_company_rev_por < 120) && (100*$corp->current_qtr_company_gp_por >= 22.6 && 100*$corp->current_qtr_company_gp_por < 23.2)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.39)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 115 && 100*$corp->current_qtr_company_rev_por < 120) && (100*$corp->current_qtr_company_gp_por >= 23.2 && 100*$corp->current_qtr_company_gp_por < 23.8)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.50)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 115 && 100*$corp->current_qtr_company_rev_por < 120) && (100*$corp->current_qtr_company_gp_por >= 23.8 && 100*$corp->current_qtr_company_gp_por < 24.4)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.62)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 115 && 100*$corp->current_qtr_company_rev_por < 120) && (100*$corp->current_qtr_company_gp_por >= 24.4 && 100*$corp->current_qtr_company_gp_por < 25)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.73)}}</td>
                                              <td @if((100*$corp->current_qtr_company_rev_por >= 115 && 100*$corp->current_qtr_company_rev_por < 120) && (100*$corp->current_qtr_company_gp_por >= 25)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.84)}}</td>
                                           </tr>
                                           
                                           <tr>
                                           <td class="bg-warning" style="color:#fff">${{ number_format(($corp->current_qtr_company_goal * 1.20), 0, ".",",")}}</td>
                                           <td @if((100*$corp->current_qtr_company_rev_por >= 120) && (100*$corp->current_qtr_company_gp_por >= 20 && 100*$corp->current_qtr_company_gp_por < 20.4)) class="selected" @endif>${{ number_format($corp->target_incentive * .90)}}</td>
                                           <td @if((100*$corp->current_qtr_company_rev_por >= 120 ) && (100*$corp->current_qtr_company_gp_por >= 20.4 && 100*$corp->current_qtr_company_gp_por < 20.8)) class="selected" @endif>${{ number_format($corp->target_incentive * .99)}}</td>
                                           <td @if((100*$corp->current_qtr_company_rev_por >= 120) && (100*$corp->current_qtr_company_gp_por >= 20.8 && 100*$corp->current_qtr_company_gp_por < 21.2)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.09)}}</td>
                                           <td @if((100*$corp->current_qtr_company_rev_por >= 120) && (100*$corp->current_qtr_company_gp_por >= 21.2 && 100*$corp->current_qtr_company_gp_por < 21.6)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.18)}}</td>
                                           <td @if((100*$corp->current_qtr_company_rev_por >= 120) && (100*$corp->current_qtr_company_gp_por >= 21.6 && 100*$corp->current_qtr_company_gp_por < 22)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.28)}}</td>
                                           <td @if((100*$corp->current_qtr_company_rev_por >= 120) && (100*$corp->current_qtr_company_gp_por >= 22 && 100*$corp->current_qtr_company_gp_por < 22.6)) class="selected" @else class="bg-info" @endif style="color:#fff">${{ number_format($corp->target_incentive * 1.37)}}</td>
                                           <td @if((100*$corp->current_qtr_company_rev_por >= 120) && (100*$corp->current_qtr_company_gp_por >= 22.6 && 100*$corp->current_qtr_company_gp_por < 23.2)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.50)}}</td>
                                           <td @if((100*$corp->current_qtr_company_rev_por >= 120) && (100*$corp->current_qtr_company_gp_por >= 23.2 && 100*$corp->current_qtr_company_gp_por < 23.8)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.62)}}</td>
                                           <td @if((100*$corp->current_qtr_company_rev_por >= 120) && (100*$corp->current_qtr_company_gp_por >= 23.8 && 100*$corp->current_qtr_company_gp_por < 24.4)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.75)}}</td>
                                           <td @if((100*$corp->current_qtr_company_rev_por >= 120) && (100*$corp->current_qtr_company_gp_por >= 24.4 && 100*$corp->current_qtr_company_gp_por < 25)) class="selected" @endif>${{ number_format($corp->target_incentive * 1.87)}}</td>
                                           <td @if((100*$corp->current_qtr_company_rev_por >= 120) && (100*$corp->current_qtr_company_gp_por >= 25)) class="selected" @endif>${{ number_format($corp->target_incentive * 2.00)}}</td>
                                           </tr>
                                      </table>
                                      </div><!-- end panel body-->
                                      </div> <!-- end panel-->
					</div><!-- end col-->
                    
                    <!-- start col--><div class="col-md-6">
                            	<!-- star panel--><div class="panel panel-primary">
                                <div class="panel-heading">
                                  <h3 class="panel-title">
  										<i class="fa fa-bar-chart-o"></i>
  											Bank Grid 
  											<span class="pull-right">
                                          
  												<a  href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a>
  												
  											</span>
  									</h3>
                                </div><!-- end panel heading-->
                            		<!-- start bpanel body--><div class="panel-body" style="max-height:443px; overflow:scroll">
                                <h3 style="font-size:16px" class="label text-primary pull-left">Bank Dollar Level ${{ number_format($dashboard_data->bank_gp_dollar_level, 0,".",",") }}</h3>
                                <h3 style="font-size:16px" class="label text-primary pull-right">Amount Need to Next Threshold ${{ number_format($dashboard_data->bank_amount_to_next_threshold, 0,".",",") }}</h3>
                                
                                <br style="clear:both;margin-bottom:20px;" />
                                <span class="label selected-bank pull-left">Selected</span>
                                 
                                <hr/>
                                    @if(Auth::user()->type == "se") 
                                   
                      				
                                      <table class="table">
                                          <tr style="background-color:#F9A94A !important; color:#fff !important"><th style="background-color:#F9A94A !important; color:#fff !important">GP$</th><th style="background-color:#F9A94A !important; color:#fff !important">Payout</th><th style="background-color:#F9A94A !important; color:#fff !important">Cumulative</th></tr>
                                           <tr @if($dashboard_data->bank_gp_dollar_level < 137500) class="selected-bank" @endif><td>$0 </td><td>$0 </td><td>$0 </td></tr>
                                           <tr @if($dashboard_data->bank_gp_dollar_level > 137500 ) class="selected-bank" @endif><td>$137,500 </td><td>$2,100 </td><td>$2,100 </td></tr>
                                           <tr @if($dashboard_data->bank_gp_dollar_level > 275000) class="selected-bank" @endif><td>$275,000 </td><td>$2,100 </td><td>$4,200 </td></tr>
                                           <tr @if($dashboard_data->bank_gp_dollar_level > 412500) class="selected-bank" @endif><td>$412,500 </td><td>$2,100 </td><td>$6,300 </td></tr>
                                           <tr @if($dashboard_data->bank_gp_dollar_level > 550000) class="selected-bank" @endif><td>$550,000 </td><td>$2,100 </td><td>$8,400 </td></tr>
                                           <tr @if($dashboard_data->bank_gp_dollar_level > 694000) class="selected-bank" @endif><td>$694,000 </td><td>$2,700 </td><td>$11,100 </td></tr>
                                           <tr @if($dashboard_data->bank_gp_dollar_level > 838000) class="selected-bank" @endif><td>$838,000 </td><td>$2,700 </td><td>$13,800 </td></tr>
                                           <tr @if($dashboard_data->bank_gp_dollar_level > 982000) class="selected-bank" @endif><td>$982,000 </td><td>$2,700 </td><td>$16,500 </td></tr>
                                           <tr @if($dashboard_data->bank_gp_dollar_level > 1126000) class="selected-bank" @endif><td>$1,126,000 </td><td>$2,700 </td><td>$19,200 </td></tr>
                                           <tr @if($dashboard_data->bank_gp_dollar_level > 1295000) class="selected-bank" @endif><td>$1,295,000 </td><td>$3,500 </td><td>$22,700 </td></tr>
                                           <tr @if($dashboard_data->bank_gp_dollar_level > 1464000) class="selected-bank" @endif><td>$1,464,000 </td><td>$3,500 </td><td>$26,200 </td></tr>
                                           <tr @if($dashboard_data->bank_gp_dollar_level > 137500) class="selected-bank" @endif><td>$1,633,000 </td><td>$3,500 </td><td>$29,700 </td></tr>
                                           <tr @if($dashboard_data->bank_gp_dollar_level > 1802000) class="selected-bank" @endif><td>$1,802,000 </td><td>$3,500 </td><td>$33,200 </td></tr>
                                           <tr @if($dashboard_data->bank_gp_dollar_level > 2005000) class="selected-bank" @endif><td>$2,005,000 </td><td>$4,650 </td><td>$37,850 </td></tr>
                                           <tr @if($dashboard_data->bank_gp_dollar_level > 2208000) class="selected-bank" @endif><td>$2,208,000 </td><td>$4,650 </td><td>$42,500 </td></tr>
                                           <tr @if($dashboard_data->bank_gp_dollar_level > 2411000) class="selected-bank" @endif><td>$2,411,000 </td><td>$4,650 </td><td>$47,150 </td></tr>
                                           <tr @if($dashboard_data->bank_gp_dollar_level > 2614000) class="selected-bank" @endif><td>$2,614,000 </td><td>$4,650 </td><td>$51,800 </td></tr>
                                           <tr @if($dashboard_data->bank_gp_dollar_level > 2863000) class="selected-bank" @endif><td>$2,863,000 </td><td>$6,300 </td><td>$58,100 </td></tr>
                                           <tr @if($dashboard_data->bank_gp_dollar_level > 3112000) class="selected-bank"" @endif><td>$3,112,000 </td><td>$6,300 </td><td>$64,400 </td></tr>
                                           <tr @if($dashboard_data->bank_gp_dollar_level > 3361000) class="selected-bank" @endif><td>$3,361,000 </td><td>$6,300 </td><td>$70,700 </td></tr>
                                           <tr @if($dashboard_data->bank_gp_dollar_level > 3610000) class="selected-bank" @endif><td>$3,610,000 </td><td>$6,300 </td><td>$77,000 </td></tr>
                                          </table>


                                      
                                      @elseif(Auth::user()->type == "ae")
                                      
                                      <table class="table">
                                          <tr style="background-color:#F9A94A !important; color:#fff !important"><th style="background-color:#F9A94A !important; color:#fff !important">GP$</th><th style="background-color:#F9A94A !important; color:#fff !important">Payout</th><th style="background-color:#F9A94A !important; color:#fff !important">Cumulative</th></tr>
                                           <tr @if($dashboard_data->bank_gp_dollar_level > 0) class="selected-bank" @endif><td>$0 </td><td>$0 </td><td>$0 </td></tr>
                                           <tr @if($dashboard_data->bank_gp_dollar_level > 10000) class="selected-bank" @endif><td>$100,000 </td><td>$750 </td><td>$750 </td></tr>
                                           <tr @if($dashboard_data->bank_gp_dollar_level > 200000) class="selected-bank" @endif><td>$200,000 </td><td>$750 </td><td>$1,500 </td></tr>
                                           <tr @if($dashboard_data->bank_gp_dollar_level > 300000) class="selected-bank" @endif><td>$300,000 </td><td>$750 </td><td>$2,250 </td></tr>
                                           <tr @if($dashboard_data->bank_gp_dollar_level > 400000) class="selected-bank" @endif><td>$400,000 </td><td>$750 </td><td>$3,000 </td></tr>
                                           <tr @if($dashboard_data->bank_gp_dollar_level > 510000) class="selected-bank" @endif><td>$510,000 </td><td>$1,000 </td><td>$4,000 </td></tr>
                                           <tr @if($dashboard_data->bank_gp_dollar_level > 620000) class="selected-bank" @endif><td>$620,000 </td><td>$1,000 </td><td>$5,000 </td></tr>
                                           <tr @if($dashboard_data->bank_gp_dollar_level > 730000) class="selected-bank" @endif><td>$730,000 </td><td>$1,000 </td><td>$6,000 </td></tr>
                                           <tr @if($dashboard_data->bank_gp_dollar_level > 840000) class="selected-bank" @endif><td>$840,000 </td><td>$1,000 </td><td>$7,000 </td></tr>
                                           <tr @if($dashboard_data->bank_gp_dollar_level > 969000) class="selected-bank" @endif><td>$969,000 </td><td>$1,300 </td><td>$8,300 </td></tr>
                                           <tr @if($dashboard_data->bank_gp_dollar_level > 1098000) class="selected-bank" @endif><td>$1,098,000 </td><td>$1,300 </td><td>$9,600 </td></tr>
                                           <tr @if($dashboard_data->bank_gp_dollar_level > 1227000) class="selected-bank" @endif><td>$1,227,000 </td><td>$1,300 </td><td>$10,900 </td></tr>
                                           <tr @if($dashboard_data->bank_gp_dollar_level > 1356000) class="selected-bank" @endif><td>$1,356,000 </td><td>$1,300 </td><td>$12,200 </td></tr>
                                           <tr @if($dashboard_data->bank_gp_dollar_level > 1511000) class="selected-bank" @endif><td>$1,511,000 </td><td>$1,700 </td><td>$13,900 </td></tr>
                                           <tr @if($dashboard_data->bank_gp_dollar_level > 1666000) class="selected-bank" @endif><td>$1,666,000 </td><td>$1,700 </td><td>$15,600 </td></tr>
                                           <tr @if($dashboard_data->bank_gp_dollar_level > 1821000) class="selected-bank" @endif><td>$1,821,000 </td><td>$1,700 </td><td>$17,300 </td></tr>
                                           <tr @if($dashboard_data->bank_gp_dollar_level > 1976000) class="selected-bank" @endif><td>$1,976,000 </td><td>$1,700 </td><td>$19,000 </td></tr>
                                           <tr @if($dashboard_data->bank_gp_dollar_level > 2166000) class="selected-bank" @endif><td>$2,166,000 </td><td>$2,300 </td><td>$21,300 </td></tr>
                                           <tr @if($dashboard_data->bank_gp_dollar_level > 2356000) class="selected-bank" @endif><td>$2,356,000 </td><td>$2,300 </td><td>$23,600 </td></tr>
                                           <tr @if($dashboard_data->bank_gp_dollar_level > 2546000) class="selected-bank" @endif><td>$2,546,000 </td><td>$2,300 </td><td>$25,900 </td></tr>
                                           <tr @if($dashboard_data->bank_gp_dollar_level > 2736000) class="selected-bank" @endif><td>$2,736,000 </td><td>$2,300 </td><td>$28,200 </td></tr>
                                          </table>

                                      
                                      @elseif(Auth::user()->type == "ce")
                                      <table class="table table-bordered table-striped display">
                                      
                                      <tr style="background-color:#F9A94A !important; color:#fff !important"><td style="background-color:#F9A94A !important; color:#fff !important">GP$</td><td style="background-color:#F9A94A !important; color:#fff !important">Payout</td><td style="background-color:#F9A94A !important; color:#fff !important">Cumulative</td></tr>
                                      <tr @if($dashboard_data->bank_gp_dollar_level > 0) class="selected-bank" @endif><td>$0 </td><td>$0 </td><td>$0 </td></tr>
                                      <tr @if($dashboard_data->bank_gp_dollar_level > 108000) class="selected-bank" @endif><td>$108,000 </td><td>$900 </td><td>$900 </td></tr>
                                      <tr @if($dashboard_data->bank_gp_dollar_level > 216000) class="selected-bank" @endif><td>$216,000 </td><td>$900 </td><td>$1,800 </td></tr>
                                      <tr @if($dashboard_data->bank_gp_dollar_level > 324000) class="selected-bank" @endif><td>$324,000 </td><td>$900 </td><td>$2,700 </td></tr>
                                      <tr @if($dashboard_data->bank_gp_dollar_level > 432000) class="selected-bank" @endif><td>$432,000 </td><td>$900 </td><td>$3,600 </td></tr>
                                      <tr @if($dashboard_data->bank_gp_dollar_level > 540000) class="selected-bank" @endif><td>$540,000 </td><td>$900 </td><td>$4,500 </td></tr>
                                      <tr @if($dashboard_data->bank_gp_dollar_level > 664000) class="selected-bank" @endif><td>$664,000 </td><td>$1,100 </td><td>$5,600 </td></tr>
                                      <tr @if($dashboard_data->bank_gp_dollar_level > 788000) class="selected-bank" @endif><td>$788,000 </td><td>$1,100 </td><td>$6,700 </td></tr>
                                      <tr @if($dashboard_data->bank_gp_dollar_level > 912000) class="selected-bank" @endif><td>$912,000 </td><td>$1,100 </td><td>$7,800 </td></tr>
                                      <tr @if($dashboard_data->bank_gp_dollar_level > 1036000) class="selected-bank" @endif><td>$1,036,000 </td><td>$1,100 </td><td>$8,900 </td></tr>
                                      <tr @if($dashboard_data->bank_gp_dollar_level > 1182000) class="selected-bank" @endif><td>$1,182,000 </td><td>$1,450 </td><td>$10,350 </td></tr>
                                      <tr @if($dashboard_data->bank_gp_dollar_level > 1328000) class="selected-bank" @endif><td>$1,328,000 </td><td>$1,450 </td><td>$11,800 </td></tr>
                                      <tr @if($dashboard_data->bank_gp_dollar_level > 1474000) class="selected-bank" @endif><td>$1,474,000 </td><td>$1,450 </td><td>$13,250 </td></tr>
                                      <tr @if($dashboard_data->bank_gp_dollar_level > 1620000) class="selected-bank" @endif><td>$1,620,000 </td><td>$1,450 </td><td>$14,700 </td></tr>
                                      <tr @if($dashboard_data->bank_gp_dollar_level > 1795000) class="selected-bank" @endif><td>$1,795,000 </td><td>$1,900 </td><td>$16,600 </td></tr>
                                      <tr @if($dashboard_data->bank_gp_dollar_level > 1970000) class="selected-bank" @endif><td>$1,970,000 </td><td>$1,900 </td><td>$18,500 </td></tr>
                                      <tr @if($dashboard_data->bank_gp_dollar_level > 2145000) class="selected-bank" @endif><td>$2,145,000 </td><td>$1,900 </td><td>$20,400 </td></tr>
                                      <tr @if($dashboard_data->bank_gp_dollar_level > 2320000) class="selected-bank" @endif><td>$2,320,000 </td><td>$1,900 </td><td>$22,300 </td></tr>
                                      <tr @if($dashboard_data->bank_gp_dollar_level > 2534000) class="selected-bank" @endif><td>$2,534,000 </td><td>$2,550 </td><td>$24,850 </td></tr>
                                      <tr @if($dashboard_data->bank_gp_dollar_level > 2748000) class="selected-bank" @endif><td>$2,748,000 </td><td>$2,550 </td><td>$27,400 </td></tr>
                                      <tr @if($dashboard_data->bank_gp_dollar_level > 2962000) class="selected-bank" @endif><td>$2,962,000 </td><td>$2,550 </td><td>$29,950 </td></tr>
                                      </table>

                                      @else
                                      not created yet 
                                      
                                      @endif
                                      </div><!-- end panel body-->
                                      </div> <!-- end panel-->
					</div><!-- end col-->
                    </div><!-- end row table-->

  						
                        
                      
                      
  						
<script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
{{HTML::script('js/jquery-ui-1.10.3.custom.min.js');}}
{{HTML::script('js/less-1.5.0.min.js');}}
{{HTML::script('js/jquery.ui.touch-punch.min.js');}}
{{HTML::script('js/bootstrap.min.js');}}
{{HTML::script('js/bootstrap-select.js');}}
{{HTML::script('js/bootstrap-switch.js');}}
{{HTML::script('js/jquery.tagsinput.js');}}
{{HTML::script('js/jquery.placeholder.js');}}


<!-- Load JS here for Faster site load =============================-->


<script src="../../js/bootstrap-typeahead.js"></script>
<script src="../../js/application.js"></script>
<script src="../../js/moment.min.js"></script>
<script src="../../js/jquery.dataTables.min.js"></script>
<script src="../../js/jquery.sortable.js"></script>
<script type="text/javascript" src="../../js/jquery.gritter.js"></script>
<script src="../../js/jquery.nicescroll.min.js"></script>
<script src=../../"js/prettify.min.js"></script>
<script src="../../js/jquery.noty.js"></script>
<script src="../../js/bic_calendar.js"></script>
<script src="../../js/jquery.accordion.js"></script>
<script src="../../js/skylo.js"></script>

<script src="../../js/theme-options.js"></script>


<script src="../../js/bootstrap-progressbar.js"></script>
<script src="../../js/bootstrap-progressbar-custom.js"></script>
<script src="../../js/bootstrap-colorpicker.min.js"></script>
<script src="../../js/bootstrap-colorpicker-custom.js"></script>



<script src="../../js/raphael-min.js"></script>
<script src="../../js/morris-0.4.3.min.js"></script>
<script src="../../js/morris-custom-3.js"></script>

<script src="../../js/charts/jquery.sparkline.min.js"></script>	



<script src="../../js/bootstrap-tour.js"></script>

<!-- Page script File  =============================-->
<script src="../../js/tooltips-popovers.js"></script>

<!-- Core Jquery File  =============================-->
<script src="../../js/core.js"></script>
<script src="../../js/dashboard-custom.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/less.js/1.5.0/less.min.js"></script>
  <script src="../../js/bootstrap-datatables.js"></script>
<script src="../../js/dataTables-custom.js"></script>
@stop