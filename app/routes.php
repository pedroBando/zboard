<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', array('as' => 'home','uses' => 'HomeController@home'));

//Login Routes
Route::get('/users/login', "LoginController@login");
Route::get('/users/register', "LoginController@register");
Route::post('/users/create', "LoginController@postCreate");
Route::post('/users/signin', "LoginController@postSignin");




//Auth Filter
Route::filter('checklogin', function() {
	if (!Auth::user() || !Auth::user()->id){
		return Redirect::to('/users/login');
	}
});

//Admin Filter
Route::filter('checkadmin', function() {
	if(Auth::user()->type == 'mg'){
		return Redirect::to('/admin/dash');
	}
});

//Protected Routes
Route::group(array('before' => 'checklogin|checkadmin'), function() {
	Route::controller('users', 'UsersController');
	Route::get('/users/mycustomers' , 'UsersController@getAecustomerslist');
	Route::post('post_files','UsersControllers@post_files');
	Route::get('/customers/{id}', array('as' => 'customer', 'uses' => 'UsersController@getCustomer'));
	Route::get('/customers/{lhistory}/{id}', "UsersController@getPayhistory");
});

Route::group(array('before' => 'checklogin'), function() {	
	
	Route::get('logout', "LoginController@getLogout");
	//Admin Controller
	Route::controller('admins', 'AdminsController');
	Route::get('/admin/dash', 'AdminsController@getAdashboard');
	Route::get('/admin/{employee_id}/{level}', array('as' => 'adminEmployees', 'uses' => 'AdminsController@getEmployeeinfo'));
	Route::get('/admin/odetail/{user_id}', array('as' => 'adminEmployeesDet', 'uses' => 'AdminsController@getAdminseoverview'));
	Route::get('/admin/adminseoverview/{userr_id}', array('as' => 'adminSeHistory', 'uses' => 'AdminsController@getAdminsehistory'));
	Route::get('/customers/{lhistory}/{id}/{user_id}', "AdminsController@getAdminpayhistory");
	
});

/*App::error(function($exception)
{
    return Redirect::to('users/login')->with('message','Your session has expired. Please try logging in again.');
});*/


App::missing(function($exception){
 return Response::view('users.404', array(), 404);
});


