<?php
 
class AdminsController extends BaseController {
	
	protected $layout = 'layouts.main';
	
	public function getLogout() {
		Auth::logout();
		return Redirect::to('users/login')->with('message', 'Your are now logged out!');
	}
	
	public function getAdashboard(){
		$year = \Carbon\Carbon::now()->year;
		$month = \Carbon\Carbon::now()->month;
		$quarter = \Carbon\Carbon::now()->quarter;
		$corporate = DB::table('company')->where('userid', '=', Auth::user()->id)->first();
		
		
		if(Auth::user()->id == 10){$teamid = 500;}
		elseif(Auth::user()->id == 20){$teamid = 510;}
		elseif(Auth::user()->id == 12){$teamid = 520;}
		
		$chartQuery = DB::table('managers')
		->select(DB::raw("manager, team, revenue, period_goal"))
		->where('month', '=', $month)
		->where('year', '=', $year, 'AND')
		->where('team', '!=' , 'team', 'AND')
		->where('manager', '=', Auth::user()->firstname . ' ' . Auth::user()->lastname, 'AND')
		->groupBy('manager')->groupBy('team')->groupBy('revenue')->groupBy('period_goal')
		->orderBy('manager')->orderBy('team')
		->get();

		$chartData = array();
		foreach($chartQuery as $d){
			$chartData[] = array(
				'title' => $d->team,
				'sales' => $d->revenue,
				'goal' => $d->period_goal
			);
		}


		
		$teams = Managers::where('user_id', '=', Auth::user()->id)->where('month', '=', $month, 'AND')->where('year', '=', $year, 'AND')->where('manager', '=', Auth::user()->firstname . ' ' . Auth::user()->lastname, 'AND')->where('team', '!=', 'team', 'AND')->get();
		
		$data = Managers::where('user_id', '=', Auth::user()->id)->where('month', '=', $month, 'AND')->where('year', '=', $year, 'AND')->where('level', '=', 'MGMT', 'AND')->where('team_id_no', '=', $teamid, 'AND')->first();
		
		$qtdata = Managers::where('user_id', '=', Auth::user()->id)->where('qtr', '=', $quarter, 'AND')->where('year', '=', $year, 'AND')->where('level', '=', 'MGMT', 'AND')->where('team_id_no', '=', $teamid, 'AND')->first();
		/*
		
		//Ray's Team
		$emily = Managers::where('user_id', '=', Auth::user()->id)->where('month', '=', $month, 'AND')->where('year', '=', $year, 'AND')->where('manager', '=', Auth::user()->firstname . ' ' . Auth::user()->lastname, 'AND')->where('team_id_no', '=', 6, 'AND')->first();
		$jason = Managers::where('user_id', '=', Auth::user()->id)->where('month', '=', $month, 'AND')->where('year', '=', $year, 'AND')->where('manager', '=', Auth::user()->firstname . ' ' . Auth::user()->lastname, 'AND')->where('team_id_no', '=', 2, 'AND')->first();
		$john = Managers::where('user_id', '=', Auth::user()->id)->where('month', '=', $month, 'AND')->where('year', '=', $year, 'AND')->where('manager', '=', Auth::user()->firstname . ' ' . Auth::user()->lastname, 'AND')->where('team_id_no', '=', 8, 'AND')->first();
		$ray = Managers::where('user_id', '=', Auth::user()->id)->where('month', '=', $month, 'AND')->where('year', '=', $year, 'AND')->where('team_id_no', '=', 10, 'AND')->first();
		$sam = Managers::where('user_id', '=', Auth::user()->id)->where('month', '=', $month, 'AND')->where('year', '=', $year, 'AND')->where('manager', '=', Auth::user()->firstname . ' ' . Auth::user()->lastname, 'AND')->where('team_id_no', '=', 3, 'AND')->first();
		$thom = Managers::where('user_id', '=', Auth::user()->id)->where('month', '=', $month, 'AND')->where('year', '=', $year, 'AND')->where('manager', '=', Auth::user()->firstname . ' ' . Auth::user()->lastname, 'AND')->where('team_id_no', '=', 5, 'AND')->first();
		
		
		//Josh Team
		$andrew = Managers::where('user_id', '=', Auth::user()->id)->where('month', '=', $month, 'AND')->where('year', '=', $year, 'AND')->where('manager', '=', Auth::user()->firstname . ' ' . Auth::user()->lastname, 'AND')->where('team_id_no', '=', 16, 'AND')->first();
		$austin = Managers::where('user_id', '=', Auth::user()->id)->where('month', '=', $month, 'AND')->where('year', '=', $year, 'AND')->where('manager', '=', Auth::user()->firstname . ' ' . Auth::user()->lastname, 'AND')->where('team_id_no', '=', 17, 'AND')->first();
		$ben = Managers::where('user_id', '=', Auth::user()->id)->where('month', '=', $month, 'AND')->where('year', '=', $year, 'AND')->where('manager', '=', Auth::user()->firstname . ' ' . Auth::user()->lastname, 'AND')->where('team_id_no', '=', 18, 'AND')->first();
		$brian = Managers::where('user_id', '=', Auth::user()->id)->where('month', '=', $month, 'AND')->where('year', '=', $year, 'AND')->where('manager', '=', Auth::user()->firstname . ' ' . Auth::user()->lastname, 'AND')->where('team_id_no', '=', 13, 'AND')->first();
		$joe = Managers::where('user_id', '=', Auth::user()->id)->where('month', '=', $month, 'AND')->where('year', '=', $year, 'AND')->where('manager', '=', Auth::user()->firstname . ' ' . Auth::user()->lastname, 'AND')->where('team_id_no', '=', 14, 'AND')->first();
		$josh = Managers::where('user_id', '=', Auth::user()->id)->where('month', '=', $month, 'AND')->where('year', '=', $year, 'AND')->where('manager', '=', Auth::user()->firstname . ' ' . Auth::user()->lastname, 'AND')->where('team_id_no', '=', 12, 'AND')->first();
		$nick = Managers::where('user_id', '=', Auth::user()->id)->where('month', '=', $month, 'AND')->where('year', '=', $year, 'AND')->where('manager', '=', Auth::user()->firstname . ' ' . Auth::user()->lastname, 'AND')->where('team_id_no', '=', 15, 'AND')->first();
		
		
		//Matt Team
		$adam = Managers::where('user_id', '=', Auth::user()->id)->where('month', '=', $month, 'AND')->where('year', '=', $year, 'AND')->where('manager', '=', Auth::user()->firstname . ' ' . Auth::user()->lastname, 'AND')->where('team_id_no', '=', 999, 'AND')->first();
		$amanda = Managers::where('user_id', '=', Auth::user()->id)->where('month', '=', $month, 'AND')->where('year', '=', $year, 'AND')->where('manager', '=', Auth::user()->firstname . ' ' . Auth::user()->lastname, 'AND')->where('team_id_no', '=', 7, 'AND')->first();
		$matte = Managers::where('user_id', '=', Auth::user()->id)->where('month', '=', $month, 'AND')->where('year', '=', $year, 'AND')->where('manager', '=', Auth::user()->firstname . ' ' . Auth::user()->lastname, 'AND')->where('team_id_no', '=', 20, 'AND')->first();
		$mattc = Managers::where('user_id', '=', Auth::user()->id)->where('month', '=', $month, 'AND')->where('year', '=', $year, 'AND')->where('manager', '=', Auth::user()->firstname . ' ' . Auth::user()->lastname, 'AND')->where('team_id_no', '=', 998, 'AND')->first();
		$steve = Managers::where('user_id', '=', Auth::user()->id)->where('month', '=', $month, 'AND')->where('year', '=', $year, 'AND')->where('manager', '=', Auth::user()->firstname . ' ' . Auth::user()->lastname, 'AND')->where('team_id_no', '=', 4, 'AND')->first();

		
		if(Auth::user()->id == 10){
			//ray
			
			
		$chartData = array(
						array(  'title' => $emily->team,
								'sales' => $emily->revenue,
								'goal' => $emily->period_goal),
						
						
						array(  'title' => $jason->team,
								'sales' => $jason->revenue,
								'goal' => $jason->period_goal),
						
						array(  'title' => $john->team,
								'sales' => $john->revenue,
								'goal' => $john->period_goal),
						
						
						array(  'title' => $sam->team,
								'sales' => $sam->revenue,
								'goal' => $sam->period_goal),
						
						array(  'title' => $thom->team,
								'sales' => $thom->revenue,
								'goal' => $thom->period_goal)
						
						);
						
		}elseif(Auth::user()->id == 12){
		//josh
		
		$chartData = array(
						array(  'title' => $andrew->team,
								'sales' => $andrew->revenue,
								'goal' => $andrew->period_goal),
						
						
						array(  'title' => $austin->team,
								'sales' => $austin->revenue,
								'goal' => $austin->period_goal),
						
						array(  'title' => $ben->team,
								'sales' => $ben->revenue,
								'goal' => $ben->period_goal),
						
						
						array(  'title' => $brian->team,
								'sales' => $brian->revenue,
								'goal' => $brian->period_goal),
						
						array(  'title' => $joe->team,
								'sales' => $joe->revenue,
								'goal' => $joe->period_goal),
						
						array(  'title' => $josh->team,
								'sales' => $josh->revenue,
								'goal' => $josh->period_goal),
						
						array(  'title' => $nick->team,
								'sales' => $nick->revenue,
								'goal' => $nick->period_goal)
						
						);
		
		}elseif(Auth::user()->id == 20){
		//Matt's Team
		$chartData = array(
						
						
						array(  'title' => $amanda->team,
								'sales' => $amanda->revenue,
								'goal' => $amanda->period_goal),
						
						array(  'title' => $matte->team,
								'sales' => $matte->revenue,
								'goal' => $matte->period_goal),
						
						
						array(  'title' => $steve->team,
								'sales' => $steve->revenue,
								'goal' => $steve->period_goal)
								
								);
			
			
		}; */
		
							
		
		
		$this->layout->content = View::make('admins.adashboard', array('dashboard_data' => $data, 'chart_data' => $chartData, 'corp' => $corporate, 'dashboard_dataq' => $qtdata, 'teams' => $teams));
	}
	
	function getEmployeeinfo($employee_id, $level){
		View::share('emploid', User::where('id', '=', $employee_id)->first());
		View::share('eemdata', Company::where('userid', '=', $employee_id)->first());
		View::share('emid', User::where('id', '=', $employee_id)->first());
		if($level == "SE1" || $level == "SE2"){
		$elid	=	User::where('id', '=', $employee_id)->first();
		$emdata = Company::where('userid', '=', $employee_id)->first();
		$data = Summary::where('userid', '=', $employee_id)->first();
		$customerdata = Customer::where('userid', '=', $employee_id)->get();

		$chartData = array(
						array(  'title' => \Carbon\Carbon::now()->subMonths(8)->formatLocalized('%b %Y'),
								'sales' => $data->eightmonthago_revenue,
								'goal' => $data->eighthmonthago_goal),
						array(  'title' => \Carbon\Carbon::now()->subMonths(7)->formatLocalized('%b %Y'),
								'sales' => $data->seventhmonthago_revenue,
								'goal' => $data->seventhmonthago_goal),
						array(  'title' => \Carbon\Carbon::now()->subMonths(6)->formatLocalized('%b %Y'),
								'sales' => $data->sixthmonthago_revenue,
								'goal' => $data->sixthmonthago_goal),
						array(  'title' => \Carbon\Carbon::now()->subMonths(5)->formatLocalized('%b %Y'),
								'sales' => $data->fifthmonthago_revenue,
								'goal' => $data->fifthmonthago_goal),
						array(  'title' => \Carbon\Carbon::now()->subMonths(4)->formatLocalized('%b %Y'),
								'sales' => $data->fourthmonthago_revenue,
								'goal' => $data->fourthmonthago_goal),
						array(  'title' => \Carbon\Carbon::now()->subMonths(3)->formatLocalized('%b %Y'),
								'sales' => $data->threemonthago_revenue,
								'goal' => $data->threemonthago_goal),
						array(  'title' => \Carbon\Carbon::now()->subMonths(2)->formatLocalized('%b %Y'),
								'sales' => $data->twomonthago_revenue,
								'goal' => $data->twomonthago_goal),
						array(  'title' => \Carbon\Carbon::now()->subMonths(1)->formatLocalized('%b %Y'),
								'sales' => $data->onemonthago_revenue,
								'goal' => $data->onemonthago_goal),
						array(  'title' => \Carbon\Carbon::now()->formatLocalized('%b %Y'),
								'sales' => $data->current_month_sales,
								'goal' => $data->current_month_goal,
								'dashLengthColumn'=> 5,
                    			'alpha'=> 0.2,
                    			'additional'=>"(projection)"));
		$chartBounty4 = array(
										array(	'label' => 'Revenue Goal',
												  'value' => number_format(($data->zmac_qtr_revenue/$data->zmac_qtr_goal*100), 2, ".", "." )),
										array(	'label' => 'Need',
												  'value' => number_format((100-$data->zmac_qtr_revenue/$data->zmac_qtr_goal*100), 2, ".", "."))
						
						);
						
		$chartBounty5 = array(
										array(	'label' => 'Gross Profit %',
												  'value' => number_format(100*$data->zmac_qtr_gp_por, 2, ".", ".")),
										array(	'label' => 'Need',
												  'value' => number_format((100-100*$data->zmac_qtr_gp_por), 2, ".", "."))
						
						);
		
		
		$this->layout->content = View::make('admins.admin-sedashboard', array('dashboard_data' => $data, 'chart_data' => $chartData, 'customerData' => $customerdata, 'chartBounty4' => $chartBounty4, 'chartBounty5' => $chartBounty5, 'emdata' => $emdata, 'elid' => $elid));
		
		}
		
		elseif($level == "AE1" || $level == "AE2"){
			$emdata = Company::where('userid', '=', $employee_id)->first();
			$data = Accounte::where('user_id', '=', $employee_id)->first();
			$customerdata = Customer::where('userid', '=', $employee_id)->first();
			
			
			$chartData = array(
						array(  'title' => \Carbon\Carbon::now('America/Chicago')->subMonths(8)->formatLocalized('%b %Y'),
								'sales' => $data->eightago_rev,
								'goal' => $data->eightago_goal),
						array(  'title' => \Carbon\Carbon::now('America/Chicago')->subMonths(7)->formatLocalized('%b %Y'),
								'sales' => $data->sevenago_rev,
								'goal' => $data->sevenago_goal),
						array(  'title' => \Carbon\Carbon::now('America/Chicago')->subMonths(6)->formatLocalized('%b %Y'),
								'sales' => $data->sixago_rev,
								'goal' => $data->sixago_goal),
						array(  'title' => \Carbon\Carbon::now('America/Chicago')->subMonths(5)->formatLocalized('%b %Y'),
								'sales' => $data->fiveago_rev,
								'goal' => $data->fiveago_goal),
						array(  'title' => \Carbon\Carbon::now('America/Chicago')->subMonths(4)->formatLocalized('%b %Y'),
								'sales' => $data->fourago_rev,
								'goal' => $data->fourago_goal),
						array(  'title' => \Carbon\Carbon::now('America/Chicago')->subMonths(3)->formatLocalized('%b %Y'),
								'sales' => $data->threeago_rev,
								'goal' => $data->threeago_goal),
						array(  'title' => \Carbon\Carbon::now('America/Chicago')->subMonths(2)->formatLocalized('%b %Y'),
								'sales' => $data->twoago_rev,
								'goal' => $data->twoago_goal),
						array(  'title' => \Carbon\Carbon::now('America/Chicago')->subMonths(1)->formatLocalized('%b %Y'),
								'sales' => $data->oneago_month_revenue,
								'goal' => $data->oneago_month_goal),
						array(  'title' => \Carbon\Carbon::now('America/Chicago')->formatLocalized('%b %Y'),
								'sales' => $data->current_month_sales,
								'goal' => $data->current_month_goal,
								'dashLengthColumn'=> 5,
                    			'alpha'=> 0.2,
                    			'additional'=>"(projection)"));
								
			$chartCoPiece1 = array(
										array(	'label' => 'Revenue Goal',
												  'value' => number_format(($data->zmac_qtr_revenue/$data->zmac_qtr_goal*100), 2, ".", "." )),
										array(	'label' => 'Need',
												  'value' => number_format((100-$data->zmac_qtr_revenue/$data->zmac_qtr_goal*100), 2, ".", "."))
						
						);
						
			$chartCoPiece2 = array(
											array(	'label' => 'Gross Profit %',
													  'value' => number_format(($data->zmac_qtr_gp_por*100), 2, ".", ".")),
											array(	'label' => 'Need',
													  'value' => number_format((100-$data->zmac_qtr_gp_por), 2, ".", "."))
							
							);	
							
											
									
			
			$this->layout->content = View::make('admins.admin-aedashboard', array('dashboard_data' => $data, 'chartCoPiece1_data' => $chartCoPiece1, 'chartCoPiece2_data' => $chartCoPiece2, 'chart_data' =>$chartData, 'emdata' => $emdata));
			
			
		}
		
		elseif($level == "CE1" || $level == "CE2"){
			$emdata = Company::where('userid', '=', $employee_id)->first();
			$data = Carriere::where('userid', '=', $employee_id)->first();
			$customerdata = Customer::where('userid', '=', $employee_id)->first();
			
			$chartData = array(
						array(  'title' => \Carbon\Carbon::now('America/Chicago')->subMonths(8)->formatLocalized('%b %Y'),
								'sales' => $data->eightmonthago_gp,
								'goal' => $data->eightmonthago_goal),
						array(  'title' => \Carbon\Carbon::now('America/Chicago')->subMonths(7)->formatLocalized('%b %Y'),
								'sales' => $data->sevenmonthago_gp,
								'goal' => $data->sevenmonthago_goal),
						array(  'title' => \Carbon\Carbon::now('America/Chicago')->subMonths(6)->formatLocalized('%b %Y'),
								'sales' => $data->sixmonthago_gp,
								'goal' => $data->sixmonthago_goal),
						array(  'title' => \Carbon\Carbon::now('America/Chicago')->subMonths(5)->formatLocalized('%b %Y'),
								'sales' => $data->fivemonthago_gp,
								'goal' => $data->fivemonthago_goal),
						array(  'title' => \Carbon\Carbon::now('America/Chicago')->subMonths(4)->formatLocalized('%b %Y'),
								'sales' => $data->fourmonthago_gp,
								'goal' => $data->fourmonthago_goal),
						array(  'title' => \Carbon\Carbon::now('America/Chicago')->subMonths(3)->formatLocalized('%b %Y'),
								'sales' => $data->threemonthago_gp,
								'goal' => $data->threemonthago_goal),
						array(  'title' => \Carbon\Carbon::now('America/Chicago')->subMonths(2)->formatLocalized('%b %Y'),
								'sales' => $data->twomonthago_gp,
								'goal' => $data->twomonthago_goal),
						array(  'title' => \Carbon\Carbon::now('America/Chicago')->subMonths(1)->formatLocalized('%b %Y'),
								'sales' => $data->onemonthago_gp,
								'goal' => $data->onemonthago_goal),
						array(  'title' => \Carbon\Carbon::now('America/Chicago')->formatLocalized('%b %Y'),
								'sales' => $data->current_month_gp,
								'goal' => $data->current_month_goal,
								'dashLengthColumn'=> 5,
                    			'alpha'=> 0.2,
                    			'additional'=>"(projection)"));
								
			$chartCoPiece1 = array(
										array(	'label' => 'Revenue Goal',
												  'value' => number_format(($data->zmac_qtr_revenue/$data->zmac_qtr_goal*100), 2, ".", "." )),
										array(	'label' => 'Need',
												  'value' => number_format((100-$data->zmac_qtr_revenue/$data->zmac_qtr_goal*100), 2, ".", "."))
						
						);
						
			$chartCoPiece2 = array(
											array(	'label' => 'Gross Profit %',
													  'value' => number_format((100*$data->zmac_qtr_gp_por), 2, ".", ".")),
											array(	'label' => 'Need',
													  'value' => number_format((100-100*$data->zmac_qtr_gp_por), 2, ".", "."))
							
							);
							
											
									
			
			$this->layout->content = View::make('admins.admin-carrierex', array('dashboard_data' => $data, 'chartCoPiece1_data' => $chartCoPiece1, 'chartCoPiece2_data' => $chartCoPiece2, 'chart_data' =>$chartData, 'emdata' => $emdata));
			
		
			
			
			
		}
		
	}
	
	
	public function getAdminseoverview($user_id){
		
		    
		    $elid	=	User::where('id', '=', $user_id)->first();
			$customerdata = Customer::where('userid', '=', $user_id)->get();
			$data = Summary::where('userid', '=', $user_id)->first();
			$levelone = Customer::where('userid', '=', $user_id)->where('l1_eligible', '=', 'yes', 'AND')->get();
			$leveltwo = Customer::where('userid', '=', $user_id)->where('l2_eligible', '=', 'yes', 'AND')->get();
			$levelthree = Customer::where('userid', '=', $user_id)->where('lthree_eligible', '=', 'yes', 'AND')->get();
			
			$this->layout->content = View::make('admins.adminseoverview', array('dashboard_data' => $data, 'customerData' => $customerdata, 'levelone' => $levelone, 'leveltwo' => $leveltwo, 'levelthree' => $levelthree, 'elid' =>$elid));
		
		
		
	}
	
	public function getAdminsehistory($userr_id){
		
		$data = Summary::where('userid', '=', $userr_id)->first();
		$customerData = DB::table('customers')->where('userid', '=', $userr_id)->get();
		$idata = DB::table('history')->where('user_id', '=', $userr_id)->where('quaterly_bank_paid','>',0)->get();
		$idatac = DB::table('history')->where('user_id', '=', $userr_id)->where('quaterly_company_incentive_paid','>',0)->get();
		
		$this->layout->content = View::make('admins.adminsehistory', array('dashboard_data' => $data, 'customerData' => $customerData, 'idata' => $idata, 'idatac' => $idatac));
		
	}
	
	function getAdminpayhistory($lhistory, $id, $user_id){
		
		$data = DB::table('salesperson_summary')->where('userid', '=', $user_id)->first();
		$customer_detail = Customer::where('userid', '=', $user_id)->where('customerid', '=', $id, 'AND')->first();
		$customerData = DB::table('customers')->where('userid', '=', $user_id)->get();
		$odata = Allorders::where('se_id', '=', $user_id)->where('cp_code', '=', $id, 'AND')->get();
		
		if($lhistory == 'l1history'){
			$odata = Borders::where('userid', '=', $user_id)->where('customerid', '=', $id, 'AND')->where('level1_order', '=', 'x', 'AND')->get();
		}
		elseif($lhistory == 'l2history'){
			$odata = Borders::where('userid', '=', $user_id)->where('customerid', '=', $id, 'AND')->where('level2_order', '=', 'x', 'AND')->get();
			
		}
		else{
			$odata = Borders::where('userid', '=', $user_id)->where('customerid', '=', $id, 'AND')->where('level3_order', '=', 'x', 'AND')->get();
			
		}
			$border1 = Borders::where('userid', '=', $user_id)->where('customerid', '=', $id, 'AND')->where('level1_order', '=', 'x', 'AND')->get();
		
		$border2 = Borders::where('userid', '=', $user_id)->where('customerid', '=', $id, 'AND')->where('level2_order', '=', 'x', 'AND')->get();
		$border3 = Borders::where('userid', '=', $user_id)->where('customerid', '=', $id, 'AND')->where('level3_order', '=', 'x', 'AND')->get();
		
		 $this->layout->content = View::make('admins.phistory', array('dashboard_data' => $data, 'customerData' => $customerData, 'orderData' => $odata, 'customer_detail' => $customer_detail, 'border1' => $border1, 'border2' => $border2, 'border3' => $border3));
		
	}//end history
	
	
	
	
	

	


}
?>