<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
		
		if(Auth::check()){
		
		if(Auth::user()->type == 'se'){
		$dorders = Allorders::where('se_id', '=', Auth::user()->id)->get();
		}
		elseif(Auth::user()->type == 'ce'){
		$dorders = Allorders::where('ce_id', '=', Auth::user()->id)->get();
		}
		elseif(Auth::user()->type == 'ae'){
		$dorders = Allorders::where('ae_id', '=', Auth::user()->id)->get();
		}
	};
		
		View::share('dorders');
	}
		

}