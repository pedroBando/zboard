<?php

class LoginController extends BaseController
{
	protected $layout = 'layouts.loginIndex';
	function login(){   
		$this->layout->content =  View::make('login.login');
	}
	
	public function register() {
    	$this->layout->content = View::make('login.register');
	}
	
	public function postCreate() {
		
		 $validator = Validator::make(Input::all(), User::$rules);
 
		  if ($validator->passes()) {
			  $user = new User;
			  $user->firstname = Input::get('firstname');
			  $user->lastname = Input::get('lastname');
			  $user->email = Input::get('email');
			  $user->password = Hash::make(Input::get('password'));
			  $user->save();
			  $this->layout->content = View::make('login.login');
		  } else {
			   return Redirect::to('login/register')->with('message', 'The following errors occurred')->withErrors($validator)->withInput();
		  }
    
	}
	
	
	
	public function postSignin() {
		if (Auth::attempt(array('email'=>Input::get('email'), 'password'=>Input::get('password')))) {
			
			if(Auth::user()->type == 'mg'){
			return Redirect::to('admin/dash');
			}
			else{
			return Redirect::to('users/dashboard')->with('message', 'You are now logged in!');
			}
			
		} else {
			return Redirect::to('users/login')
				->with('message', 'Your username/password combination was incorrect')
				->withInput();
		}
	}
	
	public function getLogout() {
		Auth::logout();
		return Redirect::to('users/login')->with('message', 'Your are now logged out!');
	}
}