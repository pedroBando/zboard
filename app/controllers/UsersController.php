<?php
 
class UsersController extends BaseController {
	
	
	
 	protected $layout = "layouts.main";
	
	public function __construct() {

    $this->beforeFilter('csrf', array('on'=>'post'));
	// $this->beforeFilter('auth', array('only'=>array('getDashboard')));
	}
	

	public function getProfile()
	{
		
			$this->layout->content = View::make('users.profile');
			
			//return Redirect::to('users/login');
		
	}
	
	public function postUserPasswordChange(){
    $validator = Validator::make(Input::all(), User::$change_password_rules);
    if($validator->passes()){

        $user = User::findOrFail(Auth::user()->id);

        $user->password = Hash::make(Input::get('new_password'));
        $user->save();
        return Redirect::to('users/profile')->with('message', 'Password changed successfully.');
    }else {
        return Redirect::to('users/profile')->with('message', 'The following errors occurred')->withErrors($validator)->withInput();
    }
	}

	public function post_upload()
    {
		$id = Auth::user()->id;
        $input = Input::all();
        $rules = array(
             'file' => 'image|mimes:jpg,gif,png|max:3000',
        );

       /* $validator = Validator::make(Input::all(), $rules);

         if ($validation->fails())
         {
			//var_dump($validator->messages()->toArray());
         	return Redirect::to('users/profile')->with('message', 'The following errors occurred')->withErrors($validator)->withInput();
         }*/


        $file = Input::file('file'); // your file upload input field in the form should be named 'file'

        $destinationPath = __DIR__ . '/../../public/uploads/profiles';
        //$filename = $file->getClientOriginalName();
		$extension =$file->getClientOriginalExtension();
        $filename = $id.'.'.$extension;
        
        $uploadSuccess = Input::file('file')->move($destinationPath, $filename);
		DB::table('users')->where('id', '=', $id)->update(array('avatar' => $filename));
		View::share('$avatar', '$filename');


        if( $uploadSuccess ) {
          return Redirect::to('users/profile')->with('message', 'The following errors occurred');
        } else {
           return Response::json('error', 400);
        }
    }
	
	public function index(){
		
		if(Auth::user()->type == se){
		$dorders = Allorders::where('se_id', '=', Auth::user()->id);
		}
		elseif(Auth::user()->type == ce){
		$dorders = Allorders::where('ce_id', '=', Auth::user()->id);
		}
		elseif(Auth::user()->type == ae){
		$dorders = Allorders::where('ae_id', '=', Auth::user()->id);
		}

		
		
	}
	
	public function getDashboard() {
		$userrid = Auth::user()->type;
		if($userrid == 'se' ){
		$data = Summary::where('userid', '=', Auth::user()->id)->first();
		$customerdata = Customer::where('userid', '=', Auth::user()->id)->get();
		
		
		
		
		//$orders = DB::select("select * from orders where order_date > DATE_ADD(order_date, INTERVAL 60 DAY)");
		
		$chartData = array(
						array(  'title' => \Carbon\Carbon::now()->subMonths(8)->formatLocalized('%b %Y'),
								'sales' => $data->eightmonthago_revenue,
								'goal' => $data->eighthmonthago_goal),
						array(  'title' => \Carbon\Carbon::now()->subMonths(7)->formatLocalized('%b %Y'),
								'sales' => $data->seventhmonthago_revenue,
								'goal' => $data->seventhmonthago_goal),
						array(  'title' => \Carbon\Carbon::now()->subMonths(6)->formatLocalized('%b %Y'),
								'sales' => $data->sixthmonthago_revenue,
								'goal' => $data->sixthmonthago_goal),
						array(  'title' => \Carbon\Carbon::now()->subMonths(5)->formatLocalized('%b %Y'),
								'sales' => $data->fifthmonthago_revenue,
								'goal' => $data->fifthmonthago_goal),
						array(  'title' => \Carbon\Carbon::now()->subMonths(4)->formatLocalized('%b %Y'),
								'sales' => $data->fourthmonthago_revenue,
								'goal' => $data->fourthmonthago_goal),
						array(  'title' => \Carbon\Carbon::now()->subMonths(3)->formatLocalized('%b %Y'),
								'sales' => $data->threemonthago_revenue,
								'goal' => $data->threemonthago_goal),
						array(  'title' => \Carbon\Carbon::now()->subMonths(2)->formatLocalized('%b %Y'),
								'sales' => $data->twomonthago_revenue,
								'goal' => $data->twomonthago_goal),
						array(  'title' => \Carbon\Carbon::now()->subMonths(1)->formatLocalized('%b %Y'),
								'sales' => $data->onemonthago_revenue,
								'goal' => $data->onemonthago_goal),
						array(  'title' => \Carbon\Carbon::now()->formatLocalized('%b %Y'),
								'sales' => $data->current_month_sales,
								'goal' => $data->current_month_goal,
								'dashLengthColumn'=> 5,
                    			'alpha'=> 0.2,
                    			'additional'=>"(projection)"));
		$chartBounty4 = array(
										array(	'label' => 'Revenue Goal',
												  'value' => number_format(($data->zmac_qtr_revenue/$data->zmac_qtr_goal*100), 2, ".", "." )),
										array(	'label' => 'Need',
												  'value' => number_format((100-$data->zmac_qtr_revenue/$data->zmac_qtr_goal*100), 2, ".", "."))
						
						);
						
		$chartBounty5 = array(
										array(	'label' => 'Gross Profit %',
												  'value' => number_format(100*$data->zmac_qtr_gp_por, 2, ".", ".")),
										array(	'label' => 'Need',
												  'value' => number_format((100-100*$data->zmac_qtr_gp_por), 2, ".", "."))
						
						);
						
						//$testDate =DB::raw(str_to_date(order_date));
						
		
			$currentMonthSales = $data->current_month_sales;
		$this->layout->content = View::make('users.dashboard', array('dashboard_data' => $data, 'chart_data' => $chartData, 'customerData' => $customerdata, 'chartBounty4' => $chartBounty4, 'chartBounty5' => $chartBounty5));
		}//end if
	
		elseif($userrid == 'ae'){
			
//start account executives pages
			$data = Accounte::where('user_id', '=', Auth::user()->id)->first();
			$customerdata = Customer::where('userid', '=', Auth::user()->id)->first();
			
			
			$chartData = array(
						array(  'title' => \Carbon\Carbon::now('America/Chicago')->subMonths(8)->formatLocalized('%b %Y'),
								'sales' => $data->eightago_rev,
								'goal' => $data->eightago_goal),
						array(  'title' => \Carbon\Carbon::now('America/Chicago')->subMonths(7)->formatLocalized('%b %Y'),
								'sales' => $data->sevenago_rev,
								'goal' => $data->sevenago_goal),
						array(  'title' => \Carbon\Carbon::now('America/Chicago')->subMonths(6)->formatLocalized('%b %Y'),
								'sales' => $data->sixago_rev,
								'goal' => $data->sixago_goal),
						array(  'title' => \Carbon\Carbon::now('America/Chicago')->subMonths(5)->formatLocalized('%b %Y'),
								'sales' => $data->fiveago_rev,
								'goal' => $data->fiveago_goal),
						array(  'title' => \Carbon\Carbon::now('America/Chicago')->subMonths(4)->formatLocalized('%b %Y'),
								'sales' => $data->fourago_rev,
								'goal' => $data->fourago_goal),
						array(  'title' => \Carbon\Carbon::now('America/Chicago')->subMonths(3)->formatLocalized('%b %Y'),
								'sales' => $data->threeago_rev,
								'goal' => $data->threeago_goal),
						array(  'title' => \Carbon\Carbon::now('America/Chicago')->subMonths(2)->formatLocalized('%b %Y'),
								'sales' => $data->twoago_rev,
								'goal' => $data->twoago_goal),
						array(  'title' => \Carbon\Carbon::now('America/Chicago')->subMonths(1)->formatLocalized('%b %Y'),
								'sales' => $data->oneago_month_revenue,
								'goal' => $data->oneago_month_goal),
						array(  'title' => \Carbon\Carbon::now('America/Chicago')->formatLocalized('%b %Y'),
								'sales' => $data->current_month_sales,
								'goal' => $data->current_month_goal,
								'dashLengthColumn'=> 5,
                    			'alpha'=> 0.2,
                    			'additional'=>"(projection)"));
								
			$chartCoPiece1 = array(
										array(	'label' => 'Revenue Goal',
												  'value' => number_format(($data->zmac_qtr_revenue/$data->zmac_qtr_goal*100), 2, ".", "." )),
										array(	'label' => 'Need',
												  'value' => number_format((100-$data->zmac_qtr_revenue/$data->zmac_qtr_goal*100), 2, ".", "."))
						
						);
						
			$chartCoPiece2 = array(
											array(	'label' => 'Gross Profit %',
													  'value' => number_format(($data->zmac_qtr_gp_por*100), 2, ".", ".")),
											array(	'label' => 'Need',
													  'value' => number_format((100-$data->zmac_qtr_gp_por), 2, ".", "."))
							
							);	
							
											
									
			
			$this->layout->content = View::make('users.accountex', array('dashboard_data' => $data, 'chartCoPiece1_data' => $chartCoPiece1, 'chartCoPiece2_data' => $chartCoPiece2, 'chart_data' =>$chartData));
		
			
		}
		
// start carrier executives page

		elseif($userrid == 'ce'){
		$data = Carriere::where('userid', '=', Auth::user()->id)->first();
			$customerdata = Customer::where('userid', '=', Auth::user()->id)->first();
			
			$chartData = array(
						array(  'title' => \Carbon\Carbon::now('America/Chicago')->subMonths(8)->formatLocalized('%b %Y'),
								'sales' => $data->eightmonthago_gp,
								'goal' => $data->eightmonthago_goal),
						array(  'title' => \Carbon\Carbon::now('America/Chicago')->subMonths(7)->formatLocalized('%b %Y'),
								'sales' => $data->sevenmonthago_gp,
								'goal' => $data->sevenmonthago_goal),
						array(  'title' => \Carbon\Carbon::now('America/Chicago')->subMonths(6)->formatLocalized('%b %Y'),
								'sales' => $data->sixmonthago_gp,
								'goal' => $data->sixmonthago_goal),
						array(  'title' => \Carbon\Carbon::now('America/Chicago')->subMonths(5)->formatLocalized('%b %Y'),
								'sales' => $data->fivemonthago_gp,
								'goal' => $data->fivemonthago_goal),
						array(  'title' => \Carbon\Carbon::now('America/Chicago')->subMonths(4)->formatLocalized('%b %Y'),
								'sales' => $data->fourmonthago_gp,
								'goal' => $data->fourmonthago_goal),
						array(  'title' => \Carbon\Carbon::now('America/Chicago')->subMonths(3)->formatLocalized('%b %Y'),
								'sales' => $data->threemonthago_gp,
								'goal' => $data->threemonthago_goal),
						array(  'title' => \Carbon\Carbon::now('America/Chicago')->subMonths(2)->formatLocalized('%b %Y'),
								'sales' => $data->twomonthago_gp,
								'goal' => $data->twomonthago_goal),
						array(  'title' => \Carbon\Carbon::now('America/Chicago')->subMonths(1)->formatLocalized('%b %Y'),
								'sales' => $data->onemonthago_gp,
								'goal' => $data->onemonthago_goal),
						array(  'title' => \Carbon\Carbon::now('America/Chicago')->formatLocalized('%b %Y'),
								'sales' => $data->current_month_gp,
								'goal' => $data->current_month_goal,
								'dashLengthColumn'=> 5,
                    			'alpha'=> 0.2,
                    			'additional'=>"(projection)"));
								
			$chartCoPiece1 = array(
										array(	'label' => 'Revenue Goal',
												  'value' => number_format(($data->zmac_qtr_revenue/$data->zmac_qtr_goal*100), 2, ".", "." )),
										array(	'label' => 'Need',
												  'value' => number_format((100-$data->zmac_qtr_revenue/$data->zmac_qtr_goal*100), 2, ".", "."))
						
						);
						
			$chartCoPiece2 = array(
											array(	'label' => 'Gross Profit %',
													  'value' => number_format((100*$data->zmac_qtr_gp_por), 2, ".", ".")),
											array(	'label' => 'Need',
													  'value' => number_format((100-100*$data->zmac_qtr_gp_por), 2, ".", "."))
							
							);
							
											
									
			
			$this->layout->content = View::make('users.carrierex', array('dashboard_data' => $data, 'chartCoPiece1_data' => $chartCoPiece1, 'chartCoPiece2_data' => $chartCoPiece2, 'chart_data' =>$chartData));
			
		}
		
		//else{ return Redirect::to('users/noroute')->with('message', 'The following errors occurred');
		//}
                 
		
		
	}//end getdashboard
	
	
	public function getOverview(){
		
		
			$customerdata = Customer::where('userid', '=', Auth::user()->id)->get();
			$data = Summary::where('userid', '=', Auth::user()->id)->first();
			$levelone = Customer::where('userid', '=', Auth::user()->id)->where('l1_eligible', '=', 'yes', 'AND')->get();
			$leveltwo = Customer::where('userid', '=', Auth::user()->id)->where('l2_eligible', '=', 'yes', 'AND')->get();
			$levelthree = Customer::where('userid', '=', Auth::user()->id)->where('lthree_eligible', '=', 'yes', 'AND')->get();
			
			$this->layout->content = View::make('users.overview', array('dashboard_data' => $data, 'customerData' => $customerdata, 'levelone' => $levelone, 'leveltwo' => $leveltwo, 'levelthree' => $levelthree));
		
		
	}
	
	public function getCustomer($id){
		
            $data = Customer::where('userid', '=', Auth::user()->id)->where('customerid', '=', $id, 'AND')->first();
			$odata = Allorders::where('se_id', '=', Auth::user()->id)->where('cp_code', '=', $id, 'AND')->get();
			$customerdata = Customer::where('userid', '=', Auth::user()->id)->get();
			$ddata = Summary::where('userid', '=', Auth::user()->id)->first();
			
			
			// Start Bounty Chart 1
			if($data->level12_revenue > 5000 && $data->level12_gp_por > .149){
				
				$chartBounty1 = array(
										array(	'label' => 'Done',
												  'value' => '100')
						
						);
			}
			elseif($data->level12_revenue < 5000 ){
				
				$chartBounty1 = array(
										array(	'label' => 'Revenue',
												  'value' => $data->level12_revenue/5000*100 ),
										array(	'label' => 'Need',
												  'value' => 100-$data->level12_revenue/5000*100)
						
						);
				
			}
			//end bounty chart 1
			
			// Start Bounty Chart 2
			if($data->level12_revenue > 20000 && $data->level12_gp_por > .189){
				
				$chartBounty2 = array(
										array(	'label' => 'Done',
												  'value' => '100')
						
						);
			}
			elseif($data->level12_revenue < 20000 ){
				
				$chartBounty2 = array(
										array(	'label' => 'Revenue',
												  'value' => $data->level12_revenue/20000*100 ),
										array(	'label' => 'Need',
												  'value' => 100-$data->level12_revenue/20000*100)
						
						);
				
			}
			//end bounty chart 2
			
			// Start Bounty Chart 3
			if($data->level3_revenue > 50000){
				
				$chartBounty3 = array(
										array(	'label' => 'Done',
												  'value' => '100')
						
						);
			}
			elseif($data->level3_revenue < 50000 ){
				
				$chartBounty3 = array(
										array(	'label' => 'Revenue',
												  'value' => $data->level3_revenue/50000*100 ),
										array(	'label' => 'Need',
												  'value' => 100-$data->level3_revenue/50000*100)
						
						);
				
			}
			//end bounty chart 3
			
		
			
			
		
			
			$this->layout->content = View::make('users.customer', array('customerData' => $customerdata, 'dashboard_data' => $ddata, 'customer_detail' => $data, 'mycustomer' => $id, 'chartBounty1' => $chartBounty1, 'chartBounty2' => $chartBounty2, 'chartBounty3' => $chartBounty3, 'orderData' => $odata));
			
		
		
	
	}
	
	function getHistory(){
		
		$data = Summary::where('userid', '=', Auth::user()->id)->first();
		$customerData = DB::table('customers')->where('userid', '=', Auth::user()->id)->get();
		$idata = DB::table('history')->where('user_id', '=', Auth::user()->id)->where('quaterly_bank_paid','>',0)->get();
		$idatac = DB::table('history')->where('user_id', '=', Auth::user()->id)->where('quaterly_company_incentive_paid','>',0)->get();
		
		$this->layout->content = View::make('users.history', array('dashboard_data' => $data, 'customerData' => $customerData, 'idata' => $idata, 'idatac' => $idatac));
		
	}//end history
	
	function getPayhistory($lhistory, $id){
		
		$data = DB::table('salesperson_summary')->where('userid', '=', Auth::user()->id)->first();
		$customer_detail = Customer::where('userid', '=', Auth::user()->id)->where('customerid', '=', $id, 'AND')->first();
		$customerData = DB::table('customers')->where('userid', '=', Auth::user()->id)->get();
		$odata = Allorders::where('se_id', '=', Auth::user()->id)->where('cp_code', '=', $id, 'AND')->get();
		
		if($lhistory == 'l1history'){
			$odata = Borders::where('userid', '=', Auth::user()->id)->where('customerid', '=', $id, 'AND')->where('level1_order', '=', 'x', 'AND')->get();
		}
		elseif($lhistory == 'l2history'){
			$odata = Borders::where('userid', '=', Auth::user()->id)->where('customerid', '=', $id, 'AND')->where('level2_order', '=', 'x', 'AND')->get();
			
		}
		else{
			$odata = Borders::where('userid', '=', Auth::user()->id)->where('customerid', '=', $id, 'AND')->where('level3_order', '=', 'x', 'AND')->get();
			
		}
			$border1 = Borders::where('userid', '=', Auth::user()->id)->where('customerid', '=', $id, 'AND')->where('level1_order', '=', 'x', 'AND')->get();
		
		$border2 = Borders::where('userid', '=', Auth::user()->id)->where('customerid', '=', $id, 'AND')->where('level2_order', '=', 'x', 'AND')->get();
		$border3 = Borders::where('userid', '=', Auth::user()->id)->where('customerid', '=', $id, 'AND')->where('level3_order', '=', 'x', 'AND')->get();
		
		 $this->layout->content = View::make('users.phistory', array('dashboard_data' => $data, 'customerData' => $customerData, 'orderData' => $odata, 'customer_detail' => $customer_detail, 'border1' => $border1, 'border2' => $border2, 'border3' => $border3));
		
	}//end history
	
	
	
	
	
	//start company performance chart
	function getCopchart(){
		
		$userrid = Auth::user()->id;
		$corporate = DB::table('company')->where('userid', '=', Auth::user()->id)->first();
		if(Auth::user()->type == 'se' ){
			
			$data = Summary::where('userid', '=', Auth::user()->id)->first();
			$corporate = DB::table('company')->where('userid', '=', Auth::user()->id)->first();
			$this->layout->content = View::make('users.copchart', array('dashboard_data' => $data, 'userid' => $userrid, 'corp' => $corporate));
		}
		elseif(Auth::user()->type == 'ae'){
			
			$data = Accounte::where('user_id', '=', Auth::user()->id)->first();
			$corporate = DB::table('company')->where('userid', '=', Auth::user()->id)->first();
			$this->layout->content = View::make('users.copchart', array('dashboard_data' => $data, 'userid' => $userrid, 'corp' => $corporate));
		}
		elseif(Auth::user()->type == 'ce'){
	
			$data = Carriere::where('userid', '=', Auth::user()->id)->first();
			$corporate = DB::table('company')->where('userid', '=', Auth::user()->id)->first();
			$this->layout->content = View::make('users.copchart', array('dashboard_data' => $data, 'userid' => $userrid, 'corp' => $corporate));
		}
		else{return Redirect::to('users/dashboard');}
		
		
		
		
		
	}//end co chart
	
	//start carrier history
	function getCarrierhistory(){
		$dashboard_data = Carriere::where('userid', '=', Auth::user()->id)->first();
		$data = Allorders::where('ce_id', '=', Auth::user()->id)->get();
		$idata = DB::table('history')->where('user_id', '=', Auth::user()->id)->where('quaterly_bank_paid','>',0)->get();
		$idatac = DB::table('history')->where('user_id', '=', Auth::user()->id)->where('quaterly_company_incentive_paid','>',0)->get();
		$incentives = DB::table('incentives')->where('userid', '=', Auth::user()->id)->first();
		
		$this->layout->content = View::make('users.carrierhistory', array('data' => $data, 'idata' => $idata, 'dashboard_data' => $dashboard_data,'idatac' => $idatac, 'incentives' => $incentives));
		
		
	}//end carrierhistory
	
	//start account executive hsitory
	function getAcchistory(){
		$dashboard_data = Accounte::where('user_id', '=', Auth::user()->id)->first();
		$data = DB::table('ae_customers')->where('user_id', '=', Auth::user()->id)->get();
		//$cdata = DB::table('customers')->where('customerid', '=', $data->cp_code)->get();
		$idata = DB::table('history')->where('user_id', '=', Auth::user()->id)->where('quaterly_bank_paid','>',0)->get();
		$idatac = DB::table('history')->where('user_id', '=', Auth::user()->id)->where('quaterly_company_incentive_paid','>',0)->get();
		$incentives = DB::table('incentives')->where('userid', '=', Auth::user()->id)->first();
		
		
		
		$this->layout->content = View::make('users.acchistory', array('data' => $data, 'idata' => $idata, 'dashboard_data' => $dashboard_data,'idatac' => $idatac, 'incentives' => $incentives));
		
		
	}//end accounthistory
	
	
	//start carrier list
	function getCarrierlist(){
		$cdata = DB::table('carrier_list')->where('carriercode' , '!=', 'Grand Total')->get();
	
		
		
		
		$this->layout->content = View::make('users.carrierlist', array('cData' => $cdata));
		
		
	}//end carrier list
	
	
	//start carrier list
	function getAecustomerslist(){
		$cdata = DB::table('ae_customers')->where('user_id', '=', Auth::user()->id)->get();
	
		
		
		
		$this->layout->content = View::make('users.aecustomerslist', array('cData' => $cdata));
		
		
	}//end carrier list
	
	
	
	


}
?>