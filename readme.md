## Zboard


Zboard is a Sales Administration panel for ZMAC Transportation, LLC. 

The sales executives have an extensive compensation plan based on their sales and gross profit.


Zboard runs with a database with a couple of tables that old clients, orders, and their goals. Zboard compare and uses all that data to determine at what bounty level they are at and if they are eligible for that level�s bonus.

Bounty Level 1 = Produce +5k in the first 6 months of a customer plus +15% in gross profit.

Bounty Level 2 = Produce +20k in the first 6 months of a customer plus +19% gross profit.

Bounty Level 3 = Produce +50k in the first 18months of a customer. The bonus is determined by a table that holds all the bonuses depending the gross profit level that they are at.
 

In addition to that ZMAC Transportation holds a bank with incentives that pay out in % depending how much gross profit they did as a group.

They also are eligible for incentives based on a % of the bonus with the opportunity of incrementing that bonus if they exceed the goal. 

You can visit: http://bandodesigns.com/zboard/public
