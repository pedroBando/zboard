/**
* returns money as a formatted string
* @param integer $amount
* @param string $symbol currency symbol
* @return string
*/


function money($amount, $symbol = '$')
{
	return $symbol . money_format('%i', $amount);
}
