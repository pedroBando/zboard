var donut1 = Morris.Donut({
    element: "bounty4",
    data: chartBounty4,
	colors: [ "#ffde00", "#e5e4b2" ],
    formatter: function(e) {
        return e + "%";
    }
}).select(0);

Morris.Donut({
    element: "bounty5",
    data: chartBounty5,
	colors: [ "#00ecb7", "#bffff1" ],
    formatter: function(e) {
        return e + "%";
    }
}).select(0);

Morris.Donut({
    element: "chartCoPiece1",
    data: chartCoPiece1,
	colors: [ "#00ecb7", "#bffff1" ],
    formatter: function(e) {
        return e + "%";
    }
}).select(0);

Morris.Donut({
    element: "chartCoPiece2",
    data: chartCoPiece2,
	colors: [ "#00ecb7", "#bffff1" ],
    formatter: function(e) {
        return e + "%";
    }
}).select(0);


