// First Donut chart on Dashboard
Morris.Donut({
    element: "bounty1",
    data: chartBounty1, 
    formatter: function(e) {
        return e + "%";
    }
}).select(0);

Morris.Donut({
    element: "bounty2",
    data: chartBounty2,
	colors: [ "#347413", "#5da438" ],
    formatter: function(e) {
        return e + "%";
    }
}).select(0);

Morris.Donut({
    element: "bounty3",
    data: chartBounty3,
	colors: [ "#a14000", "#ce7f21" ],
    formatter: function(e) {
        return e + "%";
    }
}).select(0);


Morris.Donut({
    element: "chartCoPiece1",
    data: chartCoPiece1,
	colors: [ "#00ecb7", "#bffff1" ],
    formatter: function(e) {
        return e + "%";
    }
}).select(0);

Morris.Donut({
    element: "chartCoPiece2",
    data: chartCoPiece2,
	colors: [ "#00ecb7", "#bffff1" ],
    formatter: function(e) {
        return e + "%";
    }
});



Morris.Donut({
  element: 'donut-example',
  data: [
    {label: "Download Sales", value: 12},
    {label: "In-Store Sales", value: 30},
    {label: "Mail-Order Sales", value: 20}
  ]
});

