var chart;
          /* var chartData = [
                {
                    title: "Jul 2013",
                    sales: 0,
                    goal: 0
                },
                {
                    title: "Aug 2013",
                    sales: 46579.00,
                    goal: 10979.00
                },
                {
                    title: "Sep 2013",
                    sales: 57639.00,
                    goal: 10689.00
                },
                {
                    title: "Oct 2013",
                    sales: 88968.00,
                    goal: 19143.00
                },
                {
                    title: "Nov 2013",
                    sales: 84262.50,
                    goal: 18937.00,
                },
                {
                    title: "Dec 2013",
                    sales: 39301.00,
                    goal: 10801.00,
                },
				{
                    title: "Jan 2014",
                    sales: 39301.00,
                    goal: 10801.00,
                    dashLengthColumn: 5,
                    alpha:0.2,
                    additional:"(projection)"
                }
            ];
*/

            AmCharts.ready(function () {
                // SERIAL CHART  
                chart = new AmCharts.AmSerialChart();
                chart.pathToImages = "images/";
                chart.dataProvider = chartData;
                chart.categoryField = "title";
                chart.startDuration = 1;
                
                

                // AXES
                // category
                var categoryAxis = chart.categoryAxis;
                categoryAxis.gridPosition = "start";
				
			
				
                // value
                var valueAxis = new AmCharts.ValueAxis();
                valueAxis.axisAlpha = 0;
                chart.addValueAxis(valueAxis);

                // GRAPHS
                // column graph
                var graph1 = new AmCharts.AmGraph();
                graph1.type = "column";
                graph1.title = "Margin";
                graph1.lineColor = "#29A2D7";
                graph1.valueField = "sales";
                graph1.lineAlpha = 1;
                graph1.fillAlphas = 1;
                graph1.dashLengthField = "dashLengthColumn";
                graph1.alphaField = "alpha";
                graph1.balloonText = "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]]</b> [[additional]]</span>";
				graph1.columnWidth ="0.8",
                chart.addGraph(graph1);

                // line
                var graph2 = new AmCharts.AmGraph();
                graph2.type = "line";
                graph2.title = "Margin Goal";
                graph2.lineColor = "#fcd202";
                graph2.valueField = "goal";
                graph2.lineThickness = 3;
                graph2.bullet = "round";
                graph2.bulletBorderThickness = 3;
                graph2.bulletBorderColor = "#fcd202";
                graph2.bulletBorderAlpha = 1;
                graph2.bulletColor = "#ffffff";
                graph2.dashLengthField = "dashLengthLine";
                graph2.balloonText = "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]]</b> [[additional]]</span>";
                chart.addGraph(graph2);
				
				

                // LEGEND                
                var legend = new AmCharts.AmLegend();
                legend.useGraphSettings = true;
                chart.addLegend(legend);

                // WRITE
                chart.write("chartdivca");
            });