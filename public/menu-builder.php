<?php
function buildMenu($menuList)
{
  // Get the link name
  $pieces = explode('/',$_SERVER['REQUEST_URI']);  
  $page=end($pieces); 
	$userrid = Auth::user()->id;
    foreach ($menuList as $val=>$node)
    {
      $active=(strpos($page,$node['link']) !== false) ? "active" : " ";  

        //Running array for Main parent links
        if (! empty($node['children'])) 
        {
          echo " <li class='submenu ". $active ."'><a class='dropdown' href='" . $node['link']. "' data-original-title='" . $node['title'] . "'><i class='fa fa-".$node['icon']."'></i><span class='hidden-minibar'> " . $node['title'] . "  <span class='badge bg-primary pull-right'>".count($node['children'])."</span></span></a>";
        }
        else
        {
          echo "<li class='". $active ."' ><a href='" . $node['link']. "' data-original-title='" . $node['title'] . "'><i class='fa fa-".$node['icon']."'></i><span class='hidden-minibar'> " . $node['title'] . "</span></a>";
        }
        

        // Running submenu
        if ( ! empty($node['children'])) 
        {
            echo "<ul>";
            buildMenu($node['children']);
            echo "</ul>";
        }
        echo "</li>";
    }
}

if($userrid == 1 || $userrid == 2 || $userrid == 3 ||$userrid == 4 || $userrid == 5 ){

$menuList = Array(
    0 => Array(
        'title' => 'Dashboard',
        'link' => 'index.php',
        'icon' => 'dashboard',
        'children' => Array()
    ),
	
    1 => Array(
        'title' => 'Overview',
        'link' => 'overview.php',
        'icon' => 'indent',
        'children' => Array()
            
    ),
	
	2 => Array(
        'title' => 'Support',
        'link' => 'support.php',
        'icon' => 'gears',
    ),
	3 => Array(
        'title' => 'History',
        'link' => 'customers.php',
        'icon' => 'bar-chart-o'
		),
		
   
);

}

else{
	$menuList = Array(
    0 => Array(
        'title' => 'Dashboard',
        'link' => 'index.php',
        'icon' => 'dashboard',
        'children' => Array()
    ),
	
	
	1 => Array(
        'title' => 'Support',
        'link' => 'support.php',
        'icon' => 'gears',
    ),
	2 => Array(
        'title' => 'History',
        'link' => 'customers.php',
        'icon' => 'bar-chart-o'
		),
		
   
);
	
}
?>
